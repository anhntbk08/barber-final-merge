(function() {
  "use strict";

  angular.module('barber')
    .service('BarberService', ['$q', 'GeoService', 'Firebase',
      function($q, GeoService, Firebase) {

        var lastLocation = null,
          cachedListings = [],
          firebase = Firebase.getInstance();

        return {
          getBarberInfo: getBarberInfo,
          getBarberDetail: getBarberDetail,
          getSingleBusiness: getSingleBusiness,
          getStatistics: getStatistics
        };

        function getBarberInfo(uid) {
          var defer = $q.defer();
          var users = firebase.child('users').child(uid);
          var user_settings = firebase.child('user_settings').child(uid);

          users.once('value', function (userInfo) {
            user_settings.once('value', function (settings) {
              defer.resolve(
                angular.extend({
                  settings: settings.val()
                }, {
                  userInfo: userInfo.val()
                })
              );
            })
          })

          return defer.promise;
        }

        function getBarberDetail(uid) {
          console.log(uid);
          var defer = $q.defer();
          var users = firebase.child('users').child(uid);
          var settings = firebase.child('settings').child(uid);        
          users.once('value', function (userInfo) {
            settings.once('value', function (settings) {
              defer.resolve(
                angular.extend({}, settings.val(), userInfo.val())
              );
            })
          })
          return defer.promise;

        }

        function getSingleBusiness(businessId) {
          var url,
            deferred = $q.defer();

          return deferred.promise;
        }

        function getStatistics(params){
          var defer = $q.defer();
          setTimeout(function(){
            defer.resolve({
              monthlyGoal: 2000,
              totalSoFar: 1500,
              totalHaircuts: 40,
              avgTip: 9,
              totalTips: 360
            })
          }, 100);
          
          return defer.promise;
        }
      }
    ]);
}());