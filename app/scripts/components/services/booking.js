(function() {
  "use strict";

  angular.module('barber')
    .service('BookingService', ['$http', '$q', '$ionicLoading', '$ionicPopup', 'Auth', 'Firebase', 'utils',
      function($http, $q, $ionicLoading, $ionicPopup, Auth, Firebase, utils) {
        var firebase = Firebase.getInstance();

        return {
          getBookedList: getBookedList,
          getUserBookings: getUserBookings,
          getBarberBookings: getBarberBookings,
          getBarberBookingsByDate: getBarberBookingsByDate,
          getBookings: getBookings,
          getListBookingsByBaberID: getListBookingsByBaberID,
          setBookingInfo: setBookingInfo,
          setBarberBooking: setBarberBooking,
          setUserBooking: setUserBooking,
          setUserBookingStatus: setUserBookingStatus,
          payment: payment,
          setCallendar: setCallendar
        };

        function setCallendar(clientId, event) {
          console.log(event);          
          var success = function(message) {
            var alertPopup = $ionicPopup.alert({
              title: 'Appointment Booked!',
              template: ''
            });            
          };
          var error = function(message) {
            $ionicPopup.alert({
              title: 'Set Calendar Error:',
              template: message
            });
          };
          // if you want to create a calendar with a specific color, pass in a JS object like this:
          if(window.plugins){
            var startDate = new Date(event.year,parseInt(event.month)-1,event.date,event.slot,0,0,0,0); // beware: month 0 = january, 11 = december
            var endDate = new Date(event.year,parseInt(event.month)-1,event.date,parseInt(event.slot)+1,0,0,0,0);
            var title = "Haircut appointments";
            var eventLocation = event.baber_info.barbershop+" - "+event.baber_info.address;
            var notes = "You have an appointment with the barber "+event.baber_info.name+" at "+event.slot+"h "+event.year+"-"+event.month+"-"+event.date+". Baber address: "+event.baber_info.address;
            var calOptions = window.plugins.calendar.getCalendarOptions();
            calOptions.firstReminderMinutes = 30; 
            window.plugins.calendar.createEventWithOptions(title,eventLocation,notes,startDate,endDate,calOptions,success,error);
          }else{
            console.log("calendar plugin not defined");
          }
        }

        function getBookedList(day, businessId) {
          var $defer = $q.defer();
          var formatedDay = moment(day).format('dd/mm/yyyy');
          var ref = firebase.child("barber_bookings/" + businessId);
          ref.equalTo(formatedDay).orderByChild("timestamp").on("value", function(snapshot) {
            return $defer.resolve(snapshot.val());
          });
          return $defer.promise;
        }

        function getUserBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('user_bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });

          return defer.promise;
        }

        function getBarberBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('barber_bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getBarberBookingsByDate(id, date) {
          var defer = $q.defer();
          var ref = firebase.child('barber_bookings/' + id + '/' + date).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getListBookingsByBaberID(id) {
          console.log(id);
          var defer = $q.defer();
          var ref = firebase.child('bookings').on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function setBookingInfo(key, value) {
          var defer = $q.defer();
          var ref = firebase.child("bookings").child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setBarberBooking(barberId, timestamp, key, value) {
          var defer = $q.defer();
          var ref = firebase.child("barber_bookings").child(barberId).child(timestamp).child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setUserBooking(userId, key, value) {
          var defer = $q.defer();
          var ref = firebase.child("user_bookings").child(userId).child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setUserBookingStatus(subPath, value) {
          var defer = $q.defer();
          var user_bookings = firebase.child(subPath.user);
          var barber_bookings = firebase.child(subPath.barber);
          var bookings = firebase.child(subPath.booking);
          user_bookings.set(value, function(error) {
            if (error) {
              defer.resolve({
                success: false,
                code: error.code
              });
            } else {
              bookings.set(value, function(error) {
                if (error) {
                  defer.resolve({
                    success: false,
                    code: error.code
                  });
                } else {
                  defer.resolve({
                    success: true
                  });
                }
              });
              // barber_bookings.set(value, function(error) {
              //   if (error) {
              //     defer.resolve({
              //       success: false,
              //       code: error.code
              //     });
              //   } else {
              //     bookings.set(value, function(error) {
              //       if (error) {
              //         defer.resolve({
              //           success: false,
              //           code: error.code
              //         });
              //       } else {
              //         defer.resolve({
              //           success: true
              //         });
              //       }
              //     });
              //   }
              // });
            }
          });
          return defer.promise;
        }

        function payment(paymentInfo) {
          var defer = $q.defer();
          var ref = firebase.child("user_bookings");
          ref.set(paymentInfo, function(error) {
            if (error) {
              defer.resolve({
                success: false,
                code: error.code
              });
            } else {
              defer.resolve({
                success: true
              });
            }
          });
          return defer.promise;
        }
      }
    ]);
}());