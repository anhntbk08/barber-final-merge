(function() {
  "use strict";
  angular.module('barber')

  .factory('UserService', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        getSettings: getSettings,
        updateSettings: updateSettings
      };

      function getSettings() {
        var defer = $q.defer();

        var ref = firebase.child("settings").child(Auth.currentUserInfo.uid);

        ref.once("value", function(data) {
          defer.resolve(data.val());
        });

        return defer.promise;
      }

      function updateSettings(settings) {
        var defer = $q.defer();

        var ref = firebase.child("settings").child(Auth.currentUserInfo.uid);

        ref.update(settings, function(error) {
          defer.resolve(error);
        });

        return defer.promise;
      }
    }
  ]);

}());