(function() {
  "use strict";

  angular.module('barber')
    .service('SalonService', ['$q', 'GeoService', 'Firebase',
      function($q, GeoService, Firebase) {

        var lastLocation = null,
          cachedListings = [],
          firebase = Firebase.getInstance();

        return {
          requestListBarber: requestListBarber,
          getBusiness: getBusiness,
          getSingleBusiness: getSingleBusiness,
          getCached: getCached
        };

        function requestListBarber(currentPage) {
          var deferred = $q.defer();

          GeoService.getGeofireQueryResults().then(function(geoData) {
            cachedListings = geoData;
            deferred.resolve(geoData);
          });

          return deferred.promise;
        }

        function getBusiness(businessId) {
          var deferred = $q.defer();
          var ref = firebase.child("settings").child(businessId);

          ref.once("value", function(data) {
            if (data)
              deferred.resolve(data.val());
            else
              deferred.reject();
          });

          return deferred.promise;
        }

        function getSingleBusiness(businessId) {
          var url,
            deferred = $q.defer();

          return deferred.promise;
        }

        function getCached() {
          var deferred = $q.defer();

          if (cachedListings && cachedListings.length) {
            setTimeout(function() {
              return deferred.resolve(cachedListings);
            });
          } else {
            getListings(0).then(function(lists) {
              deferred.resolve(lists);
            }, function(err) {
              deferred.reject(null);
            });
          }
          return deferred.promise;
        }

      }
    ]);
}());