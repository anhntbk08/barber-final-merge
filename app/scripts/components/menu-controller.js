(function() {
  "use strict";

  angular.module('barber')
    .controller('MenuController', function($rootScope, $scope, $ionicModal, $timeout, $ionicHistory, $ionicSideMenuDelegate, $ionicLoading, Auth, UserService) {
      $scope.userInfo = Auth.currentUserInfo;
      $scope.userType = Auth.type();
      UserService.getSettings().then(function(settings) {
        $rootScope.userSettings = settings;
      });           
      $scope.menuUserLogout = function() {
        //$scope.closeMenu();
        $rootScope.logout();
        //$ionicLoading.show({ template: 'Logging Out', noBackdrop: true, duration: 2000 });
      }

      $scope.closeMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
        $ionicHistory.nextViewOptions({
          disableAnimate: true
        });
      }
    });
}());