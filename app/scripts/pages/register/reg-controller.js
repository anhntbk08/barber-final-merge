(function() {
  'use strict';

  angular.module('barber')
    .controller('RegController', ['$rootScope', '$scope', '$ionicLoading', '$state', '$ionicHistory', '$firebase', '$q',
      function($rootScope, $scope, $ionicLoading, $state, $ionicHistory, $firebase, $q) {

        // // Send these vars to settings vars. That way they will be accessable from userPref
        // $scope.userFirstName = Settings.userFirstName;
        // $scope.userLastName = Settings.userLastName;
        // $scope.userZipCode = Settings.userZipCode;
        // $scope.userFavStyle = Settings.userFavStyle;
        // $scope.userFreq = Settings.userFreq;

        // var range = [{
        //   name: "1 mile",
        //   value: "1"
        // }, {
        //   name: "2 miles",
        //   value: "2"
        // }, {
        //   name: "5 miles",
        //   value: "5"
        // }, {
        //   name: "10 miles",
        //   value: "10"
        // }, {
        //   name: "20 miles",
        //   value: "20"
        // }];

        // var freq = [{
        //   name: "3 Days",
        //   value: "3"
        // }, {
        //   name: "Weekly",
        //   value: "7"
        // }, {
        //   name: "Every 2 Weeks",
        //   value: "14"
        // }, {
        //   name: "Monthly",
        //   value: "30"
        // }, {
        //   name: "Every 3 Months",
        //   value: "90"
        // }];

        // var area = [
        //   "16801",
        //   "33162",
        //   "33138"
        // ];

        // var style = [
        //   "Fade",
        //   "Blowout",
        //   "Afro"
        // ];

        // $scope.currentStyle = style;
        // $scope.currentFreq = freq;

        // $scope.onChange = function(type, value) {
        //   $scope[type] = value;
        //   Settings[type] = value;
        //   //console.log("$scope[" + type + "] = " + value);
        // };

        // $scope.gUserLogout = function() {
        //   $rootScope.logout();
        // }

        // $scope.regBasicSubmit = function() {
        //   $scope.regBasicFormCheck();
        //   $scope.regBasicFormCheck().then(function(data) {
        //     console.log("regBasicSubmit");
        //     console.log("inputFirstNameVar: " + Settings.userFirstName);
        //     console.log("inputLastNameVar: " + Settings.userLastName);
        //     console.log("inputZipCodeVar: " + Settings.userZipCode);
        //     $state.go('reg.userPref');
        //   }, function(error) {
        //     console.log(error);
        //     alert(error);
        //     return false;
        //   });
        // }

        // $scope.regPrefSubmit = function() {
        //   console.log("regPrefSubmit");
        //   console.log("inputFirstNameVar: " + Settings.userFirstName);
        //   console.log("inputLastNameVar: " + Settings.userLastName);
        //   console.log("inputZipCodeVar: " + Settings.userZipCode);
        //   console.log("userFavStyle: " + Settings.userFavStyle);
        //   console.log("userFreq: " + Settings.userFreq.value);
        //   console.log("userEmail: " + Settings.userEmail);

        //   var usersRef = $rootScope.fbase.child("users").child(Settings.userGoogleID);
        //   usersRef.set(Settings, function(error) {
        //     if (error) {
        //       console.log("Data could not be saved." + error);
        //     } else {
        //       console.log("Data saved successfully.");
        //       $state.go('tut.one').then(function() {
        //         $ionicHistory.clearHistory();
        //         $ionicHistory.clearCache();
        //       });
        //     }
        //   });
        // }

        // $scope.regBasicFormCheck = function() {
        //   var deferred = $q.defer();
        //   if ($scope.userFirstName === null || $scope.userFirstName === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- First Name missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- First Name missing");
        //   } else if ($scope.userLastName === null || $scope.userLastName === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- Last Name missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- Last Name missing");
        //   } else if ($scope.userZipCode === null || $scope.userZipCode === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- Zip Code missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- Zip Code missing");
        //   } else {
        //     deferred.resolve("All information has been entered");
        //   }
        //   return deferred.promise;
        // }
      }
    ]);
}());