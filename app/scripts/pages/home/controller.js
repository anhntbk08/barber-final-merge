(function() {
  'use strict';

  angular.module('barber')
    .controller('HomeController', ['$scope', '$ionicLoading', '$state', '$ionicHistory', 'Auth', '$ionicPopup',
      function($scope, $ionicLoading, $state, $ionicHistory, Auth, $ionicPopup) {
        $scope.userType = Auth.type();        
        $scope.$watch(function() {
          return Auth.type();
        }, function() {
          $scope.userType = Auth.type();
        });
      }
    ]);
}());