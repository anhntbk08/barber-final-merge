angular.module('barber')
  .directive('autoResizeImage', function($compile) {
    return {
      restrict: "EA",
      link: function(scope, elm, attrs) {

        var img = elm.find('img');

        function resizeImage() {
          var width = $(elm).width();
          if (width <= 138) {
            img.css({
              'width': width + 'px',
              'height': width + 'px'
            });
          } else {
            img.css({
              'width': '128px',
              'height': '128px'
            });
          }
        }

        scope.$watch(function() {
          return $(elm).width();
        }, resizeImage);

        $(window).bind('resize', function() {
          resizeImage();
        });
      }
    };
  });