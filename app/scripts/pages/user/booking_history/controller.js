angular.module('barber')
  .controller('UserBookingHistory', ['$scope', 'BookingService', 'BarberService', 'utils', 'Auth', '$timeout', '$ionicLoading', '$ionicModal', '$http', 'CONFIG', 'UserService', '$ionicPopup',
    function($scope, BookingService, BarberService, utils, Auth, $timeout, $ionicLoading, $ionicModal, $http, CONFIG, UserService, $ionicPopup) {
      var uid = Auth.currentUserInfo.uid;
      $scope.type = Auth.currentUserInfo.type;

      function getBookings() {
        $scope.getting = true;
        $ionicLoading.show({
          template: 'Get Listings...',
          noBackdrop: true
        });
        $scope.bookings = [];
        if ($scope.type == 'customer') {
          BookingService.getUserBookings(uid).then(function(res) {
            if (res.success && res.data) {
              $scope.userBookings = res.data;
              for (var ub in $scope.userBookings) {
                if ($scope.userBookings[ub] == 'pending')
                  delete $scope.userBookings[ub];
              }
              if ($scope.userBookings) {
                var i = 0;
                for (var ub in $scope.userBookings) {
                  i++;
                  var barberId = ub.split('_')[0];
                  (function(barberId, ub) {
                    if (localStorage.getItem(barberId)) {
                      setBarberDetailInfo(JSON.parse(localStorage.getItem(barberId)), ub);
                    } else {
                      BarberService.getBarberDetail(barberId).then(function(res) {
                        localStorage.setItem(barberId, JSON.stringify(res));
                        setBarberDetailInfo(res, ub);
                      });
                    }

                    function setBarberDetailInfo(res, ub) {
                      var bk = angular.extend({}, res);
                      BookingService.getBookings(ub).then(function(res) {
                        if (res.success && res.data) {
                          bk = angular.extend(bk, res.data);
                          $timeout(function() {
                            bk.bookingId = ub;
                            bk.bookedDate = moment(new Date(bk.booked_time)).format('dddd MMM D YYYY');
                            $scope.bookings.push(bk);
                            if (i >= $scope.bookings.length) {
                              $ionicLoading.hide();
                              $scope.$broadcast("scroll.refreshComplete");
                              $scope.getting = false;
                            }
                          });
                        }
                      });
                    }

                  })(barberId, ub);
                }
              } else {
                $ionicLoading.hide();
                $scope.$broadcast("scroll.refreshComplete");
                $scope.getting = false;
              }
            } else {
              $ionicLoading.hide();
              $scope.$broadcast("scroll.refreshComplete");
              $scope.getting = false;
            }
          });
        } else {
          BookingService.getBarberBookings(uid).then(function(res) {
            if (res.success && res.data) {
              $scope.userBookings = {};
              for (var date in res.data) {
                $scope.userBookings = angular.extend($scope.userBookings, res.data[date]);
              }
              if ($scope.userBookings) {
                var i = 0;
                for (var ub in $scope.userBookings) {
                  i++;
                  var customerId = ub.split('_')[1];
                  (function(customerId, ub) {
                    BarberService.getBarberDetail(customerId).then(function(res) {
                      console.log(res);
                      var bk = angular.extend({}, res);
                      BookingService.getBookings(ub).then(function(res) {
                        if (res.success && res.data) {
                          bk = angular.extend(bk, res.data);
                          $timeout(function() {
                            bk.bookingId = ub;
                            bk.createdDate = moment(new Date(bk.created_date)).format('dddd MMM D YYYY');
                            if (bk.status != 'pending')
                              $scope.bookings.push(bk);
                            if (i >= $scope.bookings.length) {
                              $ionicLoading.hide();
                              $scope.$broadcast("scroll.refreshComplete");
                              $scope.getting = false;
                            }
                          });
                        }
                      });
                    });
                  })(customerId, ub);
                }
              } else {
                $ionicLoading.hide();
                $scope.$broadcast("scroll.refreshComplete");
                $scope.getting = false;
              }
            } else {
              $ionicLoading.hide();
              $scope.$broadcast("scroll.refreshComplete");
              $scope.getting = false;
            }
          });
        }
      }

      getBookings();
      $scope.refresh = function() {
        getBookings();
      }

      $scope.updateStatus = function(booking, status) {
        var confirm = utils.confirm('Update Status', 'Are you sure you want to ' + status + ' this booking?').then(function(res) {
          if (res) {
            var barberId = booking.bookingId.split('_')[0];
            var path = {
              user: 'user_bookings/' + uid + '/' + booking.bookingId,
              barber: 'barber_bookings/' + barberId + '/' + booking.bookingId,
              booking: 'bookings/' + booking.bookingId + '/status'
            }
            BookingService.setUserBookingStatus(path, status).then(function(res) {
              if (res.success) {
                booking.status = status;
              }
            });
          }
        });
      }

      $scope.modal = {};
      $scope.openPaymentModal = function(booking) {
        console.log(booking);
        $scope.paymentInfo = {
          bookingId: booking.bookingId,
          status: booking.status,
          cardNumber: ''
        };

        $ionicLoading.show({
          template: 'Processing ...',
          noBackdrop: true
        });

        // get user settings
        var paymentForm = {};
        UserService
          .getSettings()
          .then(function(settings) {
            $ionicLoading.hide();


            if (!settings.payment.cardnumber ||
              !settings.payment.cardmonth ||
              !settings.payment.cardyear ||
              !settings.payment.cardcvc) {
              return swal("Oops...", "You haven't input the payment info ", "error");
            }

            settings.customerName = Auth.currentUserInfo.name;
            var temp = settings;
            delete temp.avatar;
            localStorage.setItem('user_settings', JSON.stringify(temp));

            // do the payment
            paymentForm.object = 'card';
            paymentForm.number = settings.payment.cardnumber;
            paymentForm.exp_month = settings.payment.cardmonth;
            paymentForm.exp_year = settings.payment.cardyear;
            paymentForm.cvc = settings.payment.cardcvc;
            paymentForm.customerName = Auth.currentUserInfo.name;
            paymentForm.description = 'CLYP - Pay money for haircut';

            $ionicLoading.show({
              template: 'Paying...',
              noBackdrop: true
            });

            $http.post(CONFIG.clyprServerURL + '/api/stripe/cardCharge', {
              chargeInfo: paymentForm,
              bookingId: $scope.paymentInfo.bookingId,
              userId: Auth.currentUserInfo.uid
            }).
            then(function(res) {
              if (res.data.err || res.data.status != "succeeded") {
                $ionicLoading.hide();
                return swal('Oops', res.data.msg || res.data.message || "Server return error - try again later !!", 'error');
              }
              $ionicLoading.hide();
              $ionicModal.fromTemplateUrl('scripts/pages/user/booking_history/payment/view.html', {
                scope: $scope,
                backdropClickToClose: false,
                hardwareBackButtonClose: false
              }).then(function(modal) {
                $scope.modal = modal;
                if (booking.status == 'booked')
                  $scope.modal.show();
              }, function() {
                getBookings();
              });
            }, function(err) {
              console.log(err);
            });

            // request tips
          });
      }

      $scope.paymentInfo = {};
      $scope.setTip = function(value) {
        $scope.paymentInfo.tips = value;
      }

      $scope.payment = function() {

        var paymentForm = {};
        // Save card info into JS object

        var userSettings = JSON.parse(localStorage.getItem('user_settings'));

        paymentForm.object = 'card';
        paymentForm.number = userSettings.payment.cardnumber;
        paymentForm.exp_month = userSettings.payment.cardmonth;
        paymentForm.exp_year = userSettings.payment.cardyear;
        paymentForm.cvc = userSettings.payment.cardcvc;
        paymentForm.customerName = userSettings.customerName;
        paymentForm.description = 'CLYP - Pay tips for barber';
        paymentForm.tips = $scope.paymentInfo.tips;
        if (paymentForm.tips==undefined) {
          return swal("Oops...", "You must add the tip !!", "error");
        }else if(paymentForm.tips==0){
          $scope.modal.hide();
          getBookings();          
          return true;
        }

        $ionicLoading.show({
          template: 'Paying...',
          noBackdrop: true
        });

        $http.post(CONFIG.clyprServerURL + '/api/stripe/paytips', {
          chargeInfo: paymentForm,
          bookingId: $scope.paymentInfo.bookingId,
          userId: Auth.currentUserInfo.uid
        })
          .then(function(res) {
            if (res.data.status == "succeeded") {
              $ionicLoading.hide();
              $scope.modal.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Payment Successful',
                // template: "Payment Successful"
              });
              alertPopup.then(function(res) {
                getBookings();
              });              
            } else {
              $ionicLoading.hide();
              return swal('Oops', res.data.message, 'error');
            }

          }, function(err) {
            console.log(err);
            $ionicLoading.hide();
          });

        $scope.modal.hide();

      }

      $scope.show_list = function(status, event) {
        $('#status_button .button').removeClass('button-positive').addClass('button-dark');
        $(event.target).removeClass('button-dark').addClass('button-positive');
        $('.ubh_item').css({
          display: 'none'
        });
        $('.' + status).css({
          display: 'block'
        });
      }
    }
  ]);