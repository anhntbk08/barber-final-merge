(function() {
  'use strict';
  angular.module('barber')
    .controller('UserSettingsController', ['$scope', '$rootScope', '$timeout', '$ionicLoading', 'Auth', 'UserService', 'File', 'Account',
      function($scope, $rootScope, $timeout, $ionicLoading, Auth, UserService, File, Account) {
        $ionicLoading.show({
          template: 'Loading...'
        });

        $scope.settings = {
          email: Auth.currentUserInfo.email,
          payment: {
            type: 'card'
          }
        };

        UserService
          .getSettings()
          .then(function(settings) {
            $ionicLoading.hide();
            $scope.settings = angular.extend($scope.settings, settings);
            $timeout(function () {$('input[name="cardNumber"]').inputCloak({type: 'credit'});});
            listenSettingChange();
          });

        function listenSettingChange() {
          $scope.$watchCollection('settings', function(newVal, oldVal) {
            if (oldVal)
              UserService
              .updateSettings(newVal)
              .then(function(error) {});
          })
          $scope.$watchCollection('settings.payment', function(newVal, oldVal) {
            if (oldVal)
              UserService
              .updateSettings($scope.settings)
              .then(function(error) {});
          })
        }
        $scope.cardnumberChange = function(newVal) {
          $timeout(function () {
            $scope.settings.payment.cardnumber = newVal;
          });
        }
        $scope.avatarUploadChange = function(newVal) {
          if (newVal) {
            File
              .getContent(newVal)
              .then(function(content) {
                $scope.settings.avatar = content;
                $rootScope.userSettings.avatar = content;
              });
          }
        };
      }
    ]);
}());