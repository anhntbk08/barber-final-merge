angular.module('barber')

.controller('UserAppointmentsController', [
  '$scope','$ionicLoading',
  function($scope, $ionicLoading){
    $scope.sliderToggle = 0;

    // pass in event source in this link
    $scope.eventSources = [];
    $ionicLoading.show({
      template: 'Loading...'
    });
    $scope.uiConfig = {
      calendar:{
        titleFormat: 'MMMM',
        header: {
          left: 'YYYY',
          center: 'prev, title next',
          right: ''
        },
        defaultDate: new Date(),
        viewRender: function(view) {
          $ionicLoading.hide();
          $scope.calendarYear = view.start.format().substr(0, 4);              
        }
      }
    };
  }
]);
