(function() {
  "use strict";

  angular.module('barber')
    .controller("BarberSalonController", ['$scope', '$timeout', '$rootScope', 'SalonService', '$ionicLoading', 'DEFAULT_SEARCH_SETTINGS', 'GeoService', '$state',
      function($scope, $timeout, $rootScope, SalonService, $ionicLoading, DEFAULT_SEARCH_SETTINGS, GeoService, $state) {
        var currentPage = 0;
        $scope.showingListings = true;
        $scope.searchLocation = null;
        $scope.listBarbers = {};

        $scope.sortBarberOrderList = [{
          name: "First Name",
          value: "given_name"
        }, {
          name: "Last Name",
          value: "family_name"
        }, {
          name: "Distance (Nearest First)",
          value: "distance"
        }, ];

        $scope.geocodeOptions = {
          componentRestrictions: {
            country: 'US'
          }
        };

        $scope.MAXIMUM_SEARCH_RANGE = DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles;
        $scope.option = {
          searchRadiusInMiles: DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles,
          currentSortBarberOrder: $scope.sortBarberOrderList[0].value
        }

        $scope.setShowingListings = function(nextState) {
          $scope.showingListings = nextState;
        };

        function init() {
          $ionicLoading.show({
            template: 'Get Listings...',
            noBackdrop: true,
            duration: 5000
          });

          GeoService
            .requestUserLocation()
            .then(function() {
              SalonService.requestListBarber().then(function(listBarbers) {                
                $scope.listBarbers = listBarbers;
                currentPage++;
              });
            });
        }

        $scope.countProperties = function(obj) {
          var count = 0;
          for (var index in obj) count++;

          return count;
        }

        $rootScope.$on('BARBERS_DATA_CHANGE', function(event, barbers) {
          $timeout(function() {
            $scope.listBarbers = barbers;
          })
        });

        $scope.refresh = function() {
          $scope.listBarbers = [];
          currentPage = 0;

          $ionicLoading.show({
            template: 'Refreshing...',
            noBackdrop: true,
            duration: 2000
          });

          SalonService.requestListBarber().then(function(listBarbers) {
            $ionicLoading.show({
              template: 'Get Listings...',
              noBackdrop: true,
              duration: 2000
            });
            $scope.listBarbers = listBarbers;
            currentPage++;
          });
          $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.updateSearch = function() {
          GeoService.updateGeofireQueryRadius($scope.option.searchRadiusInMiles);
          $state.go('salons.results');
        }

        init();
      }
    ]);
}());