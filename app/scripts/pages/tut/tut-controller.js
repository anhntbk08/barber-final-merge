(function() {
  'use strict';

  angular.module('barber')
    .controller('TutController', ['$scope', '$ionicLoading', '$state', '$ionicHistory',
      function($scope, $ionicLoading, $state, $ionicHistory) {
        console.log("TutController launching");

        $scope.noAnimate = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
        };

        $scope.pageTwo = function() {
          $state.go("tut.two");
        };
      }
    ]);
}());