(function() {
  "use strict";

  angular.module('barber')
    .controller("BarberReviewsController", ['$scope', 'MapService', 'LocationService', 'BarberService', '$stateParams',
      function($scope, MapService, LocationService, BarberService, $stateParams) {

        $scope.id = $stateParams.id;
        $scope.barb = {};

        BarberService.getSingleBusiness($scope.id).then(function(barber) {
          if (barber) {
            $scope.barb = barber;
          }
        });
      }
    ]);
}());