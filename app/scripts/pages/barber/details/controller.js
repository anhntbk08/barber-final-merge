(function() {
  "use strict";
  angular.module('barber')
    .controller("BarberDetailsController", ['$scope', '$stateParams', '$state', '$ionicHistory', 'BarberService',
      function($scope, $stateParams, $state, $ionicHistory, BarberService) {
        $scope.biz = {};
        $scope.barberId = $stateParams.id;
        $scope.$on('mapInitialized', function(event, map) {
          if(localStorage.getItem($stateParams.id)){
            setBaberDetailInfo(JSON.parse(localStorage.getItem($stateParams.id)), map);
          }else{
            BarberService.getBarberDetail($stateParams.id).then(function(detail_info) {
              if (detail_info) {
                localStorage.setItem($stateParams.id, JSON.stringify(detail_info));
                setBaberDetailInfo(detail_info, map);
              }
            });            
          }
        });
        function setBaberDetailInfo(detail_info, map){
          console.log(detail_info);
          $scope.biz = detail_info;
          var location = new google.maps.LatLng(detail_info.barberLocation.latitude, detail_info.barberLocation.longitude);
          var marker = new google.maps.Marker({
            position: location,
            map: map
          });
          map.panTo(location);
        }
      }
    ]);
}());