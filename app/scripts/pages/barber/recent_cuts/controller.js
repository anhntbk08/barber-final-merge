angular.module('barber')
.controller('BarberRecentCutsController', ['$scope', 'BookingService', 'BarberService', 'utils', 'Auth', '$timeout', '$ionicLoading',
  function ($scope, BookingService, BarberService, utils, Auth, $timeout, $ionicLoading) {
    var uid = Auth.currentUserInfo.uid;
    $scope.type = Auth.currentUserInfo.type;    
      $ionicLoading.show({
        template: 'Get Listings...',
        noBackdrop: true
      });
      $scope.bookings = [];
      BookingService.getBarberBookings(uid).then(function(res){        
        if(res.success && res.data){
          $scope.userBookings = {};
          var date = new Date();
          var nowDate = moment(date).format('YYYY-MM-DD');
          var nowHouse = moment(date).format('H');
          for(var date in res.data){            
            var $obj = res.data[date];
            var $slot = $obj[Object.keys($obj)];            
            if(date <= nowDate || (date == nowDate && $slot <= nowHouse)){              
              $scope.userBookings = angular.extend($scope.userBookings, res.data[date]);                
            }            
          }        
          if(!$.isEmptyObject($scope.userBookings)){
            var i = 0;
            for(var ub in $scope.userBookings){              
              i++;
              var customerId = ub.split('_')[1];
              (function(customerId, ub){
                BarberService.getBarberDetail(customerId).then(function(res){
                  var bk = angular.extend({}, res);
                  var bookingTime = parseInt(ub.split('_')[2]);
                  BookingService.getBookings(ub).then(function(res){
                    if(res.success && res.data){
                      bk = angular.extend(bk, res.data);
                      bk.bookingTime = {
                        weekday: moment(bookingTime).format('dddd'),
                        month: moment(bookingTime).format('MMM'),
                        day: moment(bookingTime).format('DD'),                        
                        year: moment(bookingTime).format('YYYY')
                      };
                      bk.slot = (bk.slot>12)?((bk.slot-12)+" PM"):(12+" AM");
                      $timeout(function(){
                        bk.bookingId = ub;                        
                        if(bk.status != 'pending' && bk.status != 'booked'){
                          $scope.bookings.push(bk);
                        }
                        if(i >= $scope.bookings.length){
                          $ionicLoading.hide();
                        }
                      });
                    }
                  });
                });
              })(customerId, ub);
            }
          }else{               
            $scope.userBookings = null;      
            $ionicLoading.hide();  
          }
        }
        else{
          $ionicLoading.hide();
        }
      });
      $scope.show_list = function(status,event,limit) {
        $('#status_button .button').removeClass('button-positive').addClass('button-dark');
        $(event.target).removeClass('button-dark').addClass('button-positive');
        $('.ubh_item').css({display:'none'});
        $('.'+status+':lt('+limit+')').css({display:'block'});
      }
    }
]);