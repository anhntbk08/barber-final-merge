angular.module('barber')

.controller('BarberStatisticController', [
  '$scope',
  'BarberService',
  function($scope, BarberService) {
    $scope.statistic = {};
    $scope.sliderToggle = 0;

    $scope.currentPeriod = "1";
	
	/*
		Get Statistic info throught services
		here you don't give me real api so i fake data,
		refer the barber_statistics.service.js for more info, i wrote an fake function for test and a real $http request(that commented).
	*/
	BarberService.getStatistics().then(function(result){
		result.needHairCuts = Math.ceil((result.monthlyGoal - result.totalSoFar - result.totalTips)/result.avgTip);
		$scope.statistic = result;
	})

    //bar data
    $scope.barChart = {
      "options": {
        "chart": {
          "backgroundColor": "transparent",
          "type": "areaspline",
          "width": $(document).width()
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        }
      },
      xAxis: {
        categories: ['Total So Far', 'Need to Reach Goal', 'Total From tips'],
        minPadding: 30
      },
      yAxis: {
        stackLabels: {
          style: {
            color: 'white'
          },
          enabled: true,
          x: -20,
        }
      },
      "series": [{
        "data": [1, 3, 5],
        "type": "column"
      }],
      title: {
        text: 'October',
        userHTML: true
      }
    }

    // line data
    $scope.lineChart = {
      "options": {
        "chart": {
          "backgroundColor": "transparent",
          "type": "line",
          "width": $(document).width()
        },
        "plotOptions": {
          "series": {
            "stacking": ""
          }
        }
      },
      "series": [{
        "data": [
          2,
          13,
          16,
          3,
          10,
          17,
          20,
          19,
          6,
          16
        ],
        "id": "series-4"
      }],

      title: {
        text: 'October',
        userHTML: true
      },

      //function (optional)
      func: function(chart) {
        console.log(chart);
        setTimeout(function() {
          var baseX = chart.plotLeft;
          var baseY = chart.chartHeight - chart.marginBottom;

          // calculate goal line
          var maxGoal = 22;
          var totalSofar = 1500;
          var goalLine = chart.yAxis[0].height / chart.yAxis[0].max * maxGoal;

          // draw goal text
          chart.renderer.text('GOAL', baseX - 50, baseY - goalLine)
            .css({
              color: 'white',
              fontSize: '16px'
            })
            .add();

          chart.renderer.path(['M', baseX, baseY - goalLine, 'L', baseX + chart.xAxis[0].width - 30, baseY - goalLine])
            .attr({
              'stroke-width': 4,
              stroke: '#00b4ff'
            })
            .add();

          chart.renderer.circle(baseX + chart.xAxis[0].width - 25, baseY - goalLine, 5)
            .attr({
              fill: '#00b4ff'
            })
            .add();

          // chart.renderer
          chart.renderer.label('$' + maxGoal, baseX + chart.xAxis[0].width - 80, baseY - goalLine - 40, 'callout')
            .css({
              color: '#FFFFFF'
            })
            .attr({
              fill: '#00b4ff',
              r: 5,
              zIndex: 6
            })
            .add();

          // total so far text
          var point = chart.series[0].points[chart.series[0].points.length - 1];
          chart.renderer.label('$' + totalSofar, point.plotX + chart.plotLeft - 60, point.plotY + chart.plotTop - 10, 'callout')
            .css({
              color: '#FFFFFF'
            })
            .attr({
              fill: '#00b4ff',
              r: 5,
              zIndex: 6
            })
            .add();

        }, 100);

      }
    }


    // donut pie
    $scope.donutChart = {
      options: {
        chart: {
          "backgroundColor": "transparent",
          "width": $(document).width(),
          type: 'pie'
        },

        plotOptions: {
          pie: {
            borderColor: '#000000',
            innerSize: '60%'
          }
        },
      },
      series: [{
        data: [
          ['Done', 60],
          ['Not Done', 40],
        ],
        size: '60%'
      }],
      title: {
        text: 'October',
        userHTML: true
      }
    };

  }
]);