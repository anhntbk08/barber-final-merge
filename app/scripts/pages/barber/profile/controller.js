angular.module('barber')

.controller('UserProfileController', [
  '$scope',
  'Auth',
  'Location',
  'BarberServices',
  '$state',
  function ($scope, Auth, Location, BarberServices, $state) {

    // get user current location
    Location
      .getCurrentLocation()
      .then(function (pos) {
        $scope.location = {
          lat: pos.coords.latitude,
          lon: pos.coords.longitude,
        };
      });

    /*
     * Get Barber Profile info
     * assume you have user-id: 8a61d297-daaf-4fda-aff7-d3f1b94ff962
     * please pass in uid in your routing url in search page
     */
    // if ($state.params.uid) {
    BarberServices
      .getBarberInfo('dc2ada04-766f-486b-932f-62c72b72f9d7')
    // .getBarberInfo($state.params.uid)
    .then(function (barberInfo) {
      $scope.barberInfo = barberInfo.userInfo;
      $scope.barberSettings = barberInfo.settings;

      if ($scope.location && $scope.barberSettings.lat)
        $scope.distance = parseInt(Location.calculateDistance(
          $scope.location.lat,
          $scope.barberSettings.lat,
          $scope.location.lon,
          $scope.barberSettings.lon));
    });
    // }

    $scope.rate = 4;

    $scope.myInterval = -1;
    $scope.getSecondIndex = function (index) {
      if (index - $scope.slides.length >= 0)
        return index - $scope.slides.length;
      else
        return index;
    };

    $scope.slides = [{
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }, {
      image: 'images/1.png',
      text: '1'
    }];

  }
]);