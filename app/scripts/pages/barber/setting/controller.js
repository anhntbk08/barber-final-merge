(function() {
  'use strict';
  angular.module('barber')
    .controller('BarberSettingsController', ['$scope', '$rootScope', '$timeout', '$ionicLoading', 'Auth', 'UserService', 'Account', 'GeoService', 'File',
      function($scope, $rootScope, $timeout, $ionicLoading, Auth, UserService, Account, GeoService, File) {
        $scope.settings = {
          email: Auth.currentUserInfo.email,
          payment: {},
          paymentType: 'Paypal',
        };

        $scope.option = {
          avatar: ""
        }
        $scope.map = {};

        $scope.$on('mapInitialized', function(event, map) {
          UserService
            .getSettings()
            .then(function(settings) {
              $scope.settings = angular.extend($scope.settings, settings);
              $timeout(function () {$('input[name="cardNumber"]').inputCloak({type: 'credit'});});
              if (typeof $scope.settings.payment == 'string')
                $scope.settings.payment = {};
              listenSettingChange();

              if ($scope.settings.barberLocation) {
                initMapBarberMarker(map, $scope.settings.barberLocation);
              } else {
                GeoService
                  .requestUserLocation(true)
                  .then(function(location) {

                    initMapBarberMarker(map, location);
                  });
              }
            });
        });

        function initMapBarberMarker(map, location) {
          // if (!$scope.settings.barberLocation) {
          //   GeoService.addBarberLocation(location);
          // };
          $scope.settings.barberLocation = {
            latitude: location.latitude,
            longitude: location.longitude
          };

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.latitude, location.longitude),
            map: map,
            draggable: true,
            title: 'Your shop location'
          });

          map.panTo(new google.maps.LatLng(location.latitude, location.longitude));


          google.maps.event.addListener(marker, "drag", function(event) {
            // event.preventDefault();
          });

          google.maps.event.addListener(marker, "dragend", function(event) {
            $scope.settings.barberLocation.latitude = event.latLng.lat();
            $scope.settings.barberLocation.longitude = event.latLng.lng();
            GeoService.addBarberLocation($scope.settings.barberLocation);
          });
        }

        function _updateSettingToFirebase(newVal) {
          UserService
            .updateSettings(newVal)
            .then(function(error) {});
        }

        function listenSettingChange() {
          $scope.$watchCollection('settings', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase(newVal);
          });

          $scope.$watchCollection('settings.payment', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          })

          $scope.$watch('settings.schedules', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);

          $scope.$watch('settings.haircuts', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);

          $scope.$watch('settings.barberLocation', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);
        }

        $scope.cardnumberChange = function(newVal) {
          $timeout(function () {
            $scope.settings.payment.cardnumber = newVal;
          });
        }
        //avatar
        $scope.avatarUploadChange = function(newVal) {
          if (newVal) {
            File
              .getContent(newVal)
              .then(function(content) {
                $scope.settings.avatar = content;
                $rootScope.userSettings.avatar = content;
              });
          }
        };

      }
    ]);
}());