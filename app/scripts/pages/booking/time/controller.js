(function() {
  "use strict";

  angular.module('barber')
    .controller("BookingTimeController", ['$scope', 'BookingService', '$state', '$ionicLoading', 'SalonService', '$q', '$window','Auth', 'BarberService',
      function($scope, BookingService, $state, $ionicLoading, SalonService, $q, $window, Auth, BarberService) {
        var LIST_DAYS = {
          'Mon': 0,
          'Tue': 1,
          'Wed': 2,
          'Thu': 3,
          'Fri': 4,
          'Sat': 5,
          'Sun': 6
        };

        /*
          model for data-picker
        */

        $scope.currentSelectedDate = new Date();

        $scope.$watch('currentSelectedDate', function(_new) {
          if (_new) {
            $scope.shownDate = $scope.currentSelectedDate.toDateString();
          }
        });

        /*
          Get avaible time for current selected day
        */
        $scope.listAvailableTime = [];
        $scope.listBooked = []

        var getAvailableTimeForDay = function(day) {
          var barberId = $state.params.id;
          $scope.barberId = barberId;      
          if(localStorage.getItem(barberId)){
            _calculateWorkingTime(JSON.parse(localStorage.getItem(barberId)));
            _getBookedOrder(day, barberId).then(function(listBooked) {
              $scope.listBooked = listBooked || [];
              _calculateAvailableTime();
            })
          }else{
            _getBarberSetting(barberId).then(function(barberSettings) {
              localStorage.setItem(barberId, JSON.stringify(barberSettings));
              _calculateWorkingTime(barberSettings);
              _getBookedOrder(day, barberId).then(function(listBooked) {
                $scope.listBooked = listBooked || [];
                _calculateAvailableTime();
              })
            });
          }          
        }

        // calculate available time base on working slot and booked orders
        function _calculateAvailableTime() {

        }

        function _calculateWorkingTime(barberSettings) {
          var barberId = $state.params.id;
          var currentDay = moment($scope.currentSelectedDate).format('ddd');          
          var schedule = barberSettings.schedules ? barberSettings.schedules[LIST_DAYS[currentDay]] : null;
          if (!schedule) {
            $ionicLoading.show({
              template: 'There is no schedule data for this barber !!!',
              duration: 2000
            });
          }
          _getBarberBooked(barberId).then(function(barberBookings){
            _getListBookingsByBaberID(barberId).then(function(listBookings){
              $scope.listAvailableTime = schedule ? _generateSlotTime(schedule,barberBookings,listBookings) : [];
            });
          });
        }

        // generate slot time for each day depend on settings schedule               

        function _generateSlotTime(schedule,barberBookings,listBookings) {
          var counter = parseFloat(schedule.start);
          var endTime = parseFloat(schedule.end);
          var breakon = parseFloat(schedule.breakon);
          var breakoff = parseFloat(schedule.breakoff);
          var slots = [];

          function _detectBooked(detail_info,slot){  
            var flag = false;
            for(var key in detail_info.data) {
              if(slot == detail_info.data[key] && listBookings.data[key].status!='cancel'){
                flag = true;
              }
            }
            return flag;
          }
          function _detectTimeout(slot){
            var curTime = new Date();            
            var curYear = moment(curTime).format('YYYY');
            var curMonth = moment(curTime).format('MM');
            var curDate = moment(curTime).format('DD');
            var curHouse = moment(curTime).format('H');            
            var selectYear = moment($scope.currentSelectedDate).format('YYYY');
            var selectMonth = moment($scope.currentSelectedDate).format('MM');
            var selectDate = moment($scope.currentSelectedDate).format('DD');                        
            var flag = false;
            if(selectYear < curYear || selectMonth < curMonth || selectDate < curDate){
              flag = true;
            }
            if(selectYear == curYear && selectMonth == curMonth && selectDate == curDate && slot < curHouse){
              flag = true;
            }
            return flag;
          }
          while (counter < endTime) {
            if (counter < breakon || counter >= breakoff) {
              slots.push({
                timeStart: counter,
                timeEnd: counter + 1,
                booked: _detectBooked(barberBookings,counter),
                timeout: _detectTimeout(counter),
              });
            }
            counter += 1;
          }

          return slots;
        }

        // get barber settings
        function _getBarberSetting(barberid) {
          var defer = $q.defer();
          SalonService
            .getBusiness(barberid)
            .then(function(data) {
              defer.resolve(data);
            }, function(error) {
              defer.reject(error);
            });
          return defer.promise;
        }
        function _getBarberBooked(barberId){          
          var defer = $q.defer();
          var currentDay = moment($scope.currentSelectedDate).format('YYYY-MM-DD');           
          BookingService.getBarberBookingsByDate(barberId,currentDay).then(function(data) {
            defer.resolve(data);
          }, function(error) {
            defer.reject(error);
          });
          return defer.promise;
        }
        function _getListBookingsByBaberID(barberId){          
          var defer = $q.defer();
          BookingService.getListBookingsByBaberID(barberId).then(function(data) {
            defer.resolve(data);
          }, function(error) {
            defer.reject(error);
          });
          return defer.promise;
        }
             
        // get boooked list
        function _getBookedOrder(day, barberId) {
          var defer = $q.defer();
          BookingService
            .getBookedList(day, barberId)
            .then(function(data) {
              defer.resolve(data);
            }, function(error) {
              defer.reject(error);
            });

          return defer.promise;
        }

        // event listener when user select day
        $scope.formatDate = function (date) {
            function pad(n) {
                return n < 10 ? '0' + n : n;
            }

            return date && date.getFullYear()
                + '-' + pad(date.getMonth() + 1)
                + '-' + pad(date.getDate());
        };

        $scope.parseDate = function (s) {
            var tokens = /^(\d{4})-(\d{2})-(\d{2})$/.exec(s);

            return tokens && new Date(tokens[1], tokens[2] - 1, tokens[3]);
        };
        $scope.onSelectDay = function(type, day) {          
          //console.log(day);
          $scope.currentSelectedDate = new Date(day);
          getAvailableTimeForDay($scope.currentSelectedDate);
        }

        // request avaiable time for current day
        getAvailableTimeForDay($scope.currentSelectedDate);

        // event when user select a slot
        $scope.onUserSelectDateRange = function(slot) {
          var isoDate = $scope.currentSelectedDate.toISOString();
          // $window.location.href = '#/booking/cut-options/' + $state.params.id + '?date=' + isoDate + '&slot=' + slot.timeStart;
          $state.go('app.booking-cut-options', {
            id: $state.params.id,
            date: isoDate,
            slot: slot.timeStart
          });
        }
      }
    ]);
}());