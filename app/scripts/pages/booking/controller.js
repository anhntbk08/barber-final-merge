(function() {
  "use strict";

  angular.module('barber')
    .controller("BookingController", ['$scope', '$rootScope', 'SalonService', '$stateParams', '$state', 'stripe', '$filter', '$ionicLoading', '$http', 'BookingService', '$window', 'CONFIG', 'Auth', 'BarberService', 'UserService',
      function($scope, $rootScope, SalonService, $stateParams, $state, stripe, $filter, $ionicLoading, $http, BookingService, $window, CONFIG, Auth, BarberService, UserService) {

        Stripe.setPublishableKey(CONFIG.stripePublicKey);
        $scope.selectedStyle = {};
        $scope.bData = {
          date: $stateParams.date,
          startTime: $stateParams.startTime,
          endTime: $stateParams.startTime + 1
        };
        var paymentForm = {};
        var cutOptions = {};
        var sTmp = '';
        var sTmp2 = '';
        var blend = [
          "High",
          "Middle",
          "Low"
        ];
        var tapeBack = [
          "Square",
          "Round",
          "Fade"
        ];
        if (localStorage.getItem($state.params.id)) {
          setCutOptionsInfo(JSON.parse(localStorage.getItem($state.params.id)));
        } else {
          BarberService.getBarberDetail($state.params.id).then(function(detail_info) {
            if (!$.isEmptyObject(detail_info)) {
              localStorage.setItem($state.params.id, JSON.stringify(detail_info));
              setCutOptionsInfo(detail_info);
            }
          });
        }

        function setCutOptionsInfo(detail_info) {
          $rootScope.baber_info = detail_info;
          $scope.barberID = $state.params.id;
          $scope.barb = {};
          $scope.cardNumber = null;
          $scope.cardType = null;
          $scope.cardMonth = null;
          $scope.cardYear = null;
          $scope.cardCVC = null;
          $scope.transData = {};

          //BookingService.setCutPrice(150);           

          $scope.sErr = '';
          //$scope.isValid = stripe.card.validateCardNumber('4242424242424242');
          $scope.isNumValid = null;
          $scope.isExpValid = null;
          $scope.isCVCValid = null;



          $scope.specialRequests = "";

          $scope.styleVarList = detail_info.haircuts;
          // $scope.styleVar = "Fade";

          $scope.blendVarList = blend;
          $scope.blendVar = "Middle";

          $scope.topLength = 1.5;
          $scope.sideLength = 0.5;

          $scope.tapeBackVarList = tapeBack;
          $scope.tapeBackVar = "Square";
        }

        $scope.saveCutOptions = function() {
          if (!$scope.selectedStyle.styleVar) return;
          cutOptions.style = $scope.styleVarList[$scope.selectedStyle.styleVar].style;
          cutOptions.blend = $scope.blendVar;
          cutOptions.topLength = $scope.topLength;
          cutOptions.sideLength = $scope.sideLength;
          cutOptions.tapeBack = $scope.tapeBackVar;
          cutOptions.specialRequests = $scope.specialRequests;
          var date = new Date($state.params.date);
          var timestamp = moment(date).format('YYYY-MM-DD');
          var booking_key = $scope.barberID + "_" + Auth.currentUserInfo.uid + "_" + date.valueOf();
          var price = $scope.styleVarList[$scope.selectedStyle.styleVar].price;
          var now_date = Date.now();
          BookingService.setBookingInfo(booking_key, {
            'hairStyle': cutOptions,
            'price': price,
            'status': 'pending',
            'slot': $state.params.slot,
            'created_date': now_date,
            'booked_time': (new Date(date)).valueOf() + parseInt($state.params.slot) * 60 * 60 * 1000
          });
          BookingService.setBarberBooking($scope.barberID, timestamp, booking_key, $state.params.slot);
          BookingService.setUserBooking(Auth.currentUserInfo.uid, booking_key, 'pending');

          $state.go('app.booking-pay-info', {
            id: booking_key,
            barberId: $scope.barberID,
            date: $stateParams.date,
            startTime: $stateParams.slot,
            endTime: parseInt($stateParams.slot) + 1
          });
        }

        $scope.show = function() {
          $ionicLoading.show({
            template: 'Procesing Payment...'
          });
        };

        $scope.hide = function() {
          $ionicLoading.hide();
        };
        $scope.form = {};
        UserService.getSettings().then(function(settings) {
          localStorage.setItem('barber_user_info', JSON.stringify(settings));
          $scope.user_settings = settings;
          //console.log($scope.user_settings);
        });
        //XXX: booking-pay-info
        BookingService.getBookings($state.params.id).then(function(booking) {
          if (booking.data) {
            $scope.bookingInfo = booking.data;
            $scope.bookingInfo.bookingTime = {
              weekday: moment($stateParams.date).format('dddd'),
              month: moment($stateParams.date).format('MMM'),
              day: moment($stateParams.date).format('DD'),
              year: moment($stateParams.date).format('YYYY')
            };
          }
        });
        $scope.submitPayment = function(form) {
          $scope.barberID = $state.params.barberId;
          $scope.showError = true;
          // this.paymentForm.submit();
          // if (!$scope.form.paymentForm.$valid) return;

          $scope.show();
          // if (!$scope.user_settings) {
          //   $scope.user_settings = JSON.parse(localStorage.getItem('barber_user_info'));
          // }
          // Save card info into JS object
          UserService.getSettings().then(function(settings) {
            $scope.user_settings = settings;

            paymentForm.object = 'card';
            paymentForm.number = $scope.user_settings.payment.cardnumber;
            paymentForm.exp_month = $scope.user_settings.payment.cardmonth;
            paymentForm.exp_year = $scope.user_settings.payment.cardyear;
            paymentForm.cvc = $scope.user_settings.payment.cardcvc;
            paymentForm.customerName = Auth.currentUserInfo.name;
            paymentForm.description = 'CLYP - Booking for haircut';

            var postData = {
              chargeInfo: paymentForm,
              cutInfo: $scope.bData,
              bookingId: $stateParams.id,
              userId: Auth.currentUserInfo.uid
            };

            $http.post(CONFIG.clyprServerURL + '/api/booking/makeAppt', postData).
            then(function(res) {
              sTmp = 'HTTP Post Request Successful \n';
              sTmp = sTmp + 'Transaction ID: ' + res.data.confNum;
              BookingService.setCallendar(Auth.currentUserInfo.uid, {
                year: moment($stateParams.date).format('YYYY'),
                month: moment($stateParams.date).format('MM'),
                date: moment($stateParams.date).format('DD'),
                slot: $scope.bookingInfo.slot,
                baber_info: $rootScope.baber_info
              });

              $http({
                method: 'POST',
                url: CONFIG.clyprPushServerURL + '/send',
                data: {
                  ios: {
                    badge: 0,
                    alert: "You've got new booking from " + Auth.currentUserInfo.name
                  },
                  users: [$scope.barberID]
                }
              }).then(function successCallback(response) {
                console.log(response);
              }, function errorCallback(error) {
                console.log(error);
              });

              $rootScope.baber_info = false;
              $scope.hide();
              $state.go('app.home');
            }, function(err) {
              sTmp = 'HTTP Post Request Failed \n';
              sTmp = sTmp + err.data.message;
              alert(sTmp);
              $scope.hide();
            });
          });

        }

        function stripeResponseHandler(status, response) {
          if (response.error) {
            alert('Stripe Token Creation - Failed');
          } else {
            var token = response.id;
            alert('Stripe Token Creation - success');
          }
        }

        $scope.backToAppointmentPage = function() {
          $state.go('app.booking-time', {
            id: $state.params.id
          });
        }

        $scope.onChange = function(type, value) {
          $scope[type] = value;
          if (type === 'cardNumber') {
            sTmp = String(value);
            $scope.isNumValid = stripe.card.validateCardNumber(sTmp);
            $scope.cardType = stripe.card.cardType(sTmp);
          }
          if (type === 'cardMonth' || type === 'cardYear') {
            sTmp = String($scope.cardMonth);
            sTmp2 = String($scope.cardYear);
            $scope.isExpValid = stripe.card.validateExpiry(sTmp, sTmp2);
          }
          if (type === 'cardCVC') {
            sTmp = String(value);
            $scope.isCVCValid = stripe.card.validateCVC(sTmp);
          }
        };

        SalonService.getSingleBusiness($scope.barberID).then(function(barber) {
          if (barber) {
            $scope.barb = barber;
          }
        });
      }
    ]);
}());