(function() {
  "use strict";

  angular.module('barber')
    .service('AuthService', ['$firebase', '$ionicLoading',
      function($firebase, $ionicLoading) {
        var firebase = new Firebase("https://clypdev.firebaseio.com"); // Save firebase name in constants! Dont hardcode this here!!!

        var Auth = {
          googleLogin: function() {

            firebase.authWithOAuthPopup("google", function(error, authData) {
              if (error) {
                if (error.code === "TRANSPORT_UNAVAILABLE") {
                  firebase.authWithOAuthRedirect("google", function(error, authData) {
                    if (error) {
                      $ionicLoading.show({
                        template: 'Login Failed!',
                        noBackdrop: true,
                        duration: 2000
                      });
                    } else if (authData) {
                      uAuthData = authData;
                      // user authenticated with Firebase
                      $ionicLoading.show({
                        template: 'Login worked via Redirect!!!...Logged in as ' + authData.google.displayName + ' ...logging out!!!',
                        noBackdrop: true,
                        duration: 2000
                      });
                    }
                  });
                }
              } else if (authData) {
                uAuthData = authData;
                $ionicLoading.show({
                  template: 'Login worked via Popup!!!...Logged in as ' + authData.google.displayName + ' ...logging out!!!',
                  noBackdrop: true,
                  duration: 2000
                });
              }
            });

            return false;
          },

          login: function(email, password) {
            var defer = $q.defer();

            firebase.authWithPassword({
              email: email,
              password: password
            }, function(error, authData) {
              if (error) {
                defer.reject(error);
              } else {
                Auth.authData = angular.copy(authData);
                Auth.me().then(function(userInfo) {
                  defer.resolve(userInfo);
                });

              }
            });

            return defer.promise;
          },

          me: function() {
            var defer = $q.defer();

            var ref = firebase.child("users").child(Auth.authData.uid);

            ref.once("value", function(data) {
              Auth.currentUserInfo = data.val();
              defer.resolve(Auth.currentUserInfo);
            });

            return defer.promise;
          },

          checkCurrentToken: function() {
            var authData = firebase.getAuth();

            Auth.authData = authData;

            if (authData) {
              var defer = $q.defer();
              Auth.me().then(function(info) {
                defer.resolve(info);
              });
              return defer.promise;
            }

            return authData;
          },

          isAuthenticated: function() {
            return !!Auth.authData;
          },

          type: function() {
            if (Auth.currentUserInfo)
              return Auth.currentUserInfo.type;
            else
              return null;
          },

          logout: function() {
            firebase.unauth();
            Auth.authData = null;
          }
        };

        return Auth;

      }
    ]);
}());