(function() {
  'use strict';

  angular.module('barber')
    .controller('AuthController', [
      '$rootScope',
      '$scope',
      '$interval',
      '$ionicLoading',
      'Auth',
      '$state',
      '$ionicHistory',
      '$firebase',
      'Google',
      function($rootScope, $scope, $interval, $ionicLoading, Auth, $state, $ionicHistory, $firebase, Google) {

        function logoutCountDown(){
          $scope.counter = 30;
          $scope.stopCount = false;
          var count = $interval(function(){
            if ($scope.stopCount) {
              $ionicLoading.hide();
              $scope.firstTime = false;
              $scope.counter = 0;
              return $interval.cancel(count);
            }
            $scope.counter--;
            if(!$scope.counter){
              $interval.cancel(count);
              $ionicLoading.hide();
              $scope.firstTime = false;
              $rootScope.logout();
            } 
          }, 1000);
        }

        $scope.gUserLogin = function(isBarber) {
          // request authentication from google
          Google.login().then(function(uAuthData) {
            // checking in our firebase system
            Auth.checkRegistration(uAuthData.uid).then(function(data) {
              $rootScope.homeRedirect();
            }, function(error) {
              $scope.userInfoTemp = uAuthData;
              $scope.firstTime = true;
              Auth.logout();
              // logoutCountDown();
            });

          });
        }

        $scope.userType = {};
        $scope.setType = function(){
          if($scope.userInfoTemp){
            var uAuthData = $scope.userInfoTemp;
            uAuthData.type = $scope.userType.isBarber ? 'barber' : 'customer';
            uAuthData.active_code = $scope.userType.active_code;
            Auth
              .registerGoogleAccount(uAuthData)
              .then(function() {
                $scope.userInfoTemp = null;
                $scope.firstTime = false;
                $scope.stopCount = true;
                // $rootScope.homeRedirect();
                if($scope.userType.isBarber){
                  $state.go('app.barber-settings').then(function() {
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                  });
                }else{
                  $state.go('app.user-settings').then(function() {
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                  });
                }                
              },function(error){
                console.log(error);
                $scope.statusActive = error.data.error;
              });
          }
          else{
            $scope.userInfoTemp = null;
            $scope.firstTime = false;
          }
        }
      }
    ]);
}());