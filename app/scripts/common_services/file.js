(function() {

  angular.module('barber')

  .factory('File', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        getContent: function(file) {
          var defer = $q.defer();
          var reader = new FileReader();

          reader.onload = function(fileContent) {
            defer.resolve(fileContent.target.result);
          }

          reader.readAsDataURL(file);
          return defer.promise;
        }
      }
    }
  ]);
}());