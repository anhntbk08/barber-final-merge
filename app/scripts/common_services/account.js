(function() {
  angular.module('barber')

  .factory('Account', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        /*
        Register user service on firebase
       */
        register: function(email, password, name, type) {
          var defer = $q.defer();
          firebase.createUser({
            email: email,
            password: password
          }, function(error, userData) {
            if (error) {
              switch (error.code) {
                case "EMAIL_TAKEN":
                  defer.reject({
                    msg: "The new user account cannot be created because the email is already in use."
                  });
                  break;
                case "INVALID_EMAIL":
                  defer.reject({
                    msg: "The specified email is not a valid email."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error creating user:",
                    error: error
                  });
              }
            } else {

              // create user-data in users
              firebase.child('users').child(userData.uid).set({
                type: type,
                name: name
              }, function(error) {
                console.log(error);
                if (error) {
                  defer.reject(error)
                } else {
                  defer.resolve(userData);
                }
              })
            }
          });

          return defer.promise;
        },

        /*
        Change avatar service
       */
        changeAvatar: function(avatar) {
          var defer = $q.defer();
          var ref = firebase.child("users").child(Auth.authData.uid).child('avatar');

          ref.set(avatar, function(error) {
            defer.resolve(error);
          });

          return defer.promise;
        },

        /*
        Change Email
       */
        changeEmail: function(oldEmail, newEmail, password) {
          var defer = $q.defer();

          firebase.changeEmail({
            oldEmail: oldEmail,
            newEmail: newEmail,
            password: password
          }, function(error) {
            if (error) {
              switch (error.code) {
                case "INVALID_PASSWORD":
                  defer.reject({
                    msg: "The specified user account password is incorrect."
                  });
                  break;
                case "INVALID_USER":
                  defer.reject({
                    msg: "The specified user account does not exist."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error creating user:",
                    data: error
                  });
              }
            } else {
              defer.resolve(true);
            }
          });

          return defer.promise;
        },

        /*
        Change password
       */
        changePassword: function(email, oldPassword, newpassword) {
          var defer = $q.defer();

          firebase.changePassword({
            email: email,
            oldPassword: oldPassword,
            newPassword: newpassword
          }, function(error) {
            if (error) {
              switch (error.code) {
                case "INVALID_PASSWORD":
                  defer.reject({
                    msg: "The specified user account password is incorrect."
                  });
                  break;
                case "INVALID_USER":
                  defer.reject({
                    msg: "The specified user account does not exist."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error changing password:",
                    data: error
                  });
              }
            } else {
              defer.resolve("User password changed successfully!");
            }
          });

          return defer.promise;
        }
      }
    }
  ]);

}());