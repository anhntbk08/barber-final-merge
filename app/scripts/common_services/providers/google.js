(function() {
  angular.module('barber')

  .factory('Google', [
    '$q',
    'Firebase',
    'Auth',
    '$ionicLoading',
    function($q, Firebase, Auth, $ionicLoading) {
      var firebase = Firebase.getInstance();
      var clientId = '447590262163-99vrnebrbjk7lr2k9ctlvnuo28ltl270.apps.googleusercontent.com';
      var apiKey = 'AIzaSyBPjJ5YH2y5ChVniev8PVkXqIYdMyU-TYE';
      var googleScope = "email,https://www.googleapis.com/auth/calendar,https://www.googleapis.com/auth/plus.login,https://www.googleapis.com/auth/plus.me";

      return {
        /*
        Register user service on firebase
       */
        login: function() {
          var deferred = $q.defer();

          firebase.authWithOAuthPopup("google", function(error, authData) {
            if (error) {
              if (error.code === "TRANSPORT_UNAVAILABLE") {
                firebaseRef.authWithOAuthRedirect("google", function(error, authData) {
                  if (error) {
                    $ionicLoading.show({
                      template: 'Login Failed!',
                      noBackdrop: true,
                      duration: 2000
                    });
                  } else if (authData) {
                    // user authenticated with Firebase
                    deferred.resolve(authData);
                  }
                }, {
                  scope: googleScope
                });
              }
            } else if (authData) {
              deferred.resolve(authData);
            }
          }, {
            scope: googleScope
          });

          return deferred.promise;
        }

      }
    }
  ]);

}());