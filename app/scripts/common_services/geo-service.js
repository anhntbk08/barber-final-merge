(function() {
  "use strict";

  angular.module('barber')
    .factory('GeoService', ['$rootScope', '$q', '$ionicLoading', 'Firebase', 'Auth', 'DEFAULT_SEARCH_SETTINGS',
      function($rootScope, $q, $ionicLoading, Firebase, Auth, DEFAULT_SEARCH_SETTINGS) {
        var listBarbers = {},
          geoQuery = null,
          firebase = Firebase.getInstance(),
          geoFire = new GeoFire(firebase.child("geofire")),
          radiusInKm = convertMilesToKm(DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles),
          watchLocation = null;

        // TODO : break this into two function
        function requestUserLocation(isBarber) {
          var defer = $q.defer();

          if (typeof navigator !== "undefined" && typeof navigator.geolocation !== "undefined") {
            navigator.geolocation.getCurrentPosition(function(location) {

              var latitude = location.coords.latitude;
              var longitude = location.coords.longitude;

              // update user location to firebase
              var userID = Auth.currentUserInfo.uid;
              var uSettings = firebase.child("settings/").child(userID);

              uSettings.update({
                latitude: latitude,
                longitude: longitude
              }, function() {});

              if (!isBarber) {
                // instantiate the geoFire query
                geoQuery = geoFire.query({
                  center: [latitude, longitude], // don't know why using real data doesnt work
                  radius: radiusInKm
                });

                __registerGeofileListener(geoQuery);

                defer.resolve(listBarbers);
              }

              defer.resolve({
                latitude: latitude,
                longitude: longitude
              });
            }, function __errorHandler(error) {
              defer.reject("You must allow us to access location before doing any search !!");
            });

            if (watchLocation) navigator.geolocation.clearWatch(watchLocation);

            watchLocation = navigator.geolocation.watchPosition(function(location) {

              var latitude = location.coords.latitude;
              var longitude = location.coords.longitude;

              // update user location to firebase
              var userID = Auth.currentUserInfo.uid;
              var uSettings = firebase.child("settings/").child(userID);

              uSettings.update({
                latitude: latitude,
                longitude: longitude
              }, function() {});

              if (!isBarber) {
                // instantiate the geoFire query
                geoQuery.updateCriteria({
                  center: [latitude, longitude],
                });
              }

            }, function __errorHandler(error) {
              defer.reject("You must allow us to access location before doing any search !!");
            });
          } else {
            defer.reject("Your browser does not support the HTML5 Geolocation API, so this will not work.");
          }

          return defer.promise;
        }

        // TODO implement watch user location to update to firebase
        function __registerGeofileListener(geoQuery) {

          geoQuery.on("ready", function() {
            console.log('geoQuery in ready state!!');
          });

          geoQuery.on("key_entered", function(barberId, barberLocation, distanceInKm) {
            var distanceInMiles = convertKmToMiles(distanceInKm);

            firebase.child("users").child(barberId).once("value", function(dataSnapshot) {
              var barber = dataSnapshot.val();

              if (!barber) return;

              barber.distance = +distanceInMiles.toFixed(2);
              listBarbers[barberId] = barber;

              firebase.child("settings").child(barberId).once('value', function(setting) {
                listBarbers[barberId] = angular.extend(barber, setting.val());
                $rootScope.$broadcast('BARBERS_DATA_CHANGE', listBarbers);
              });
            });
          });

          geoQuery.on("key_moved", function(barberId, barberLocation, distanceInKm) {
            if (!listBarbers[barberId]) return;

            var barber = listBarbers[barberId];
            var distanceInMiles = convertKmToMiles(distanceInKm);
            barber.distance = distanceInMiles;
          });

          geoQuery.on("key_exited", function(barberId, barberLocation, distanceInKm) {
            delete listBarbers[barberId];
          });
        }

        function updateKeyDistance() {
          var listLen = Object.keys(listBarbers).length;
          var key, tlat, tlng, tloc, tdisK, tdisM;

          for (key in listBarbers) {
            tlat = listBarbers[key].latitude;
            tlng = listBarbers[key].longitude;
            tloc = [tlat, tlng];
            tdisK = GeoFire.distance(tloc, center);
            tdisM = convertKmToMiles(tdisK);
            listBarbers[key].distance = +tdisM.toFixed(2); // the + ensures the result is a number (instead of string)
          }
        }

        function addBarberLocation(location) {
          var defer = $q.defer();
          geoFire
            .set(Auth.currentUserInfo.uid, [location.latitude, location.longitude])
            .then(function() {
              console.log('Update barber ' + Auth.currentUserInfo.uid + ' location ');
              defer.resolve(true);
            }, function(error) {
              console.log(error);
              defer.resolve(false);
            });

          return defer.promise;
        }

        function updateGeofireQueryCenter(lat, lng) {
          // center = [lat, lng];
          // radiusInKm = convertMilesToKm(DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles);
          // geoQuery = geoFire.query({
          //   center: center,
          //   radius: radiusInKm
          // });
          // updateKeyDistance();
        }

        function updateGeofireQueryRadius(radiusInMiles) {
          radiusInKm = convertMilesToKm(radiusInMiles);
          geoQuery.updateCriteria({
            radius: radiusInKm
          });
        }

        function getGeofireQueryResults() {
          var deferred = $q.defer();

          deferred.resolve(listBarbers);

          return deferred.promise;
        }

        function getGeofireQueryCenter() {
          return geoQuery.center();
        }

        function convertKmToMiles(km) {
          return km * 0.621371;
        }

        function convertMilesToKm(miles) {
          return miles / 0.621371;
        }

        return {
          requestUserLocation: requestUserLocation,
          updateKeyDistance: updateKeyDistance,
          updateGeofireQueryCenter: updateGeofireQueryCenter,
          updateGeofireQueryRadius: updateGeofireQueryRadius,
          getGeofireQueryResults: getGeofireQueryResults,
          getGeofireQueryCenter: getGeofireQueryCenter,
          convertKmToMiles: convertKmToMiles,
          convertMilesToKm: convertMilesToKm,
          addBarberLocation: addBarberLocation
        };

      }
    ]);
}());