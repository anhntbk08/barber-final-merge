(function() {

  angular.module('barber')

  .factory('Auth', [
    '$http',
    '$q',
    'Firebase',
    '$timeout',
    '$state',
    '$ionicHistory',
    'CONFIG',
    function($http, $q, Firebase, $timeout, $state, $ionicHistory, CONFIG) {
      var firebase = Firebase.getInstance();

      var Auth = {
        login: function(email, password) {
          var defer = $q.defer();

          firebase.authWithPassword({
            email: email,
            password: password
          }, function(error, authData) {
            if (error) {
              defer.reject(error);
            } else {
              Auth.authData = angular.copy(authData);
              Auth.me().then(function(userInfo) {
                defer.resolve(userInfo);
              });
            }
          });

          return defer.promise;
        },

        __standardGoogleData: function(googleData) {
          var userInfo = {
            type: googleData.type,
            provider: 'google',
            uid: googleData.uid,
            token: googleData.token,
            id: googleData.google.id,
            email: googleData.google.email
          }
          angular.extend(userInfo, googleData.google.cachedUserProfile);
          userInfo.name = userInfo.given_name + " " + userInfo.family_name;
          return userInfo;
        },

        registerGoogleAccount: function(googleData) {
          var userInfo = this.__standardGoogleData(googleData);
          if (localStorage.getItem('deviceToken')) {
            userInfo.deviceToken = localStorage.getItem('deviceToken');
            // register device token with notification server
            $http({
              method: 'POST',
              url: CONFIG.clyprPushServerURL + '/subscribe',
              data: {
                token: userInfo.deviceToken,
                user: userInfo.uid,
                type: 'ios'
              }
            }).then(function successCallback(response) {
              console.log(response);
            }, function errorCallback(error) {
              console.log(error);
            });
          }

          var defer = $q.defer();
          $http({
            method: 'GET',
            url: CONFIG.clyprServerURL + '/api/clypr-code/checkValid/' + googleData.active_code,
          }).then(function successCallback(res) {
            $http({
              method: 'PUT',
              url: CONFIG.clyprServerURL + '/api/clypr-code/useCode/?code=' + googleData.active_code + "&userInfo=" + JSON.stringify(userInfo),
            }).then(function successCallback(response) {
              var usersRef = firebase.child("users").child(userInfo.uid);
              usersRef.set(userInfo, function(error, authData) {
                if (error) {
                  defer.reject(error);
                } else {
                  Auth.authData = Auth.currentUserInfo = userInfo;
                  defer.resolve(userInfo);
                }
              });
            }, function errorCallback(error) {
              defer.reject(error);
            });
          }, function errorCallback(error) {
            defer.reject(error);
          })
          return defer.promise;
        },

        me: function() {
          var defer = $q.defer();

          var ref = firebase.child("users").child(Auth.authData.uid);

          ref.once("value", function(data) {
            var info = data.val();
            Auth.currentUserInfo = info;
            defer.resolve(Auth.currentUserInfo);

            var deviceToken = localStorage.getItem('deviceToken');
            if (info.deviceToken != deviceToken) {
              info.deviceToken = deviceToken;
              // register device token with notification server
              $http({
                method: 'POST',
                url: CONFIG.clyprPushServerURL + '/subscribe',
                data: {
                  token: deviceToken,
                  user: info.uid,
                  type: 'ios'
                }
              }).then(function successCallback(response) {
                console.log(response);
              }, function errorCallback(error) {
                console.log(error);
              });

              ref.set(info);
            }
          });

          return defer.promise;
        },

        checkCurrentToken: function() {
          var defer = $q.defer();

          var authData = firebase.getAuth();
          Auth.authData = authData;

          if (authData) {
            Auth.me().then(function(info) {
              defer.resolve(info);
            });
            return defer.promise;
          } else {
            $timeout(function() {
              defer.reject(authData);
            });
          }

          return defer.promise;
        },

        isAuthenticated: function() {
          return !!Auth.authData;
        },

        type: function() {
          if (Auth.currentUserInfo)
            return Auth.currentUserInfo.type;
          else
            return null;
        },

        logout: function() {
          firebase.unauth();
          Auth.authData = Auth.currentUserInfo = null;
          $state.go('auth.signin').then(function() {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
          });
        },

        checkRegistration: function(googleId) {
          var deferred = $q.defer();
          firebase.child('users').child(googleId).once('value', function(data) {
            if (data.val() === null) {
              deferred.reject("User is not registered");
            } else {
              Auth.currentUserInfo = data.val();
              deferred.resolve(data.val());
            }
          });
          return deferred.promise;
        }
      };

      return Auth;
    }
  ]);

}());