(function() {
  angular.module('barber')

  .factory('utils', [
    '$q',
    'Firebase',
    'Auth',
    '$ionicPopup',
    function($q, Firebase, Auth, $ionicPopup) {
      return {
        object2List: function(object, keyProp) {
          var list = [];
          var keys = Object.keys(object);
          for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var temp = object[key];
            temp[keyProp] = key;
            list.push(temp);
          }
          return list;
        },
        confirm: function(title, template) {
          var confirmDialog = $ionicPopup.confirm({
            title: title,
            template: template,
            cssClass: 'dialog confirm-dialog'
          });

          return confirmDialog;
        },
        showSuccess: function(msg) {
          var type;
          this.messageToast = $.simplyToast(" <i class='fa fa-check'></i>" + msg, type = 'success', {
            delay: 3000,
            align: 'center',
            allowDismiss: true
          });
          return setTimeout((function(_this) {
            return function() {
              if (_this.messageToast) {
                $.simplyToast.remove(_this.messageToast);
                return _this.messageToast = null;
              }
            };
          })(this), 5000);
        },
        showError: function(msg) {
          if (this.lastMsg === msg) {
            return;
          }
          if (this.errorToast) {
            $.simplyToast.remove(this.errorToast);
            this.errorToast = null;
          }
          this.errorToast = $.simplyToast(msg + " ", 'danger', {
            align: 'center'
          });
          setTimeout((function(_this) {
            return function() {
              _this.lastMsg = "";
              $.simplyToast.remove(_this.errorToast);
              return _this.errorToast = null;
            };
          })(this), 5000);
          return this.lastMsg = msg;
        },
      }
    }
  ]);

}());