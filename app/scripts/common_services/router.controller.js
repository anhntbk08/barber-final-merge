(function() {
  angular.module('barber')

  .factory('Router', [
    '$http',
    '$q',
    'OUT_APP_STATES',
    'IN_APP_STATES',
    '$rootScope',
    '$state',
    'Auth',
    'HOME_PAGE',
    '$timeout',
    function($http, $q, OUT_APP_STATES, IN_APP_STATES, $rootScope, $state, Auth, HOME_PAGE, $timeout) {

      var hasAccessRight = function(stateName, type) {
        return IN_APP_STATES[stateName] == type || IN_APP_STATES[stateName].indexOf(type) >= 0;
      }

      /*
      Change state start handler
    */
      var handlerChangeStart = function(events, prams, status) {

        if ($rootScope.checkingCurrentUser) {
          Auth.oldRouter = {
            name: prams,
            parameters: status
          };
          return events.preventDefault();
        }

        if (!Auth.isAuthenticated()) {
          if (OUT_APP_STATES[prams.name])
            return true;
          else {
            $state.go('auth.signin');
          }
        }

        // stop routing when can't get me info
        if (!Auth.type()) {
          Auth.oldRouter = {
            name: prams,
            parameters: status
          };
          return events.preventDefault();
        }

        /*
         * Go to home page when user is logged in and current state is sigin, signup
         */
        if (OUT_APP_STATES[prams.name]) {
          if (Auth.isAuthenticated())
            $timeout(function() {
              $state.go(HOME_PAGE[Auth.type()]);
            })
          else
            return true;
        } else {
          /*
           * Redirect when trying to access page that don't have permisison
           * for example an customer can't go to barber pages
           */
          /* 
        if (Auth.isAuthenticated() && !hasAccessRight(prams.name, Auth.type())) {
          events.preventDefault();
          $state.go(HOME_PAGE[Auth.type()]);
        }
        */
        }

        return true;

      }

      return {
        registerRouterChecker: function() {
          $rootScope.$on('$stateChangeStart', handlerChangeStart);

          $rootScope.$on('$stateChangeSuccess', function(events, prams, status) {

          });
        }
      }
    }
  ]);

}());