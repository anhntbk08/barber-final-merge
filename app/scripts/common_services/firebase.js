(function() {

  angular.module('barber')

  .factory('Firebase', [
    '$http',
    '$q',
    function($http, $q) {
      return {
        getInstance: function() {
          if (!this.firebase) {
            this.firebase = new Firebase("https://clypr-7.firebaseio.com/");
          }

          return this.firebase;
        }
      };
    }
  ]);

}());