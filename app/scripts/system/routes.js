(function() {
  "use strict";
  angular.module('barber')
    .config(['$stateProvider', '$urlRouterProvider',
      function($stateProvider, $urlRouterProvider) {

        $stateProvider
          .state('auth', {
            url: "/auth",
            abstract: true,
            templateUrl: "scripts/pages/login/templates/auth.html"
          })
          .state('auth.signin', {
            url: "/signin",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-signin.html",
                controller: "AuthController"
              }
            }
          })
          .state('auth.noGoogleLogin', {
            url: "/noGoogleLogin",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-noGoogleLogin.html",
                controller: "AuthController"
              }
            }
          })
          .state('auth.testPage', {
            url: "/testPage",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-testPage.html",
                controller: "AuthController"
              }
            }
          })
          .state('reg', {
            url: "/reg",
            abstract: true,
            templateUrl: "scripts/pages/register/reg.html"
          })
          .state('reg.userBasic', {
            url: "/userBasic",
            views: {
              'regUser': {
                templateUrl: "scripts/pages/register/reg-userBasic.html",
                controller: "RegController"
              }
            }
          })
          .state('reg.userPref', {
            url: "/userPref",
            views: {
              'regUser': {
                templateUrl: "scripts/pages/register/reg-userPref.html",
                controller: "RegController"
              }
            }
          })
          .state('tut', {
            url: "/tut",
            abstract: true,
            templateUrl: 'scripts/pages/tut/tut.html'
          })
          .state('tut.one', {
            url: "/one",
            views: {
              'tutorial': {
                templateUrl: "scripts/pages/tut/tut-one.html",
                controller: "TutController"
              }
            }
          })
          .state('tut.two', {
            url: "/two",
            views: {
              'tutorial': {
                templateUrl: "scripts/pages/tut/tut-two.html",
                controller: "TutController"
              }
            }
          })
          .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "scripts/system/menu.html",
            controller: 'MenuController'
          })
          .state('app.home', {
            url: "/home",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/home/view.html",
                controller: "HomeController"
              }
            }
          })
          .state('app.barber-reviews', {
            url: "/barber/reviews/:id",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/barber/review/view.html",
                controller: "BarberReviewsController"
              }
            }
          })
          .state('app.user-settings', {
            url: "/user/settings",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/user/setting/view.html",
                controller: "UserSettingsController"
              }
            }
          })
          .state('app.contact', {
            url: "/user/contact",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/contact/view.html",
                controller: "ContactController"
              }
            }
          })
          .state('app.about', {
            url: "/about",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/aboutUs/about.html"
              }
            }
          })
          .state('app.user-appointments', {
            url: "/user/appointments",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/user/appointments/view.html',
                controller: 'UserAppointmentsController'
              }
            }
          })
          .state('app.user-booking-history', {
            url: "/user/booking-history",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/user/booking_history/view.html",
                controller: "UserBookingHistory"
              }
            }
          })
        //X: barber
        .state('app.barber-settings', {
          url: "/barber/settings",
          views: {
            'menuContent': {
              templateUrl: 'scripts/pages/barber/setting/view.html',
              controller: 'BarberSettingsController'
            }
          }
        })
          .state('app.barber-appointments', {
            url: "/barber/appointments",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/appointments/view.html',
                controller: 'BarberAppointmentsController'
              }
            }
          })
          .state('app.barber-recent-cuts', {
            url: "/barber/barber-recent-cuts",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/recent_cuts/view.html',
                controller: 'BarberRecentCutsController'
              }
            }
          })
          .state('app.barber-profile', {
            url: "/barber/profile",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/profile/view.html',
                controller: 'BarberProfileController'
              }
            }
          })
          .state('app.barber-details', {
            url: "/barber-details/:id",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/barber/details/view.html",
                controller: "BarberDetailsController"
              }
            }
          })
          .state('salons', {
            url: "/salon",
            abstract: true,
            templateUrl: "scripts/pages/salon/view.html",
            controller: "BarberSalonController"
          })
          .state('salons.results', {
            url: "/results",
            views: {
              'salonsResults': {
                templateUrl: "scripts/pages/salon/results/view.html",
              }
            }
          })
          .state('salons.options', {
            url: "/options",
            views: {
              'salonsOptions': {
                templateUrl: "scripts/pages/salon/options/view.html",
              }
            }
          })
        // .state('booking', {
        //   url: "/booking",
        //   abstract: true,
        //   templateUrl: "scripts/pages/booking/view.html"
        // })
        .state('app.booking-time', {
          url: "/booking-time/:id",
          views: {
            'menuContent': {
              templateUrl: "scripts/pages/booking/time/view.html",
              controller: "BookingTimeController"
            }
          }
        })
          .state('app.booking-cut-options', {
            url: "/booking-cut-options/:id?date&slot",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/cut_options/view.html",
                controller: "BookingController"
              }
            }
          })
          .state('app.booking-pay-info', {
            url: "/booking-pay-info/:id?date&startTime?barberId",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/pay_info/view.html",
                controller: "BookingController"
              }
            }
          })
          .state('app.booking-confirm', {
            url: "/booking-confirm",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/confirm/view.html",
                controller: "BookingController"
              }
            }
          });
        // $urlRouterProvider.otherwise('/app/home');
      }
    ]).run(['$ionicPlatform', 'GeoService', '$rootScope', '$ionicLoading', '$ionicHistory', '$state', '$http', 'Router', 'Auth', '$timeout', '$ionicPopup', '$cordovaPush',
      function($ionicPlatform, GeoService, $rootScope, $ionicLoading, $ionicHistory, $state, $http, Router, Auth, $timeout, $ionicPopup, $cordovaPush) {
        Router.registerRouterChecker();
        $rootScope.checkingCurrentUser = true;

        var iosConfig = {
          "badge": true,
          "sound": true,
          "alert": true,
        };

        document.addEventListener("deviceready", function() {
          $cordovaPush.register(iosConfig).then(function(deviceToken) {
            localStorage.setItem('deviceToken', deviceToken);

          }, function(err) {
            alert("Registration error: " + err)
          });


          // $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
          //   if (notification.alert) {
          //     navigator.notification.alert(notification.alert);
          //   }

          //   if (notification.sound) {
          //     var snd = new Media(event.sound);
          //     snd.play();
          //   }

          //   if (notification.badge) {
          //     $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
          //       // Success!
          //     }, function(err) {
          //       // An error occurred. Show a message to the user
          //     });
          //   }
          // });

          // // WARNING! dangerous to unregister (results in loss of tokenID)
          // $cordovaPush.unregister(options).then(function(result) {
          //   // Success!
          // }, function(err) {
          //   // Error
          // });
        });

        $ionicPlatform.ready(function() {
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();
          }



          // check user login 
          Auth.checkCurrentToken().then(function(data) {
            $rootScope.checkingCurrentUser = false;
            if (Auth.oldRouter) {
              console.log(Auth.oldRouter);
              $state.go(Auth.oldRouter.name.name, Auth.oldRouter.parameters);
            } else {
              $state.go('app.home');
            }

          }, function() {
            $rootScope.checkingCurrentUser = false;
            $state.go('auth.signin');
          });

        });

        // $rootScope.saveInfo = function(uData) {
        //   //This field is obtained from the Firebase data. This can be updated with Google+ data
        //   $http.defaults.headers.common.Authorization = 'Bearer ' + Settings.userAuthData.google.accessToken;
        // };

        // $rootScope.clearUserInfo = function() {
        //   //There must be a better way to do this! Perhaps looping through $rootScope user variables and setting them all to null
        //   $http.defaults.headers.common.Authorization = null;
        // };

        $rootScope.homeRedirect = function() {
          $state.go('app.home').then(function() {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
          });
        };

        $rootScope.logout = function() {
          Auth.logout();

          // work around for logout authentication from google
          if (window.location.href.indexOf('http') == 0) {

            // for web version
            var width = $('body').width();
            var height = $('body').height();
            var win = window.open("https://accounts.google.com/logout", "", "width=" + width + ",height=" + height);
            setTimeout(function() {
              win.close();
            }, 500);
          } else {

            // for app version
            var browser = window.open('https://accounts.google.com/logout', '_blank', 'location = no');
            browser.addEventListener('loadstop', function(res) {
              if (res.url.indexOf('http://www.google.com.vn/accounts/Logout2') >= 0)
                browser.close();
            });
          }


        };

        $rootScope.goHome = function() {
          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();
          $state.go('app.home');
        };

        $rootScope.goBack = function() {
          // $ionicHistory.goBack();
          window.history.back();
        };

        $rootScope.comingSoon = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
          var alertPopup = $ionicPopup.alert({
            title: 'Coming soon',
            template: "We're still working on this. We'll keep you posted."
          });
          alertPopup.then(function(res) {

          });
        };

        $rootScope.reviewComingSoon = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
          var alertPopup = $ionicPopup.alert({
            title: 'Coming soon',
            template: "We're still working on this. We'll keep you posted."
          });
          alertPopup.then(function(res) {

          });
        };

        $rootScope.noAnimate = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
        };
      }
    ]);
}());