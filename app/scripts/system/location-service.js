(function() {
  "use strict";

  angular.module('barber')
    .factory('LocationService', ['$q', '$ionicLoading', 'Auth',
      function($q, $ionicLoading, Auth) {
        var autoUpdateLocation = true,
          hasGeoLocation = ("geolocation" in navigator),
          geo = navigator.geolocation,
          watchId = null,
          lastPosition = null,
          geo_options = {
            enableHighAccuracy: true,
            maximumAge: 30000,
            timeout: 27000
          };

        // console.log("geo = " + hasGeoLocation);
        // init();

        function init() {
          if (hasGeoLocation) {
            navigator.geolocation.getCurrentPosition(savePosition);
            watchId = navigator.geolocation.watchPosition(savePosition, errorPosition, geo_options);
          }
        }

        function stop() {
          if (watchId) {
            navigator.geolocation.clearWatch(watchId);
            watchId = null;
          }
        }

        function getCurrentPosition() {
          $ionicLoading.show({
            template: 'Get Current Position...',
            noBackdrop: true,
            duration: 2000
          });
          var deferred = $q.defer();
          if (lastPosition) {
            setTimeout(function() {
              deferred.resolve(lastPosition);
            });
          } else {
            navigator.geolocation.getCurrentPosition(function(position) {
              return deferred.resolve(position);
            }, function(err) {
              return deferred.reject(null);
            });
          }
          return deferred.promise;
        }

        function getLastPosition() {
          return lastPosition;
        }

        function pauseUpdates() {
          autoUpdateLocation = false;
        }

        function resumeUpdates() {
          autoUpdateLocation = true;
        }

        function savePosition(position) {
          var loc = position.coords.latitude + "%3A" + position.coords.longitude;
          lastPosition = position;
          if (watchId) {
            navigator.geolocation.clearWatch(watchId);
            watchId = null;
          } else if (autoUpdateLocation) {
            console.log(this);
            this.location = loc;
          }
        }

        function errorPosition() {
          console.log("Error received in geolocation");
        }

        return {
          init: init,
          stop: stop,
          getCurrentPosition: getCurrentPosition,
          getLastPosition: getLastPosition,
          pauseUpdates: pauseUpdates,
          resumeUpdates: resumeUpdates
        };
      }
    ]);
}());