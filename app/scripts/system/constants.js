(function() {
  "use strict";

  angular.module('barber')
    .constant("YP_BASE_ADDRESS", "http://pubapi.yp.com/search-api/search/devapi/search?format=json&sort=distance&searchloc=")
    .constant("DETAILS_ADDRESS", "http://api2.yp.com/listings/v1/details?format=json&listingid=")
    .constant("YP_API_KEY", "w4ycm9k9ct")
    .constant('HOME_PAGE', {
      'barber': 'app.home',
      'customer': 'app.home',
    })
    .constant('IN_APP_STATES', {
      'app.barber-settings': "barber",
      'app.user-appointments': "customer",
      //'barber-appointments': "barber",
      //'barber-statistics': "barber",
      'app.home': ["customer", 'barber'],
      'app.settings': "customer"
    })
    .constant('OUT_APP_STATES', {
      "auth.signin": "Login State"
    })
    .constant('DEFAULT_SEARCH_SETTINGS', {
      searchRadiusInMiles: 50
    })
    .constant('CONFIG', {
      stripePublicKey: 'sk_test_BQokikJOvBiI2HlWgH4olfQ2',      
      clyprServerURL: 'http://103.253.146.118:8100',
      clyprPushServerURL: 'http://devdobusiness.com:8989',
      // clyprPushServerURL: 'http://localhost:8000',
      // clyprServerURL: 'http://localhost:8100'
    });

}());