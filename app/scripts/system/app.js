(function() {
  "use strict";

  angular.module('barber', [
    'ionic',
    'ngCordova',
    'ngMap',
    'firebase',
    'ngOrderObjectBy',
    'ion-google-place',
    'tabSlideBox',
    'angular-stripe',
    'mp.datePicker',
    'ui.calendar',
    'ui.bootstrap',
  ]);

}());