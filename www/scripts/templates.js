angular.module('barber').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('scripts/pages/aboutUs/about.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "        <h1 class=\"title\">About</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <div class=\"card\">\r" +
    "\n" +
    "            <div class=\"item item-divider item-dark text-center\">\r" +
    "\n" +
    "                <h1>Coffee!</h1>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"item text-center\">\r" +
    "\n" +
    "                version 0.8<br/><br/>\r" +
    "\n" +
    "                designed & developed by<br/>\r" +
    "\n" +
    "                the Rockncoder<br/> <br/>\r" +
    "\n" +
    "                Built using: <br/>\r" +
    "\n" +
    "                JavaScript, AngularJS, Cordova, & Ionic<br/><br/>\r" +
    "\n" +
    "                And the following web services:<br/>\r" +
    "\n" +
    "                YP.com and Google Maps\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/appointments/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"barberAppointmentes\" cache-view=\"false\">\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "    <h1 class=\"title\">Barber Appointments</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-content>\r" +
    "\n" +
    "    <div class=\"wrapper\">\r" +
    "\n" +
    "        <ion-list>          \r" +
    "\n" +
    "          <div class=\"ubh_list\">\r" +
    "\n" +
    "            <div ng-repeat=\"booking in bookings | orderBy:'-created_date'\">\r" +
    "\n" +
    "              <div class=\"item row\">\r" +
    "\n" +
    "                <div class=\"col col-20\" auto-resize-image>\r" +
    "\n" +
    "                  <img class=\"avata\" src=\"{{booking.avatar}}\"/>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col col-40 b-r positive-border\">\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">{{booking.given_name}} {{booking.family_name}}</span></p>\r" +
    "\n" +
    "                  <!-- <p><span class=\"energized\">{{booking.barbershop}}</span> - {{booking.address}} {{booking.zipcode}}</p> -->\r" +
    "\n" +
    "                  <p ng-if=\"booking.phonenumber\">- {{booking.phonenumber}}</p>\r" +
    "\n" +
    "                  <div class=\"line\"></div>\r" +
    "\n" +
    "                  <p>\r" +
    "\n" +
    "                    <strong ng-if=\"booking.slot < 12\"> {{ booking.slot }} AM </strong>\r" +
    "\n" +
    "                    <strong ng-if=\"booking.slot > 12\"> {{ booking.slot-12 }} PM </strong> {{booking.bookingTime.weekday}}<br/>\r" +
    "\n" +
    "                    {{booking.bookingTime.month}} {{booking.bookingTime.day}} {{booking.bookingTime.year}}</p>\r" +
    "\n" +
    "                  <p><span class=\"assertive-bg light p-3-8\">{{booking.status}}</span></p>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col col-40\">\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Blend:</span> {{booking.hairStyle.blend}}</p>\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Side Length:</span> {{booking.hairStyle.sideLength}}</p>\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Style:</span> {{booking.hairStyle.style}}</p>\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Tape Back:</span> {{booking.hairStyle.tapeBack}}</p>                  \r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Top Length:</span> {{booking.hairStyle.topLength}}</p>\r" +
    "\n" +
    "                  <p><span class=\"font-bold\">Special requests:</span> {{booking.hairStyle.specialRequests}}</p>\r" +
    "\n" +
    "                  <p ng-if=\"booking.price\"><span class=\"assertive\">${{booking.price}}</span></p>\r" +
    "\n" +
    "                </div>         \r" +
    "\n" +
    "                <!-- <div class=\"status ion-help-circled\"></div>  -->  \r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "          <div class=\"ubh_empty bar bar-header bar-calm\" ng-if=\"!userBookings\">\r" +
    "\n" +
    "            <h1 class=\"title\">You don't have any pending appointments</h1>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</ion-content>\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/barber/details/map/view.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-energized  item-input-inset\">\r" +
    "\n" +
    "        <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "        <h1 class=\"title\">Coffee Shops</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <ion-content  map zoom=\"18\"\r" +
    "\n" +
    "                 disable-default-u-i=\"false\"\r" +
    "\n" +
    "                 disable-double-click-zoom=\"false\"\r" +
    "\n" +
    "                 draggable=\"true\"\r" +
    "\n" +
    "                 draggable-cursor=\"help\"\r" +
    "\n" +
    "                 dragging-cursor=\"move\"\r" +
    "\n" +
    "                 keyboard-shortcuts=\"false\"\r" +
    "\n" +
    "                 max-zoom=\"20\"\r" +
    "\n" +
    "                 min-zoom=\"8\"\r" +
    "\n" +
    "                 tilt=\"0\"\r" +
    "\n" +
    "                 map-type-id=\"ROADMAP\">\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/details/view.html',
    "<style type=\"text/css\">    \r" +
    "\n" +
    "   .avatar{width: 23vw; height: 23vw; overflow: hidden; margin: auto;}    \r" +
    "\n" +
    "   .avatar img {width: 100%; border-radius: 0px; border-radius: 5px;}\r" +
    "\n" +
    "   #baberInfo.zoom{overflow: visible; z-index: 10;}\r" +
    "\n" +
    "   #baberInfo.zoom .avatar{overflow: visible;}\r" +
    "\n" +
    "   #baberInfo.zoom .avatar img{position: relative; top: 100%; -webkit-transform: scale(3); -moz-transform: scale(3); -ms-transform: scale(3); transform: scale(3);}\r" +
    "\n" +
    "</style>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "    <h1 class=\"title\">Barber Information</h1>\r" +
    "\n" +
    "    <button ng-click=\"$root.goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<ion-pane>\r" +
    "\n" +
    "    <ion-content has-header=\"true\">\r" +
    "\n" +
    "        <div class=\"card\">\r" +
    "\n" +
    "            <div class=\"item item-divider text-center\">\r" +
    "\n" +
    "                <!-- <h2>{{biz.businessName}}</h2> -->\r" +
    "\n" +
    "                <h2>{{biz.given_name}} {{biz.family_name}}</h2>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"item item-profile text-center\" toggle-class=\"zoom\" id=\"baberInfo\">\r" +
    "\n" +
    "                <div class=\"avatar\"><img src=\"{{biz.avatar}}\"></div>\r" +
    "\n" +
    "                <div><strong>{{biz.name}}</strong></div>\r" +
    "\n" +
    "                <div><strong>{{biz.address}}</strong></div>\r" +
    "\n" +
    "                <div><strong>{{biz.zipcode}}</strong></div>                \r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <!-- <div class=\"item  text-center\">\r" +
    "\n" +
    "                <a class=\"item\" href=\"tel:{{biz.phone}}\" style=\"font-size: 150%;\">\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-telephone\" style=\"font-size: 150%;\"></i>\r" +
    "\n" +
    "                    Call:  {{biz.phone}}\r" +
    "\n" +
    "                </a>\r" +
    "\n" +
    "            </div> -->\r" +
    "\n" +
    "            <div class=\"item  text-center\">\r" +
    "\n" +
    "                <a class=\"item\" style=\"font-size: 150%;\" ng-click=\"reviewComingSoon()\">\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-star\" style=\"font-size: 100%;\"></i>\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-star\" style=\"font-size: 100%;\"></i>\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-star\" style=\"font-size: 100%;\"></i>\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-star-half\" style=\"font-size: 100%;\"></i>\r" +
    "\n" +
    "                    <i class=\"icon ion-ios-star-outline\" style=\"font-size: 100%;\"></i>\r" +
    "\n" +
    "                    <br>\r" +
    "\n" +
    "                    0 Reviews\r" +
    "\n" +
    "                    <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "                </a>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"item  text-center\">\r" +
    "\n" +
    "                <a class=\"item\" ui-sref=\"app.booking-time({id: barberId})\" style=\"font-size: 150%;\">\r" +
    "\n" +
    "                    <i class=\"icon ion-calendar\" style=\"font-size: 150%;\"></i>\r" +
    "\n" +
    "                    Book Appointment\r" +
    "\n" +
    "                </a>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"card\" map zoom=\"18\" ng-init=\"initMap()\"\r" +
    "\n" +
    "             tilt=\"0\"\r" +
    "\n" +
    "             map-type-id=\"ROADMAP\" style=\" height: 300px;\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-pane>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/profile/view.html',
    "\r" +
    "\n" +
    "<style type=\"text/css\">\r" +
    "\n" +
    "span.update-thumb {\r" +
    "\n" +
    "    position: fixed;\r" +
    "\n" +
    "    top: 100px;\r" +
    "\n" +
    "    right: 0;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "i.inv {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    max-width: 40px;\r" +
    "\n" +
    "    opacity: 0;\r" +
    "\n" +
    "    position: relative;\r" +
    "\n" +
    "    z-index: 2;\r" +
    "\n" +
    "    overflow: hidden;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "span.update-thumb:before {\r" +
    "\n" +
    "    content: \"\\f03e\";\r" +
    "\n" +
    "    font-family: icon;\r" +
    "\n" +
    "    position: relative;\r" +
    "\n" +
    "    z-index: -1;\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    margin-right: -40px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "</style>\r" +
    "\n" +
    "<div class=\"wrapper\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div id=\"logo\">\r" +
    "\n" +
    "        <a class=\"backlink\" href=\"./homeu.html\">\r" +
    "\n" +
    "            <span class=\"btn gray\">BACK</span>\r" +
    "\n" +
    "        </a>\r" +
    "\n" +
    "        <img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" />\r" +
    "\n" +
    "       \r" +
    "\n" +
    "        <button class=\"btn btn-info header-btn-right\">Book</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div id=\"barber-profile\">\r" +
    "\n" +
    "        <div class=\"thumb-container\">\r" +
    "\n" +
    "            <img class=\"rounded thumb\"  src=\"{{barberInfo.avatar}}\" height=\"100\" width=\"100\" alt=\"Outlet Thumb\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <i class=\"fa fa-check-circle-o fa-2 check-profile-thumb op-06\"></i>\r" +
    "\n" +
    "        <h1>{{barberInfo.name}}</h1>\r" +
    "\n" +
    "        <h2>\r" +
    "\n" +
    "            <rating ng-model=\"rate\" max=\"5\" readonly=\"true\" ></rating>\r" +
    "\n" +
    "        </h2>\r" +
    "\n" +
    "        <h4>\r" +
    "\n" +
    "            <i class=\"fa fa-map-marker fa-2\"></i>\r" +
    "\n" +
    "            {{distance}} km(s)\r" +
    "\n" +
    "        </h4>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"wrapper profile-wrapper\" id=\"settings\">\r" +
    "\n" +
    "    <div class=\"row general-info\" >\r" +
    "\n" +
    "        <div class=\"col-xs-4 text-center b-r no-b-t mh-75 \">\r" +
    "\n" +
    "            <h1 class=\"fz-20\">{{barberSettings.haircuts[0].style}}</h1>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <h2 class=\"gray-text barber-profile-label\">Specialty</h2>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-xs-4 text-center b-r no-b-t mh-75\">\r" +
    "\n" +
    "            <h1>${{barberSettings.haircuts[0].price}}</h1>\r" +
    "\n" +
    "            <h2 class=\"gray-text barber-profile-label\">Price</h2>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-xs-4 text-center mh-75\">\r" +
    "\n" +
    "            <h1 class=\"fz-20\">{{barberSettings.haircuts[0].time}} minutes</h1>\r" +
    "\n" +
    "            <h2 class=\"gray-text barber-profile-label\">Time</h2>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div >\r" +
    "\n" +
    "        <slick infinite=false speed=300 slides-to-show=3 touch-move=false init-onload=true data=\"slides\" class=\"slick-slider slider multiple-items\">\r" +
    "\n" +
    "            <!-- <img ng-repeat=\"slide in slides\" ng-src=\"{{slide.image}}\" /> -->\r" +
    "\n" +
    "            <div ng-repeat=\"slide in slides\">\r" +
    "\n" +
    "                <img class=\"carousel-item\"  ng-src=\"{{slide.image}}\" />\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </slick>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/recent_cuts/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"barberRecentCuts\" cache-view=\"false\">\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "    <h1 class=\"title\">Recent Cuts</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-content id=\"recentCut\">\r" +
    "\n" +
    "    <div class=\"wrapper\">\r" +
    "\n" +
    "        <ion-list>    \r" +
    "\n" +
    "          <div class=\"button-bar\" id=\"status_button\">\r" +
    "\n" +
    "            <button class=\"button button-positive\" ng-click=\"show_list('done',$event,10)\">Past</button>                            \r" +
    "\n" +
    "            <button class=\"button button-dark\" ng-click=\"show_list('cancel',$event,10)\">Cancelled</button>\r" +
    "\n" +
    "          </div>      \r" +
    "\n" +
    "          <div class=\"ubh_list\">\r" +
    "\n" +
    "            <div ng-repeat=\"booking in bookings | orderBy:'-created_date'\">\r" +
    "\n" +
    "              <div class=\"ubh_item {{booking.status}}\">\r" +
    "\n" +
    "                <div class=\"item row\">\r" +
    "\n" +
    "                  <div class=\"col col-20\" auto-resize-image>\r" +
    "\n" +
    "                    <img class=\"avata\" src=\"{{booking.avatar}}\"/>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col col-40 b-r positive-border\">\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">{{booking.given_name}} {{booking.family_name}}</span></p>\r" +
    "\n" +
    "                    <!-- <p><span class=\"energized\">{{booking.barbershop}}</span> - {{booking.address}} {{booking.zipcode}}</p> -->\r" +
    "\n" +
    "                    <p ng-if=\"booking.phonenumber\">- {{booking.phonenumber}}</p>\r" +
    "\n" +
    "                    <div class=\"line\"></div>\r" +
    "\n" +
    "                    <p>{{booking.slot}} {{booking.bookingTime.weekday}} {{booking.bookingTime.month}} {{booking.bookingTime.day}} {{booking.bookingTime.year}}</p>\r" +
    "\n" +
    "                    <p><span class=\"assertive-bg light p-3-8\">{{booking.status}}</span></p>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col col-40\">\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Blend:</span> {{booking.hairStyle.blend}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Side Length:</span> {{booking.hairStyle.sideLength}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Style:</span> {{booking.hairStyle.style}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Tape Back:</span> {{booking.hairStyle.tapeBack}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Top Length:</span> {{booking.hairStyle.topLength}}</p>\r" +
    "\n" +
    "                    <p ng-if=\"booking.price\"><span class=\"assertive\">${{booking.price}}</span></p>\r" +
    "\n" +
    "                  </div>         \r" +
    "\n" +
    "                  <!-- <div class=\"status ion-help-circled\"></div>  -->  \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "          <div class=\"ubh_empty bar bar-header bar-calm\" ng-if=\"!userBookings\">\r" +
    "\n" +
    "            <h1 class=\"title\">You do not have any orders</h1>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</ion-content>\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/barber/review/view.html',
    "<ion-view view-title=\"Barber Reviews\">\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <h2>Review Page for {{barb.firstName}} {{barb.lastName}}</h2>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/setting/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"barberSettings\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "        <h1 class=\"title\">Settings</h1>\r" +
    "\n" +
    "        <button ng-click=\"$root.goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Email </div>\r" +
    "\n" +
    "                <input type=\"email\" name=\"email\"  placeholder=\"Enter e-mail\" ng-model=\"settings.email\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Avatar </div>\r" +
    "\n" +
    "                <input type=\"file\" name=\"update-thumb\"  onchange=\"angular.element(this).scope().avatarUploadChange(this.files[0])\">                \r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Specialty </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"specialty\"  placeholder=\"Enter specialty\" ng-model=\"settings.haircuts[0].style\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Specialty Price </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"price\"  placeholder=\"Enter Price\" ng-model=\"settings.haircuts[0].price\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Specialty Time </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.haircuts[0].time\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"10\">10</option>\r" +
    "\n" +
    "                    <option value=\"15\">15</option>\r" +
    "\n" +
    "                    <option value=\"20\">20</option>\r" +
    "\n" +
    "                    <option value=\"25\">25</option>\r" +
    "\n" +
    "                    <option value=\"30\">30</option>\r" +
    "\n" +
    "                    <option value=\"40\">40</option>\r" +
    "\n" +
    "                    <option value=\"50\">50</option>\r" +
    "\n" +
    "                    <option value=\"60\">60</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Second Hair </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"storename\" placeholder=\"Second Hair Style\" ng-model=\"settings.haircuts[1].style\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Second Hair Price </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"storename\" placeholder=\"Second Hair Price\" ng-model=\"settings.haircuts[1].price\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Second Time </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.haircuts[1].time\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"10\">10</option>\r" +
    "\n" +
    "                    <option value=\"15\">15</option>\r" +
    "\n" +
    "                    <option value=\"20\">20</option>\r" +
    "\n" +
    "                    <option value=\"25\">25</option>\r" +
    "\n" +
    "                    <option value=\"30\">30</option>\r" +
    "\n" +
    "                    <option value=\"40\">40</option>\r" +
    "\n" +
    "                    <option value=\"50\">50</option>\r" +
    "\n" +
    "                    <option value=\"60\">60</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Third Hair </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"storename\" placeholder=\"Third Hair Style\" ng-model=\"settings.haircuts[2].style\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Third Hair Price </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"storename\" placeholder=\"Third Hair Price\" ng-model=\"settings.haircuts[2].price\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Third Time </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.haircuts[2].time\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"10\">10</option>\r" +
    "\n" +
    "                    <option value=\"15\">15</option>\r" +
    "\n" +
    "                    <option value=\"20\">20</option>\r" +
    "\n" +
    "                    <option value=\"25\">25</option>\r" +
    "\n" +
    "                    <option value=\"30\">30</option>\r" +
    "\n" +
    "                    <option value=\"40\">40</option>\r" +
    "\n" +
    "                    <option value=\"50\">50</option>\r" +
    "\n" +
    "                    <option value=\"60\">60</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Schedule </div>\r" +
    "\n" +
    "                <select ng-model=\"currentSettingDay\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"\" disabled selected>Select Day</option>\r" +
    "\n" +
    "                    <option value=\"0\">Monday</option>\r" +
    "\n" +
    "                    <option value=\"1\">Tuesday</option>\r" +
    "\n" +
    "                    <option value=\"2\">Wednesday</option>\r" +
    "\n" +
    "                    <option value=\"3\">Thursday</option>\r" +
    "\n" +
    "                    <option value=\"4\">Friday</option>\r" +
    "\n" +
    "                    <option value=\"5\">Saturday</option>\r" +
    "\n" +
    "                    <option value=\"6\">Sunday</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Start </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.schedules[currentSettingDay].start\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"\" disabled selected>Select Start Time</option>\r" +
    "\n" +
    "                    <option value=\"9\">9 am</option>\r" +
    "\n" +
    "                    <option value=\"10\">10 am</option>\r" +
    "\n" +
    "                    <option value=\"11\">11 am</option>\r" +
    "\n" +
    "                    <option value=\"12\">12 pm</option>\r" +
    "\n" +
    "                    <option value=\"13\">1 pm</option>\r" +
    "\n" +
    "                    <option value=\"14\">2 pm</option>\r" +
    "\n" +
    "                    <option value=\"14\">3 pm</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "             <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Break On </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.schedules[currentSettingDay].breakon\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"\" disabled selected>Select Break Time</option>\r" +
    "\n" +
    "                    <option value=\"11\">11 am</option>\r" +
    "\n" +
    "                    <option value=\"11.5\">11h30 am</option>\r" +
    "\n" +
    "                    <option value=\"12\">12 pm</option>\r" +
    "\n" +
    "                    <option value=\"12.5\">12h30 pm</option>\r" +
    "\n" +
    "                    <option value=\"13\">1 pm</option>\r" +
    "\n" +
    "                    <option value=\"13.5\">1h30 pm</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "             <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Break Off </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.schedules[currentSettingDay].breakoff\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"\" disabled selected>Select Break Off Time</option>\r" +
    "\n" +
    "                    <option value=\"11.5\">11h30 am</option>\r" +
    "\n" +
    "                    <option value=\"12\">12 pm</option>\r" +
    "\n" +
    "                    <option value=\"12.5\">12h30 pm</option>\r" +
    "\n" +
    "                    <option value=\"13\">1 pm</option>\r" +
    "\n" +
    "                    <option value=\"13.5\">1h30 pm</option>\r" +
    "\n" +
    "                    <option value=\"14\">2h pm</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\"> End Time </div>\r" +
    "\n" +
    "                <select ng-model=\"settings.schedules[currentSettingDay].end\" ng-change=\"\">\r" +
    "\n" +
    "                    <option value=\"\" disabled selected>Select End Time</option>\r" +
    "\n" +
    "                    <option value=\"13\">1 pm</option>\r" +
    "\n" +
    "                    <option value=\"14\">2 pm</option>\r" +
    "\n" +
    "                    <option value=\"15\">3 pm</option>\r" +
    "\n" +
    "                    <option value=\"16\">4 pm</option>\r" +
    "\n" +
    "                    <option value=\"17\">5 pm</option>\r" +
    "\n" +
    "                    <option value=\"18\">6 pm</option>\r" +
    "\n" +
    "                    <option value=\"19\">7 pm</option>\r" +
    "\n" +
    "                    <option value=\"20\">8 pm</option>\r" +
    "\n" +
    "                    <option value=\"21\">9 pm</option>\r" +
    "\n" +
    "                    <option value=\"22\">10 pm</option>\r" +
    "\n" +
    "                    <option value=\"23\">11 pm</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Barber Shop </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"storename\" placeholder=\"Store Name\" ng-model=\"settings.barbershop\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Address </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"address\"  placeholder=\"Address\" ng-model=\"settings.address\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Zip Code </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"zip\" placeholder=\"Zip\" ng-model=\"settings.zipcode\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Phone Number </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"phone\" placeholder=\"Enter Phone Number\" ng-model=\"settings.phonenumber\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Card Number:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardNumber\" placeholder=\"\" ng-model=\"settings.payment.cardnumber\" onchange=\"angular.element(this).scope().cardnumberChange(this.value)\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Expiration Month:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardMonth\" placeholder=\"\" ng-model=\"settings.payment.cardmonth\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Expiration Year:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardYear\" placeholder=\"\" ng-model=\"settings.payment.cardyear\">\r" +
    "\n" +
    "            </div>                \r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Enter CVC Code:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardCVC\" placeholder=\"\" ng-model=\"settings.payment.cardcvc\"><br/>\r" +
    "\n" +
    "            </div>    \r" +
    "\n" +
    "\r" +
    "\n" +
    "            <map zoom=\"16\" style=\"height: 300px\">\r" +
    "\n" +
    "            </map>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/barber/statistics/view.html',
    "<div class=\"wrapper barber-statistic-page\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <div id=\"logo\">\r" +
    "\n" +
    "        <a class=\"backlink\" href=\"#/user-appointments\"><span class=\"btn gray\">BACK</span></a>\r" +
    "\n" +
    "      <img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" />\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form novalidate name=\"statisticForm\" id=\"settings\" >\r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Monthly CLYPR Goal</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.monthlyGoal }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      \r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Total So far</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.totalSoFar }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Total Haircuts</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.totalHaircuts }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Avarage tip</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.avgTip }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Total from tips</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.totalTips }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div>\r" +
    "\n" +
    "            <span>Hair cut need to reach Goal</span>\r" +
    "\n" +
    "            <span class=\"float-r\">\r" +
    "\n" +
    "                <span > {{statistic.needHairCuts }} </span>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"row\">\r" +
    "\n" +
    "    <div class=\"col-sm-6\">\r" +
    "\n" +
    "        <select class=\"select month-select h3\" ng-model=\"currentPeriod\">\r" +
    "\n" +
    "            <option value=\"1\"> This Month</option>\r" +
    "\n" +
    "            <option value=\"2\"> Last Month</option>\r" +
    "\n" +
    "        </select>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"slider-toggle\">\r" +
    "\n" +
    "  <a ng-class=\"{'active': sliderToggle == 0 }\" ng-click=\" sliderToggle = 0\"></a>\r" +
    "\n" +
    "  <a ng-class=\"{'active': sliderToggle == 1 }\" ng-click=\" sliderToggle = 1\"></a>\r" +
    "\n" +
    "  <a ng-class=\"{'active': sliderToggle == 2 }\" ng-click=\" sliderToggle = 2\"></a>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<ul class=\"align-c slider\" style=\"position: relative; height: 260px;margin-bottom: 50px;\">\r" +
    "\n" +
    "  <li id=\"upcoming\"  ng-class=\"{'active': sliderToggle == 0 }\">\r" +
    "\n" +
    "    <highchart config=\"lineChart\" style=\"height: 300px;width: 80%;background: transparent;position: relative;\"></highchart>\r" +
    "\n" +
    "  </li>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <li ng-class=\"{'active': sliderToggle == 1 }\" >\r" +
    "\n" +
    "    <highchart config=\"barChart\" style=\"height: 300px;width: 80%;background: transparent;position: relative;\"></highchart>\r" +
    "\n" +
    "  </li>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <li id=\"canceled\" ng-class=\"{'active': sliderToggle == 2 }\" data=\"2015-03-23\" >\r" +
    "\n" +
    "    <highchart config=\"donutChart\" style=\"height: 300px;width: 80%;background: transparent;position: relative;\"></highchart>\r" +
    "\n" +
    "  </li>\r" +
    "\n" +
    "</ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/booking/calendar/view.html',
    "<div class=\"header\">\r" +
    "\n" +
    "    <i class=\"icon ion-ios-arrow-left\" ng-click=\"previous()\"></i>\r" +
    "\n" +
    "    <span>{{month.format(\"MMMM, YYYY\")}}</span>\r" +
    "\n" +
    "    <i class=\"icon ion-ios-arrow-right\" ng-click=\"next()\"></i>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div class=\"week names\">\r" +
    "\n" +
    "    <span class=\"day\">Sun</span>\r" +
    "\n" +
    "    <span class=\"day\">Mon</span>\r" +
    "\n" +
    "    <span class=\"day\">Tue</span>\r" +
    "\n" +
    "    <span class=\"day\">Wed</span>\r" +
    "\n" +
    "    <span class=\"day\">Thu</span>\r" +
    "\n" +
    "    <span class=\"day\">Fri</span>\r" +
    "\n" +
    "    <span class=\"day\">Sat</span>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div class=\"week\" ng-repeat=\"week in weeks\">\r" +
    "\n" +
    "    <span class=\"day\" ng-class=\"{ today: day.isToday, 'different-month': !day.isCurrentMonth, selected: day.date.isSame(selected) }\" ng-click=\"select(day)\" ng-repeat=\"day in week.days\">{{day.number}}</span>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/booking/confirm/view.html',
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\" enable-menu-with-back-views=\"true\">\r" +
    "\n" +
    "    <h1 class=\"title\">Booking - Confirmation</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-pane>\r" +
    "\n" +
    "    <ion-content has-header=\"true\">\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Booking Successful!\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Confirmation Number: {{bData.cardInfo.transId}}\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Booking Price: {{bData.cardInfo.amount/100 | currency:\"$\":2}}\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Booking Date: {{bData.appDate}}\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Booking Time: {{bData.appTime.timeStartNum}}\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "        <a class=\"button button-block button-balanced\"  ng-click=\"goHome()\">\r" +
    "\n" +
    "            Return To Home Screen\r" +
    "\n" +
    "        </a>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-pane>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/booking/cut_options/view.html',
    "<ion-view >\r" +
    "\n" +
    "  <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "    <h1 class=\"title\">Haircut Request</h1>\r" +
    "\n" +
    "    <button ng-click=\"$root.goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "  </ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <ion-pane id=\"bookingCutOptions\">\r" +
    "\n" +
    "    <ion-content has-header=\"true\">\r" +
    "\n" +
    "      <!-- <h2>Booking Page for {{barb.firstName}} {{barb.lastName}}</h2> -->\r" +
    "\n" +
    "      <ion-list>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          <div class=\"item-input item-select\">\r" +
    "\n" +
    "            <div class=\"input-label\">\r" +
    "\n" +
    "              Style:\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <select ng-model=\"selectedStyle.styleVar\">\r" +
    "\n" +
    "                <option ng-repeat=\"(key,style) in styleVarList\" value=\"{{key}}\"> {{style.style}}</option>\r" +
    "\n" +
    "            </select>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          <div class=\"item-input item-select\">\r" +
    "\n" +
    "            <div class=\"input-label\">\r" +
    "\n" +
    "              Blend Location:\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <select ng-model=\"blendVar\" ng-change=\"onChange('blendVar', blendVar)\" ng-options=\"blendVar for blendVar in blendVarList track by blendVar\">\r" +
    "\n" +
    "            </select>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          Length on Top: {{topLength}}\r" +
    "\n" +
    "          <div class=\"range range-positive\">\r" +
    "\n" +
    "            <!-- <i class=\"icon ion-ios-sunny-outline\"></i> -->\r" +
    "\n" +
    "            0\r" +
    "\n" +
    "            <input type=\"range\" name=\"volume\" min=\"0\" max=\"5\" step=\"0.5\" value=\"{{topLength}}\" ng-model=\"topLength\" ng-change=\"onChange('topLength', topLength)\"> 5\r" +
    "\n" +
    "            <!-- <i class=\"icon ion-ios-sunny\"></i> -->\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          Length on Side: {{sideLength}}\r" +
    "\n" +
    "          <div class=\"range range-positive\">\r" +
    "\n" +
    "            <!-- <i class=\"icon ion-ios-sunny-outline\"></i> -->\r" +
    "\n" +
    "            0\r" +
    "\n" +
    "            <input type=\"range\" name=\"volume\" min=\"0\" max=\"5\" step=\"0.5\" value=\"{{sideLength}}\" ng-model=\"sideLength\" ng-change=\"onChange('sideLength', sideLength)\"> 5\r" +
    "\n" +
    "            <!-- <i class=\"icon ion-ios-sunny\"></i> -->\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          <div class=\"item-input item-select\">\r" +
    "\n" +
    "            <div class=\"input-label\">\r" +
    "\n" +
    "              Tape in Back:\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <select ng-model=\"tapeBackVar\" ng-change=\"onChange('tapeBackVar', tapeBackVar)\" ng-options=\"tapeBackVar for tapeBackVar in tapeBackVarList track by tapeBackVar\">\r" +
    "\n" +
    "            </select>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          Special Requests for Barber:\r" +
    "\n" +
    "          <textarea class=\"specialRequests\" ng-model=\"specialRequests\" ng-change=\"onChange('specialRequests', specialRequests)\" ng-trim=\"false\" maxlength=\"140\" placeholder=\"Leave a note for your barber...\"></textarea>\r" +
    "\n" +
    "          <span>{{specialRequests.length}}/140</span>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item>\r" +
    "\n" +
    "          Price (excluding tip)\r" +
    "\n" +
    "          <div class=\"border2 price\">\r" +
    "\n" +
    "            <span class=\"hours\">${{styleVarList[selectedStyle.styleVar].price}}</span><span>.</span>\r" +
    "\n" +
    "            <sup class=\"ap\"><u>{{styleVarList[selectedStyle.styleVar].time}} mins</u></sup>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "      </ion-list>\r" +
    "\n" +
    "      <a class=\"button button-block button-balanced\" ng-click=\"saveCutOptions()\">\r" +
    "\n" +
    "            Enter Payment Information\r" +
    "\n" +
    "        </a>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "  </ion-pane>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/booking/pay_info/view.html',
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <a style=\"color:#fff;\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "    <h1 class=\"title\">Review Order</h1>\r" +
    "\n" +
    "    <button ng-click=\"goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-pane>\r" +
    "\n" +
    "    <ion-content has-header=\"true\" id=\"payInfo\">\r" +
    "\n" +
    "        <form name=\"form.paymentForm\" novalidate ng-submit=\"submitPayment(form.paymentForm)\">\r" +
    "\n" +
    "            <div class=\"list\">\r" +
    "\n" +
    "              <div class=\"item item-divider\">Haircut Details</div>\r" +
    "\n" +
    "              <div class=\"item\">Style: <span class=\"item_phv\">{{bookingInfo.hairStyle.style}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">Blend Location: <span class=\"item_phv\">{{bookingInfo.hairStyle.blend}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">Length on Top: <span class=\"item_phv\">{{bookingInfo.hairStyle.topLength}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">Length on Side: <span class=\"item_phv\">{{bookingInfo.hairStyle.sideLength}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">Tape in Back: <span class=\"item_phv\">{{bookingInfo.hairStyle.tapeBack}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">Price: <span class=\"item_phv\">${{bookingInfo.price}}</span></div>\r" +
    "\n" +
    "              <div class=\"item\">\r" +
    "\n" +
    "                Time:\r" +
    "\n" +
    "                    <span class=\"item_phv\">\r" +
    "\n" +
    "                        <strong ng-if=\"bookingInfo.slot < 12\"> {{ bookingInfo.slot }} AM </strong>\r" +
    "\n" +
    "                        <strong ng-if=\"bookingInfo.slot > 12\"> {{ bookingInfo.slot-12 }} PM </strong> {{bookingInfo.bookingTime.weekday}} {{bookingInfo.bookingTime.month}} {{bookingInfo.bookingTime.day}} {{bookingInfo.bookingTime.year}}</p>\r" +
    "\n" +
    "                    </span>\r" +
    "\n" +
    "               </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"card\">\r" +
    "\n" +
    "              <div class=\"item item-text-wrap\">\r" +
    "\n" +
    "                <p>Total: ${{bookingInfo.price}}</p>\r" +
    "\n" +
    "                <p>Deposit: $1</p>\r" +
    "\n" +
    "                <p>Remaining: ${{bookingInfo.price - 1}}</p>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <input required name=\"cardNumber\" type=\"hidden\" ng-minlength=\"8\" ng-maxlength=\"16\" ng-model=\"user_settings.payment.cardnumber\"/>\r" +
    "\n" +
    "            <input required name=\"cardmonth\" required type=\"hidden\" ng-minlength=\"2\" ng-model=\"user_settings.payment.cardmonth\" ng-maxlength=\"2\" placeholder=\"MM\"/>\r" +
    "\n" +
    "            <input required name=\"cardyear\" required type=\"hidden\" ng-minlength=\"4\" ng-model=\"user_settings.payment.cardyear\" ng-maxlength=\"4\" placeholder=\"yyyy\"/>\r" +
    "\n" +
    "            <input required name=\"cvc\" required type=\"hidden\" ng-maxlength=\"4\" ng-model=\"user_settings.payment.cardcvc\"/>\r" +
    "\n" +
    "            <button class=\"button button-block button-balanced\" type=\"submit\"  >\r" +
    "\n" +
    "                Submit Payment Information\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "        </form>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-pane>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/booking/time/view.html',
    "<ion-view cache-view=\"false\" id=\"bookingTime\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "        <h1 class=\"title\">Barber Schedule</h1>\r" +
    "\n" +
    "        <button ng-click=\"$root.goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <ion-pane>\r" +
    "\n" +
    "        <ion-content has-header=\"true\">\r" +
    "\n" +
    "            <!-- Loading angular-date-picker -->\r" +
    "\n" +
    "            <date-picker ng-model=\"day\" ng-change=\"onSelectDay('day', day)\" format-date=\"formatDate\" parse-date=\"parseDate\"></date-picker>\r" +
    "\n" +
    "\r" +
    "\n" +
    "             <div class=\"row times\">\r" +
    "\n" +
    "                 <!-- <div class=\"col text-center\" ng-repeat=\"days in availTime\"> -->\r" +
    "\n" +
    "                 <div class=\"col text-center\">\r" +
    "\n" +
    "    \t\t        <span style=\"color:#FFF; text-align:center\">\r" +
    "\n" +
    "                        Selected date:<br> \r" +
    "\n" +
    "                        <b>{{shownDate}}</b>\r" +
    "\n" +
    "                        <!-- <b>{{ day }}</b> -->\r" +
    "\n" +
    "                    </span>\r" +
    "\n" +
    "                    <div style=\"margin-top: 10px; text-align: left;\">\r" +
    "\n" +
    "                         <div class=\"time-slot time-slot-spacing\" ng-repeat=\"slot in listAvailableTime\">       \r" +
    "\n" +
    "                             <input type=\"checkbox\" id=\"{{ slot.timeStart }}\" ng-model=\"slot.booked\" ng-disabled=\"slot.booked || slot.timeout\" ng-change=\"onUserSelectDateRange(slot)\">\r" +
    "\n" +
    "                             <label for=\"{{ slot.timeStart }}\">                             \r" +
    "\n" +
    "                                 <span ng-if=\"slot.timeStart < 12\">\r" +
    "\n" +
    "                                     {{ slot.timeStart }} AM - \r" +
    "\n" +
    "                                 </span>\r" +
    "\n" +
    "                                 <span ng-if=\"slot.timeStart > 12\">\r" +
    "\n" +
    "                                     {{ slot.timeStart-12 }} PM - \r" +
    "\n" +
    "                                 </span>                                 \r" +
    "\n" +
    "                                 <span ng-if=\"slot.booked\">Booked</span>\r" +
    "\n" +
    "                                 <span ng-if=\"!slot.booked && !slot.timeout\">Available</span>\r" +
    "\n" +
    "                                 <span ng-if=\"!slot.booked && slot.timeout\">Unavaiable</span>\r" +
    "\n" +
    "                             </label>\r" +
    "\n" +
    "                         </div>\r" +
    "\n" +
    "                    </div> \r" +
    "\n" +
    "                 </div>    \r" +
    "\n" +
    "             </div> \r" +
    "\n" +
    "        </ion-content>\r" +
    "\n" +
    "    </ion-pane>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/booking/view.html',
    "<ion-view cache-view=\"false\">\r" +
    "\n" +
    "  <ion-nav-view name=\"bookingTime\"></ion-nav-view>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <ion-nav-view name=\"bookingCutOptions\"></ion-nav-view>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <ion-nav-view name=\"bookingPayInfo\"></ion-nav-view>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <ion-nav-view name=\"bookingConfirm\"></ion-nav-view>\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/contact/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"contact-page\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "        <h1 class=\"title\">Contact Us</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "    <ion-pane>\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <div class=\"list\">\r" +
    "\n" +
    "        \t<h4 class=\"text-center item item-text-wrap\">Thank you, and welcome to CLYPR.</h4>\r" +
    "\n" +
    "\t\t\t<p class=\"item item-text-wrap\">We appreciate your support in downloading our app to change the way the world gets haircuts. Now we need your help to keep us improving. Please don't hesitate to reach out for anything. We're here to help and to listen to our users.</p>\r" +
    "\n" +
    "\t\t\t<a class=\"item item-icon-left\" href=\"mailto:Carlos@clypr.net?subject=Feed back\">\r" +
    "\n" +
    "\t\t\t    <i class=\"icon ion-email positive\"></i>\r" +
    "\n" +
    "\t\t\t    <p>Carlos Banks </p>\r" +
    "\n" +
    "\t\t\t\t<p>Co-Founder</p>\r" +
    "\n" +
    "\t      \t\t<p><span class=\"dark\">Carlos@clypr.net</span></p>\r" +
    "\n" +
    "\t\t\t</a>\r" +
    "\n" +
    "\t\t\t<a class=\"item item-icon-left\" href=\"mailto:Byron@clypr.net?subject=Feed back\">\r" +
    "\n" +
    "\t\t\t    <i class=\"icon ion-email positive\"></i>\r" +
    "\n" +
    "\t\t\t    <p>Byron Gaskin</p>\r" +
    "\n" +
    "\t\t\t\t<p>Co-Founder </p>\r" +
    "\n" +
    "\t      \t\t<p><span class=\"dark\">Byron@clypr.net</span></p>\r" +
    "\n" +
    "\t\t\t</a>\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"item item-icon-left\" href=\"#\">\t\t\t\t\r" +
    "\n" +
    "\t\t\t    <i class=\"icon ion-chatbubbles positive\"></i>\r" +
    "\n" +
    "\t\t\t    Social:\r" +
    "\n" +
    "\t\t\t    \t<a href=\"https://m.facebook.com/clypr#!/clypr\" class=\"c-ion c-positive\"><i class=\"ion-social-facebook light\"></i></a>\r" +
    "\n" +
    "\t\t\t    \t<a href=\"https://www.instagram.com/clypr/\" class=\"c-ion c-assertive\"><i class=\"ion-social-instagram light\"></i></a>\r" +
    "\n" +
    "\t\t\t    \t<a href=\"https://mobile.twitter.com/clyprteam\" class=\"c-ion c-positive\"><i class=\"ion-social-twitter light\"></i></a>\r" +
    "\n" +
    "\t\t\t    \t<a href=\"https://www.snapchat.com/add/clypr\" class=\"c-ion c-warning\"><i class=\"ion-social-snapchat-outline\"></i></a>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "    </ion-pane>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/home/blank/view.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "        <h1 class=\"title\">User Home</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    " <ion-content>\r" +
    "\n" +
    "                <div class=\"content\">\r" +
    "\n" +
    "                        <h3>Connect with the perfect barber</h3>\r" +
    "\n" +
    "                        <p>some text to describe your service</p>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "<ion-list>\r" +
    "\n" +
    "  <ion-item>Hello!</ion-item>\r" +
    "\n" +
    "  <ion-item nav-clear menu-close href=\"#/app/listings\">\r" +
    "\n" +
    "    Link to Listing page\r" +
    "\n" +
    "  </ion-item>\r" +
    "\n" +
    "</ion-list>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<a nav-clear menu-close href=\"#/app/listings\">Visit the Listing page</a>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/home/view.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "    <h1 class=\"title\"></h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-content>\r" +
    "\n" +
    "<div class=\"wrapper\">\r" +
    "\n" +
    "    <div id=\"logo\"><img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" /></div>\r" +
    "\n" +
    "    <div id=\"screen\">\r" +
    "\n" +
    "        <div class=\"content\">\r" +
    "\n" +
    "            <h3 ng-if=\"userType == 'customer'\">Connect with the perfect barber</h3>\r" +
    "\n" +
    "            <h3 ng-if=\"userType == 'barber'\">You are a perfect barber</h3>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <!-- customer -->\r" +
    "\n" +
    "    <div id=\"menuHome\" ng-if=\"userType == 'customer'\">\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr6\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"app.user-booking-history\" ng-click=\"noAnimate\">\r" +
    "\n" +
    "                <img src=\"./images/recent_cuts.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                My Bookings\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr2\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" href=\"#/app/user-explore\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "                <img src=\"./images/location.png\" width=\"70\" height=\"70\" alt=\"icon\"/><span class=\"ion-locked\"></span>\r" +
    "\n" +
    "                Explore\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr3\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"salons.results\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "                <img src=\"./images/search.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                Search\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr5\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"app.user-settings\" ng-click=\"noAnimate\">\r" +
    "\n" +
    "                <img src=\"./images/settings.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                My Settings\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>        \r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr7\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ng-click=\"logout()\">\r" +
    "\n" +
    "                <img src=\"./images/power.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                <span>Sign Out</span>\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <!-- barber -->\r" +
    "\n" +
    "    <div id=\"menuHome\" ng-if=\"userType == 'barber'\">\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr1\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"app.barber-appointments\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "                <img src=\"./images/appointments.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                Appointments\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr2\"><!-- missing page -->\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" href=\"#/app/barber-recent-cuts\" ui-sref=\"app.barber-recent-cuts\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "                <img src=\"./images/recent_cuts.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                Recent Cuts\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr3\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"app.barber-settings\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "                <img src=\"./images/barber_settings.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                Barber Settings\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr4\"><!-- missing page -->\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ui-sref=\"app.barber-statistics\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "                <img src=\"./images/stats.png\" width=\"70\" height=\"70\" alt=\"icon\"/><span class=\"ion-locked\"></span>\r" +
    "\n" +
    "                Stats\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr6\"><!-- missing page -->\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" href=\"#/app/barber-feedback\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "                <img src=\"./images/feedback.png\" width=\"70\" height=\"70\" alt=\"icon\"/><span class=\"ion-locked\"></span>\r" +
    "\n" +
    "                Feedback\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"card-style-home\" data=\"scr7\">\r" +
    "\n" +
    "            <a nav-clear class=\"button card-style-home-button\" ng-click=\"logout()\">\r" +
    "\n" +
    "                <img src=\"./images/sign_out.png\" width=\"70\" height=\"70\" alt=\"icon\"/>\r" +
    "\n" +
    "                <span>Sign Out</span>\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "</ion-content>\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/login/templates/auth-noGoogleLogin.html',
    "<ion-header-bar class=\"bar-dark\">\r" +
    "\n" +
    "   <h1 class=\"title\">Login Without Google+</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<!-- <ion-content class=\"has-header padding\"> -->\r" +
    "\n" +
    "<ion-content class=\"has-header padding\">\r" +
    "\n" +
    "    <div class=\"card\">\r" +
    "\n" +
    "      <div class=\"item item-text-wrap\">\r" +
    "\n" +
    "        Write text here explaining how loging in with Google makes our system more efficient.\r" +
    "\n" +
    "        <br><br>We are considering adding other login options, and they can submit an email address to be notified when these other options are available.\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/login/templates/auth-signin.html',
    "<!-- <ion-content class=\"has-header padding\"> -->\r" +
    "\n" +
    "<ion-content class=\"has-header padding\" style=\"top:0px;\">\r" +
    "\n" +
    "    <div class=\"logo-large\">\r" +
    "\n" +
    "        <a><img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" /></a>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"count-down-container\" ng-if=\"counter\">\r" +
    "\n" +
    "      <div class=\"count-down\">\r" +
    "\n" +
    "        <div>You will be logged out in</div>\r" +
    "\n" +
    "        <div>{{counter}}</div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "   <form name=\"login\" method=\"post\" class=\"login\">\r" +
    "\n" +
    "      <div class=\"list\">\r" +
    "\n" +
    "        <!-- <div class=\"item item-input email\">\r" +
    "\n" +
    "          <input type=\"text\" name=\"email\"\r" +
    "\n" +
    "                  placeholder=\"E-mail address\"\r" +
    "\n" +
    "                  ng-model=\"account.email\" \r" +
    "\n" +
    "                  ng-pattern=\"/^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)/\"\r" +
    "\n" +
    "                  required>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"item item-input password\">\r" +
    "\n" +
    "          <input type=\"password\"  name=\"password\"\r" +
    "\n" +
    "                  placeholder=\"Password\"\r" +
    "\n" +
    "                  ng-model=\"account.password\"\r" +
    "\n" +
    "                  ng-pattern=\"/(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/\"\r" +
    "\n" +
    "                  required>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"padding\">\r" +
    "\n" +
    "            <small class=\"error-text sm\" ng-show='showError && login.email.$error.required'>*Email is mandatory</small>\r" +
    "\n" +
    "              <small class=\"error-text sm\" ng-show='showError && login.email.$error.pattern'>*Invalid email</small>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <small class=\"error-text sm\" ng-show='showError && login.password.$error.required'>*Password is mandatory</small>\r" +
    "\n" +
    "              <small class=\"error-text sm\" ng-show='showError && login.password.$error.pattern'>*Password must have at least 8 characters and contain following: 1 digit, 1 lower case letter, 1 upper case letter</small>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "        </div> -->\r" +
    "\n" +
    "        <div ng-if=\"firstTime\">\r" +
    "\n" +
    "          <div class=\"item tp-bg\" style=\"line-height: 32px; position: relative;\">\r" +
    "\n" +
    "              <span class=\"label\"> Active code </span>\r" +
    "\n" +
    "              <span class=\"label\" style=\"position: absolute; top: -5px; right: 16px; color: yellow; text-transform: capitalize;\">{{statusActive}}</span>\r" +
    "\n" +
    "              <input style=\"float: right; padding: 0px 5px; width: 50%;\" type=\"text\" name=\"active_code\" placeholder=\"Active code\" required=\"required\" ng-model=\"userType.active_code\">              \r" +
    "\n" +
    "          </div>          \r" +
    "\n" +
    "          <div class=\"item adding question-barber item-toggle tp-bg\">\r" +
    "\n" +
    "            <span class=\"label\">Are you a barber?</span>\r" +
    "\n" +
    "            <label class=\"toggle\">\r" +
    "\n" +
    "               <input type=\"checkbox\" ng-model=\"userType.isBarber\">\r" +
    "\n" +
    "               <div class=\"track\">\r" +
    "\n" +
    "                 <div class=\"handle\"></div>\r" +
    "\n" +
    "               </div>\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "          </div>          \r" +
    "\n" +
    "          <div class=\"padding\">\r" +
    "\n" +
    "            <label class=\"button button-block button-balanced\" ng-click=\"setType()\">\r" +
    "\n" +
    "                Go\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        <div class=\"padding\" ng-if=\"!firstTime\">\r" +
    "\n" +
    "          <!-- <input type=\"button\" ng-click=\"submit()\" value=\"Sign In\" class=\"btn blue\"> -->\r" +
    "\n" +
    "         <!--  <label class=\"button button-block button-positive\" ng-click=\"submit()\">\r" +
    "\n" +
    "              Sign In\r" +
    "\n" +
    "          </label>\r" +
    "\n" +
    "          <span class=\"label\" style=\"display:block;\">or</span> -->\r" +
    "\n" +
    "          <label class=\"button button-block button-balanced\" ng-click=\" gUserLogin(account.isBarber)\">\r" +
    "\n" +
    "              Login via Google+\r" +
    "\n" +
    "          </label>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "  </form>\r" +
    "\n" +
    " \r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/login/templates/auth-testPage.html',
    "<ion-header-bar class=\"bar-dark\">\r" +
    "\n" +
    "   <h1 class=\"title\">Logged In</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<!-- <ion-content class=\"has-header padding\"> -->\r" +
    "\n" +
    "<ion-content class=\"has-header padding\">\r" +
    "\n" +
    "      Logged in via Google+\r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/login/templates/auth.html',
    "      <ion-nav-view name=\"auth-signin\"></ion-nav-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/register/reg-userBasic.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <h1 class=\"title\">User Registration</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <div id=\"logo\"><img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" /></div>\r" +
    "\n" +
    "        <div id=\"screen\">\r" +
    "\n" +
    "                <div class=\"content\">\r" +
    "\n" +
    "                        <h2>Welcome</h2>\r" +
    "\n" +
    "                        <!-- <p>For security purposes, your registration name must match your physical ID (for example, a Driver's License)</p> -->\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"list white-bg\">\r" +
    "\n" +
    "            <label class=\"item item-input item-stacked-label\">\r" +
    "\n" +
    "                <span class=\"input-label\">First Name (as appears on ID)</span>\r" +
    "\n" +
    "                <input type=\"text\" \r" +
    "\n" +
    "                       placeholder=\"John\" \r" +
    "\n" +
    "                       value={{userFirstName}} \r" +
    "\n" +
    "                       ng-model=\"userFirstName\"\r" +
    "\n" +
    "                       ng-change=\"onChange('userFirstName', userFirstName)\"\r" +
    "\n" +
    "                       id=\"userFirstNameInput\">\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "            <label class=\"item item-input item-stacked-label\">\r" +
    "\n" +
    "                <span class=\"input-label\">Last Name (as appears on ID)</span>\r" +
    "\n" +
    "                <input type=\"text\" \r" +
    "\n" +
    "                       placeholder=\"Doe\" \r" +
    "\n" +
    "                       value={{userLastName}} \r" +
    "\n" +
    "                       ng-model=\"userLastName\"\r" +
    "\n" +
    "                       ng-change=\"onChange('userLastName', userLastName)\">\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "            <label class=\"item item-input item-stacked-label\">\r" +
    "\n" +
    "                <span class=\"input-label\">Zip Code</span>\r" +
    "\n" +
    "                <input type=\"text\" \r" +
    "\n" +
    "                       placeholder=\"33196\" \r" +
    "\n" +
    "                       ng-model=\"userZipCode\"\r" +
    "\n" +
    "                       ng-change=\"onChange('userZipCode', userZipCode)\">\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "  <button class=\"button button-block button-balanced\" ng-click=\"regBasicSubmit()\" >\r" +
    "\n" +
    "      Next\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "  <button class=\"button button-block button-assertive\" ng-click=\"logout()\">\r" +
    "\n" +
    "      Logout\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/register/reg-userPref.html',
    "<ion-view hide-nav-bar=\"true\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <h1 class=\"title\">User Preferences</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <div id=\"logo\"><img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" /></div>\r" +
    "\n" +
    "        <div id=\"screen\">\r" +
    "\n" +
    "                <div class=\"content\">\r" +
    "\n" +
    "                        <h3>Tell us about yourself</h3>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "            <label class=\"item item-input item-select item-text-wrap\">\r" +
    "\n" +
    "                <div class=\"input-label\">\r" +
    "\n" +
    "                    How often do you cut your hair?\r" +
    "\n" +
    "                </div> \r" +
    "\n" +
    "                <select ng-model=\"userFreq\"\r" +
    "\n" +
    "                        ng-change=\"onChange('userFreq', userFreq)\"\r" +
    "\n" +
    "                        ng-options=\"freq.name for freq in currentFreq track by freq.value\">\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "            <label class=\"item item-input item-select item-text-wrap\">\r" +
    "\n" +
    "                <div class=\"input-label\">\r" +
    "\n" +
    "                    Favorite Hair Style\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <select ng-model=\"userFavStyle\"\r" +
    "\n" +
    "                        ng-change=\"onChange('userFavStyle', userFavStyle)\"\r" +
    "\n" +
    "                        ng-options=\"style for style in currentStyle track by style\">\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "  <button class=\"button button-block button-balanced\" ng-click=\"regPrefSubmit()\">\r" +
    "\n" +
    "      Next\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "  <button class=\"button button-block button-assertive\" ng-click=\"logout()\">\r" +
    "\n" +
    "      Logout\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/register/reg.html',
    "      <ion-nav-view name=\"regUser\"></ion-nav-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/salon/options/view.html',
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "    <h1 class=\"title\">Barber Shops</h1>\r" +
    "\n" +
    "    <button ng-click=\"refresh()\" class=\"button button-icon icon  ion-ios-refresh-empty\"></button>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-pane>\r" +
    "\n" +
    "    <ion-content has-header=\"true\">\r" +
    "\n" +
    "        <button class=\"button button-block button-positive\" ng-click=\"updateSearch()\">\r" +
    "\n" +
    "            Update Search\r" +
    "\n" +
    "        </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Search Location:\r" +
    "\n" +
    "                <ion-google-place placeholder=\"Enter an address, Apt# and ZIP\" ng-model=\"searchLocation\" ng-change=\"onSearchFilterChange('searchLocation', searchLocation)\" geocode-options=\"geocodeOptions\" style=\"width: 100%\">\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                <label class=\"item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\">\r" +
    "\n" +
    "                    Sort By: \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <select ng-model=\"option.currentSortBarberOrder\">\r" +
    "\n" +
    "                    <option ng-repeat=\"order in sortBarberOrderList\" value=\"{{order.value}}\"> {{order.name}}</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "                </label>\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "            <ion-item>\r" +
    "\n" +
    "                Search Radius (in Miles): {{option.searchRadiusInMiles}}\r" +
    "\n" +
    "                <div class=\"range range-positive\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <input type=\"range\" name=\"volume\" min=\"1\" max=\"{{MAXIMUM_SEARCH_RANGE}}\" step=\"1\" value=\"{{option.searchRadiusInMiles}}\" ng-model=\"option.searchRadiusInMiles\" ng-change=\"onSearchFilterChange('option.searchRadiusInMiles', option.searchRadiusInMiles)\">\r" +
    "\n" +
    "                {{MAXIMUM_SEARCH_RANGE}}\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-pane>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/salon/results/view.html',
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "    <h1 class=\"title\">Barber Listings</h1>\r" +
    "\n" +
    "    <button ng-click=\"refresh()\" class=\"button button-icon icon  ion-ios-refresh-empty\"></button>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-pane>\r" +
    "\n" +
    "    <ion-content has-header=\"true\">\r" +
    "\n" +
    "        <ion-refresher  pulling-text=\"Pull to refresh...\" on-refresh=\"refresh()\"></ion-refresher>\r" +
    "\n" +
    "        <ion-list ng-show=\"showingListings\">\r" +
    "\n" +
    "            <ion-item ng-repeat=\"item in listBarbers | orderObjectBy:option.currentSortBarberOrder\" href=\"#/app/barber-details/{{item.uid}}\" class=\"item item-avatar item-icon-right\">\r" +
    "\n" +
    "      \t        <img ng-src=\"{{item.avatar || item.picture}}\">\r" +
    "\n" +
    "                <div>{{item.given_name}} {{item.family_name}}</div>\r" +
    "\n" +
    "                <div ng-if=\"item.barberShop\">{{item.barberShop}}</div>\r" +
    "\n" +
    "                <div ng-if=\"item.address || item.zipcode\">{{item.address}} {{item.zipcode}}</div>\r" +
    "\n" +
    "                <div><b>{{item.distance.toFixed(2)}} miles away</b></div>\r" +
    "\n" +
    "                <i class=\"icon ion-chevron-right icon-accessory\"></i>\r" +
    "\n" +
    "            </ion-item>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "        <div class=\"card\" ng-show=\"showingListings && !countProperties(listBarbers)\">\r" +
    "\n" +
    "          <div class=\"item item-text-wrap\">\r" +
    "\n" +
    "             There are no barbers nearby!\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-pane>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/salon/view.html',
    "<!-- <ion-view hide-nav-bar=\"true\"> -->\r" +
    "\n" +
    "<ion-view>\r" +
    "\n" +
    "    <ion-tabs class=\"tabs-dark tabs-icon-top\">\r" +
    "\n" +
    "        <ion-tab title=\"Results\" icon-on=\"ion-ios-filing\" icon-off=\"ion-ios-filing-outline\" href=\"#/salon/results\">\r" +
    "\n" +
    "            <ion-nav-view name=\"salonsResults\"></ion-nav-view>\r" +
    "\n" +
    "        </ion-tab>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <ion-tab title=\"Search Options\" icon-on=\"ion-ios-gear\" icon-off=\"ion-ios-gear-outline\" href=\"#/salon/options\">\r" +
    "\n" +
    "            <ion-nav-view name=\"salonsOptions\"></ion-nav-view>\r" +
    "\n" +
    "        </ion-tab>\r" +
    "\n" +
    "    </ion-tabs>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/tut/tut-one.html',
    "<!-- <ion-content class=\"has-header padding\"> -->\r" +
    "\n" +
    "<ion-content class=\"has-header padding\">\r" +
    "\n" +
    "  <h3>Tutorial Page 1</h3>\r" +
    "\n" +
    "  <button class=\"button button-block button-balanced\" ng-click=\"pageTwo()\">\r" +
    "\n" +
    "      Next\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "  <button class=\"button button-block button-assertive\" ng-click=\"homeRedirect()\">\r" +
    "\n" +
    "      Skip\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "<!-- Logged-in user text -->\r" +
    "\n" +
    "<!--\r" +
    "\n" +
    "    <div class=\"card\">\r" +
    "\n" +
    "      <div class=\"item item-text-wrap\" >User logged in as {{ $rootScope.userAuthData.google.displayName }}</div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "-->\r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/tut/tut-two.html',
    "<!-- <ion-content class=\"has-header padding\"> -->\r" +
    "\n" +
    "<ion-content class=\"has-header padding\">\r" +
    "\n" +
    "  <h3>Tutorial Page 2</h3>\r" +
    "\n" +
    "  <button class=\"button button-block button-balanced\" ng-click=\"homeRedirect()\">\r" +
    "\n" +
    "      Finish\r" +
    "\n" +
    "  </button>\r" +
    "\n" +
    "<!-- Logged-in user text -->\r" +
    "\n" +
    "<!--\r" +
    "\n" +
    "    <div class=\"card\">\r" +
    "\n" +
    "      <div class=\"item item-text-wrap\" >User logged in as {{ $rootScope.userAuthData.google.displayName }}</div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "-->\r" +
    "\n" +
    "</ion-content>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/tut/tut.html',
    "<ion-nav-view name=\"tutorial\"></ion-nav-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/user/appointments/view.html',
    "<ion-view hide-nav-bar=\"true\" cache-view=\"false\">\r" +
    "\n" +
    "  <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "    <h1 class=\"title\">User Home</h1>\r" +
    "\n" +
    "  </ion-header-bar>\r" +
    "\n" +
    "  <ion-content>\r" +
    "\n" +
    "    <div class=\"wrapper\">\r" +
    "\n" +
    "      <div id=\"logo\">        \r" +
    "\n" +
    "        <img src=\"./images/logo.png\" alt=\"logo\" width=\"150\" height=\"100\" />\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div id=\"calendar\">\r" +
    "\n" +
    "        <h1>{{calendarYear}}</h1>\r" +
    "\n" +
    "        <!-- <div id=\"kalender\"></div> -->\r" +
    "\n" +
    "        <div ui-calendar=\"uiConfig.calendar\" ng-model=\"eventSources\" ></div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div class=\"slider-toggle\">\r" +
    "\n" +
    "        <a ng-class=\"{'active': sliderToggle == 0 }\" ng-click=\" sliderToggle = 0\"></a>\r" +
    "\n" +
    "        <a ng-class=\"{'active': sliderToggle == 1 }\" ng-click=\" sliderToggle = 1\"></a>\r" +
    "\n" +
    "        <a ng-class=\"{'active': sliderToggle == 2 }\" ng-click=\" sliderToggle = 2\"></a>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <ul class=\"align-c slider\">\r" +
    "\n" +
    "        <li id=\"upcoming\" ng-class=\"{'active': sliderToggle == 0 }\">\r" +
    "\n" +
    "          <div>\r" +
    "\n" +
    "            <h3>UPCOMING</h3>\r" +
    "\n" +
    "            <h2 class=\"uptime\">Nov 29, 2.30<sup>PM</sup></h2>\r" +
    "\n" +
    "            <h2>Jason Street</h2>\r" +
    "\n" +
    "            <h4>Cutting Edge Cuts</h4>\r" +
    "\n" +
    "            <h4 class=\"address\">45 Park Avenue, NY</h4>\r" +
    "\n" +
    "            <span class=\"btn red\">Cancel</span>\r" +
    "\n" +
    "            <span class=\"btn green\">Pay Now</span>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-class=\"{'active': sliderToggle == 1 }\">\r" +
    "\n" +
    "          <div>\r" +
    "\n" +
    "            <h3 id=\"completed\" class=\"green\" data=\"2015-03-10, 2015-03-17\">COMPLETED CUTS</h3>\r" +
    "\n" +
    "            <div class=\"clear\">\r" +
    "\n" +
    "              <!-- valid -->\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <h2>Jason Street</h2>\r" +
    "\n" +
    "                <div class=\"rating\" data=\"4\" half=\"\">\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <!-- evalid -->\r" +
    "\n" +
    "              valid\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <p>Nov 1st, 10.30 <sup>am</sup></p>\r" +
    "\n" +
    "                <p>Cutting Edge Cuts</p>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <!-- evalid -->\r" +
    "\n" +
    "              <div class=\"w20\">\r" +
    "\n" +
    "                <a class=\"btn gray\"href=\"#\">View Pics</a>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"clear\">\r" +
    "\n" +
    "              <!-- valid -->\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <h2>Matt Smith</h2>\r" +
    "\n" +
    "                <div class=\"rating\" data=\"3\" half=\"\">\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                  <i></i>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <!-- valid -->\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <p>Nov 15th, 03.00 <sup>pm</sup></p>\r" +
    "\n" +
    "                <p>Cutting Edge Cuts</p>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"w20\">\r" +
    "\n" +
    "                <a class=\"btn gray\"href=\"#\">View Pics</a>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li id=\"canceled\" ng-class=\"{'active': sliderToggle == 2 }\" data=\"2015-03-23\">\r" +
    "\n" +
    "          <div>\r" +
    "\n" +
    "            <h3 class=\"red\">CANCELED CUTS</h3>\r" +
    "\n" +
    "            <div class=\"clear\">\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <h2>Jason Street</h2>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"w40\">\r" +
    "\n" +
    "                <p>Nov 28th, 07.30 <sup>pm</sup></p>\r" +
    "\n" +
    "                <p>Cutting Edge Cuts</p>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"w20\">\r" +
    "\n" +
    "                <a class=\"btn red\"href=\"#\">Canceled</a>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "      </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </ion-content>\r" +
    "\n" +
    "</ion-view>"
  );


  $templateCache.put('scripts/pages/user/booking_history/payment/view.html',
    "<ion-modal-view>\r" +
    "\n" +
    "  <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <h1 class=\"title\">Set Tips for Barber</h1>\r" +
    "\n" +
    "  </ion-header-bar>\r" +
    "\n" +
    "  <ion-content>\r" +
    "\n" +
    "      <div class=\"list\">\r" +
    "\n" +
    "        <label class=\"item item-input\" style=\"white-space: normal;\" ng-init=\"paymentInfo.tips = 5\">Don't forget to tip your barber! They work hard to keep you looking good!</label>\r" +
    "\n" +
    "        <!-- <input type=\"numberß\" name=\"tips\" ng-model=\"paymentInfo.tips\" ng-show=\"false\"> -->\r" +
    "\n" +
    "        <div class=\"button-bar tips-bar\">\r" +
    "\n" +
    "          <a class=\"button\" ng-click=\"setTip(0)\" ng-class=\"{'active': paymentInfo.tips == 0}\">No Tip</a>\r" +
    "\n" +
    "          <a class=\"button\" ng-click=\"setTip(1)\" ng-class=\"{'active': paymentInfo.tips == 1}\">$2</a>\r" +
    "\n" +
    "          <a class=\"button\" ng-click=\"setTip(2)\" ng-class=\"{'active': paymentInfo.tips == 2}\">$5</a>\r" +
    "\n" +
    "          <a class=\"button\" ng-click=\"setTip(5)\" ng-class=\"{'active': paymentInfo.tips == 5}\">$10</a>\r" +
    "\n" +
    "          <label class=\"item item-input button\">\r" +
    "\n" +
    "            <input type=\"number\" placeholder=\"Other\" ng-model=\"customTips\" ng-change=\"setTip(customTips)\" style=\"height: 100%; text-align: center; width: 100%; padding: 0px;margin-top:18%;\">\r" +
    "\n" +
    "          </label>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"button-bar item\" ng-if=\"paymentInfo.status=='booked'\">\r" +
    "\n" +
    "          <button type=\"submit\" class=\"button button-positive\" ng-click=\"payment()\">Pay</button>\r" +
    "\n" +
    "          <a class=\"button button-light\" ng-click=\"modal.hide();refresh()\">Cancel</a>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "  </ion-content>\r" +
    "\n" +
    "</ion-modal-view>"
  );


  $templateCache.put('scripts/pages/user/booking_history/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"userBookingHistory\" cache-view=\"false\">\r" +
    "\n" +
    "<ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "    <button menu-toggle=\"left\" class=\"button button-icon icon ion-navicon\"></button>\r" +
    "\n" +
    "    <h1 class=\"title\">Booking History</h1>\r" +
    "\n" +
    "</ion-header-bar>\r" +
    "\n" +
    "<ion-content>\r" +
    "\n" +
    "    <ion-refresher on-refresh=\"refresh()\"></ion-refresher>\r" +
    "\n" +
    "    <div ng-show=\"!bookings.length && !getting\">\r" +
    "\n" +
    "      <br>\r" +
    "\n" +
    "      <center>\r" +
    "\n" +
    "        <h1 style=\"\">Pull to Refresh</h1>\r" +
    "\n" +
    "      </center>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"wrapper\">\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "          <!-- customer view -->\r" +
    "\n" +
    "            <div class=\"button-bar\" id=\"status_button\">\r" +
    "\n" +
    "              <button class=\"button button-positive\" ng-click=\"show_list('booked',$event)\">Future</button>\r" +
    "\n" +
    "              <button class=\"button button-dark\" ng-click=\"show_list('done',$event)\">Past</button>                            \r" +
    "\n" +
    "              <button class=\"button button-dark\" ng-click=\"show_list('cancel',$event)\">Cancelled</button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"ubh_list\">              \r" +
    "\n" +
    "              <div ng-repeat=\"booking in bookings | orderBy:'-created_date'\">\r" +
    "\n" +
    "                <div class=\"ubh_item {{booking.status}}\">\r" +
    "\n" +
    "                  <div class=\"item row\">\r" +
    "\n" +
    "                    <div class=\"col col-20\" auto-resize-image>\r" +
    "\n" +
    "                      <img class=\"avata\" src=\"{{booking.avatar}}\"/>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col col-40 b-r positive-border\">\r" +
    "\n" +
    "                      <h2>{{booking.given_name}} {{booking.family_name}}</h2>\r" +
    "\n" +
    "                      <p><span class=\"energized\">{{booking.barbershop}}</span> - {{booking.address}} {{booking.zipcode}}</p>\r" +
    "\n" +
    "                      <p ng-if=\"booking.phonenumber\">{{booking.phonenumber}}</p>\r" +
    "\n" +
    "                      <div class=\"line\"></div>\r" +
    "\n" +
    "                      <p>\r" +
    "\n" +
    "                        <span ng-if=\"booking.slot < 12\">{{booking.slot}} AM</span>\r" +
    "\n" +
    "                        <span ng-if=\"booking.slot > 12\">{{booking.slot-12}} PM</span>\r" +
    "\n" +
    "                      </p>\r" +
    "\n" +
    "                      <p>{{booking.bookedDate}}</p>\r" +
    "\n" +
    "                      <p><span class=\"assertive-bg light p-3-8\">{{booking.status}}</span></p>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col col-40\">\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Blend:</span> {{booking.hairStyle.blend}}</p>\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Side Length:</span> {{booking.hairStyle.sideLength}}</p>\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Style:</span> {{booking.hairStyle.style}}</p>\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Tape Back:</span> {{booking.hairStyle.tapeBack}}</p>\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Top Length:</span> {{booking.hairStyle.topLength}}</p>\r" +
    "\n" +
    "                      <p><span class=\"font-bold\">Special requests:</span> {{booking.hairStyle.specialRequests}}</p>\r" +
    "\n" +
    "                      <p ng-if=\"booking.price\"><span class=\"assertive\">${{booking.price}}</span></p>\r" +
    "\n" +
    "                    </div>         \r" +
    "\n" +
    "                    <!-- <div class=\"status ion-help-circled\"></div>  -->  \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <!-- <ion-list class=\"ubh_select\" ng-if=\"booking.status=='booked'\">\r" +
    "\n" +
    "                    <ion-radio ng-model=\"status\" value=\"cancel\">Cancel</ion-radio>                  \r" +
    "\n" +
    "                    <ion-radio ng-model=\"status\" value=\"done\">Pay Barber</ion-radio>\r" +
    "\n" +
    "                  </ion-list> -->\r" +
    "\n" +
    "                  <div class=\"button-bar\" ng-if=\"booking.status=='booked'\">\r" +
    "\n" +
    "                    <a class=\"button button-positive\" ng-click=\"openPaymentModal(booking)\">Pay Barber</a>\r" +
    "\n" +
    "                    <a class=\"button button-assertive\" ng-click=\"updateStatus(booking, 'cancel')\">Cancel</a>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <!-- barber view -->\r" +
    "\n" +
    "          <div ng-if=\"type=='barber'\">\r" +
    "\n" +
    "            <div class=\"ubh_list\">\r" +
    "\n" +
    "              <div ng-repeat=\"booking in bookings | orderBy:'-created_date\">\r" +
    "\n" +
    "                <div class=\"item row\">\r" +
    "\n" +
    "                  <div class=\"col col-20\" auto-resize-image>\r" +
    "\n" +
    "                    <img class=\"avata\" src=\"{{booking.avatar}}\"/>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col col-40 b-r positive-border\">\r" +
    "\n" +
    "                    <h2>{{booking.given_name}} {{booking.family_name}}</h2>\r" +
    "\n" +
    "                    <!-- <p><span class=\"energized\">{{booking.barbershop}}</span> - {{booking.address}} {{booking.zipcode}}</p> -->\r" +
    "\n" +
    "                    <p ng-if=\"booking.phonenumber\">{{booking.phonenumber}}</p>\r" +
    "\n" +
    "                    <div class=\"line\"></div>\r" +
    "\n" +
    "                    <p>{{booking.createdDate}}</p>\r" +
    "\n" +
    "                    <p><span class=\"assertive-bg light p-3-8\">{{booking.status}}</span></p>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col col-40\">\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Blend:</span> {{booking.hairStyle.blend}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Side Length:</span> {{booking.hairStyle.sideLength}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Style:</span> {{booking.hairStyle.style}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Tape Back:</span> {{booking.hairStyle.tapeBack}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Top Length:</span> {{booking.hairStyle.topLength}}</p>\r" +
    "\n" +
    "                    <p><span class=\"font-bold\">Special requests:</span> {{booking.hairStyle.specialRequests}}</p>\r" +
    "\n" +
    "                    <p ng-if=\"booking.price\"><span class=\"assertive\">${{booking.price}}</span></p>\r" +
    "\n" +
    "                  </div>         \r" +
    "\n" +
    "                  <!-- <div class=\"status ion-help-circled\"></div>  -->  \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/pages/user/setting/view.html',
    "<ion-view hide-nav-bar=\"true\" id=\"userSettings\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar bar-header bar-dark  item-input-inset\">\r" +
    "\n" +
    "        <a style=\"color:#fff;\" href=\"javascript:void(0)\" ng-click=\"$root.goBack()\" class=\"button button-icon icon ion-chevron-left\"></a>\r" +
    "\n" +
    "        <h1 class=\"title\">Settings</h1>\r" +
    "\n" +
    "        <button ng-click=\"$root.goHome()\" class=\"button button-icon icon  ion-ios-home\"></button>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "        <ion-list>\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Account </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"email\" placeholder=\"Enter e-mail\" disabled=\"\" ng-model=\"settings.email\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Avatar </div>\r" +
    "\n" +
    "                <input type=\"file\" name=\"update-thumb\" onchange=\"angular.element(this).scope().avatarUploadChange(this.files[0])\">                \r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Zip Code </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"zip\" id=\"zip\" placeholder=\"Zip\" ng-model=\"settings.zipcode\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Phone Number </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"phone\" id=\"phone\" placeholder=\"Enter Phone Number\" ng-model=\"settings.phonenumber\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <!-- <div class=\"item item-input item-select\">\r" +
    "\n" +
    "                <div class=\"input-label\">Payment</div>\r" +
    "\n" +
    "                <select ng-model=\"settings.payment.type\" >\r" +
    "\n" +
    "                    <option value=\"card\">Card</option>\r" +
    "\n" +
    "                    <option value=\"visa\">Cash</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div> -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Card Number:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardNumber\" placeholder=\"\" ng-model=\"settings.payment.cardnumber\" onchange=\"angular.element(this).scope().cardnumberChange(this.value)\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Expiration Month:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardMonth\" placeholder=\"\" ng-model=\"settings.payment.cardmonth\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Expiration Year:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardYear\" placeholder=\"\" ng-model=\"settings.payment.cardyear\">\r" +
    "\n" +
    "            </div>                \r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\">Enter CVC Code:</div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"cardCVC\" placeholder=\"\" ng-model=\"settings.payment.cardcvc\"><br/>\r" +
    "\n" +
    "            </div>    \r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"item item-input\">\r" +
    "\n" +
    "                <div class=\"input-label\"> Hairstyle </div>\r" +
    "\n" +
    "                <input type=\"text\" name=\"style\" id=\"style\" placeholder=\"Hair style\" ng-model=\"settings.style\">\r" +
    "\n" +
    "            </div> \r" +
    "\n" +
    "        </ion-list>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "</ion-view>\r" +
    "\n"
  );


  $templateCache.put('scripts/system/menu.html',
    "<ion-side-menus cache-view=\"false\" enable-menu-with-back-views=\"true\">\r" +
    "\n" +
    "  <ion-side-menu-content>\r" +
    "\n" +
    "    <!-- <ion-nav-bar class=\"bar-dark\">\r" +
    "\n" +
    "      <ion-nav-back-button side=\"left\">\r" +
    "\n" +
    "      </ion-nav-back-button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <ion-nav-buttons side=\"left\">\r" +
    "\n" +
    "        <button class=\"button button-icon button-clear ion-navicon\" menu-toggle=\"left\">\r" +
    "\n" +
    "        </button>\r" +
    "\n" +
    "      </ion-nav-buttons>\r" +
    "\n" +
    "    </ion-nav-bar> -->\r" +
    "\n" +
    "    <ion-nav-view name=\"menuContent\"></ion-nav-view>\r" +
    "\n" +
    "  </ion-side-menu-content>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <ion-side-menu side=\"left\">\r" +
    "\n" +
    "    <ion-header-bar class=\"bar-dark\">\r" +
    "\n" +
    "      <h1 class=\"title\" style=\"font-weight: bold;\">CLYPR</h1>\r" +
    "\n" +
    "    </ion-header-bar>\r" +
    "\n" +
    "    <ion-content>\r" +
    "\n" +
    "      <!-- customer menu -->\r" +
    "\n" +
    "      <ion-list ng-if=\"userType == 'customer'\">\r" +
    "\n" +
    "        <ion-item nav-clear menu-close ui-sref=\"app.user-settings\" class=\"item-icon-left\">\r" +
    "\n" +
    "          <div class=\"avatar\" style=\"background: url({{userSettings.avatar}}) no-repeat top center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;\"></div>\r" +
    "\n" +
    "          <div class=\"text-center username\" style=\"padding: 10px 0px 0px 0px; font-weight: bold;\">{{userSettings.name}}</div>\r" +
    "\n" +
    "        </ion-item>        \r" +
    "\n" +
    "        <ion-item nav-clear menu-close ui-sref=\"app.home\" class=\"item-icon-left\">\r" +
    "\n" +
    "          <i class=\"icon ion-ios-home-outline\" style=\"color: #000;\"></i>          \r" +
    "\n" +
    "          Home\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close ui-sref=\"salons.results\" class=\"item-icon-left\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "          <i class=\"icon ion-search\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "          Find Barbers\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close ui-sref=\"app.user-booking-history\" class=\"item-icon-left\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "          <i class=\"icon ion-person-stalker\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "          My Bookings\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close ui-sref=\"booking.cut-options\" class=\"item-icon-left\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "          <i class=\"icon ion-card\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "          Payment Method\r" +
    "\n" +
    "          <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close href=\"#/app/promo-code\" class=\"item-icon-left\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "          <i class=\"icon ion-cash\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "          Promo codes\r" +
    "\n" +
    "          <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close href=\"#/app/user-settings\" ui-sref=\"app.user-settings\" class=\"item-icon-left\" ng-click=\"closeMenu()\">\r" +
    "\n" +
    "          <i class=\"icon ion-ios-gear-outline\" style=\"color: #0066FF;\"></i>\r" +
    "\n" +
    "          My settings\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "        <ion-item nav-clear menu-close href=\"#/app/contact\" ui-sref=\"app.contact\" class=\"item-icon-left\" ng-click=\"closeMenu()\">\r" +
    "\n" +
    "          <i class=\"icon ion-email\" style=\"color: #0066FF;\"></i>\r" +
    "\n" +
    "          Contact us\r" +
    "\n" +
    "        </ion-item>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      </ion-list>\r" +
    "\n" +
    "      <!-- barber menu -->\r" +
    "\n" +
    "      <ion-list ng-if=\"userType == 'barber'\">\r" +
    "\n" +
    "          <ion-item nav-clear menu-close ui-sref=\"app.barber-settings\" class=\"item-icon-left\">\r" +
    "\n" +
    "            <div class=\"avatar\" style=\"background: url({{userSettings.avatar}}) no-repeat top center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;\"></div>\r" +
    "\n" +
    "            <div class=\"text-center username\" style=\"padding: 10px 0px 0px 0px; font-weight: bold;\">{{userInfo.given_name}} {{userInfo.family_name}}</div>\r" +
    "\n" +
    "          </ion-item> \r" +
    "\n" +
    "          <ion-item nav-clear menu-close ui-sref=\"app.home\" class=\"item-icon-left\">\r" +
    "\n" +
    "            <i class=\"icon ion-ios-home-outline\" style=\"color: #000;\"></i>            \r" +
    "\n" +
    "            Home\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close ui-sref=\"app.barber-appointments\" class=\"item-icon-left\" ng-click=\"noAnimate()\">\r" +
    "\n" +
    "            <i class=\"icon ion-person-stalker\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "            My Appointments\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close ui-sref=\"app.barber-statistics\" class=\"item-icon-left\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "            <i class=\"icon ion-stats-bars\" style=\"color: #0066FF;\"></i>\r" +
    "\n" +
    "            My Stats\r" +
    "\n" +
    "            <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close href=\"#/booking/cut-options\" class=\"item-icon-left\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "            <i class=\"icon ion-card\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "            Payment Method\r" +
    "\n" +
    "            <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close href=\"#/app/promo-code\" class=\"item-icon-left\" ng-click=\"comingSoon()\">\r" +
    "\n" +
    "            <i class=\"icon ion-cash\" style=\"color: #000;\"></i>\r" +
    "\n" +
    "            Promo codes\r" +
    "\n" +
    "            <span class=\"ion-locked\"></span>\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close ui-sref=\"app.barber-settings\" class=\"item-icon-left\" ng-click=\"closeMenu()\">\r" +
    "\n" +
    "            <i class=\"icon ion-ios-gear-outline\" style=\"color: #0066FF;\"></i>\r" +
    "\n" +
    "            My settings\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "          <ion-item nav-clear menu-close href=\"#/app/contact\" ui-sref=\"app.contact\" class=\"item-icon-left\" ng-click=\"closeMenu()\">\r" +
    "\n" +
    "            <i class=\"icon ion-email\" style=\"color: #0066FF;\"></i>\r" +
    "\n" +
    "            Contact us\r" +
    "\n" +
    "          </ion-item>\r" +
    "\n" +
    "      </ion-list>\r" +
    "\n" +
    "    </ion-content>\r" +
    "\n" +
    "  </ion-side-menu>\r" +
    "\n" +
    "</ion-side-menus>\r" +
    "\n"
  );

}]);
