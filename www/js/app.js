(function() {
  "use strict";

  angular.module('barber', [
    'ionic',
    'ngCordova',
    'ngMap',
    'firebase',
    'ngOrderObjectBy',
    'ion-google-place',
    'tabSlideBox',
    'angular-stripe',
    'mp.datePicker',
    'ui.calendar',
    'ui.bootstrap',
  ]);

}());
(function() {
  "use strict";

  angular.module('barber')
    .constant("YP_BASE_ADDRESS", "http://pubapi.yp.com/search-api/search/devapi/search?format=json&sort=distance&searchloc=")
    .constant("DETAILS_ADDRESS", "http://api2.yp.com/listings/v1/details?format=json&listingid=")
    .constant("YP_API_KEY", "w4ycm9k9ct")
    .constant('HOME_PAGE', {
      'barber': 'app.home',
      'customer': 'app.home',
    })
    .constant('IN_APP_STATES', {
      'app.barber-settings': "barber",
      'app.user-appointments': "customer",
      //'barber-appointments': "barber",
      //'barber-statistics': "barber",
      'app.home': ["customer", 'barber'],
      'app.settings': "customer"
    })
    .constant('OUT_APP_STATES', {
      "auth.signin": "Login State"
    })
    .constant('DEFAULT_SEARCH_SETTINGS', {
      searchRadiusInMiles: 50
    })
    .constant('CONFIG', {
      stripePublicKey: 'sk_test_BQokikJOvBiI2HlWgH4olfQ2',      
      clyprServerURL: 'http://103.253.146.118:8100',
      clyprPushServerURL: 'http://devdobusiness.com:8989',
      // clyprPushServerURL: 'http://localhost:8000',
      // clyprServerURL: 'http://localhost:8100'
    });

}());
(function() {
  "use strict";
  angular.module('barber')
    .config(['$stateProvider', '$urlRouterProvider',
      function($stateProvider, $urlRouterProvider) {

        $stateProvider
          .state('auth', {
            url: "/auth",
            abstract: true,
            templateUrl: "scripts/pages/login/templates/auth.html"
          })
          .state('auth.signin', {
            url: "/signin",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-signin.html",
                controller: "AuthController"
              }
            }
          })
          .state('auth.noGoogleLogin', {
            url: "/noGoogleLogin",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-noGoogleLogin.html",
                controller: "AuthController"
              }
            }
          })
          .state('auth.testPage', {
            url: "/testPage",
            views: {
              'auth-signin': {
                templateUrl: "scripts/pages/login/templates/auth-testPage.html",
                controller: "AuthController"
              }
            }
          })
          .state('reg', {
            url: "/reg",
            abstract: true,
            templateUrl: "scripts/pages/register/reg.html"
          })
          .state('reg.userBasic', {
            url: "/userBasic",
            views: {
              'regUser': {
                templateUrl: "scripts/pages/register/reg-userBasic.html",
                controller: "RegController"
              }
            }
          })
          .state('reg.userPref', {
            url: "/userPref",
            views: {
              'regUser': {
                templateUrl: "scripts/pages/register/reg-userPref.html",
                controller: "RegController"
              }
            }
          })
          .state('tut', {
            url: "/tut",
            abstract: true,
            templateUrl: 'scripts/pages/tut/tut.html'
          })
          .state('tut.one', {
            url: "/one",
            views: {
              'tutorial': {
                templateUrl: "scripts/pages/tut/tut-one.html",
                controller: "TutController"
              }
            }
          })
          .state('tut.two', {
            url: "/two",
            views: {
              'tutorial': {
                templateUrl: "scripts/pages/tut/tut-two.html",
                controller: "TutController"
              }
            }
          })
          .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "scripts/system/menu.html",
            controller: 'MenuController'
          })
          .state('app.home', {
            url: "/home",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/home/view.html",
                controller: "HomeController"
              }
            }
          })
          .state('app.barber-reviews', {
            url: "/barber/reviews/:id",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/barber/review/view.html",
                controller: "BarberReviewsController"
              }
            }
          })
          .state('app.user-settings', {
            url: "/user/settings",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/user/setting/view.html",
                controller: "UserSettingsController"
              }
            }
          })
          .state('app.contact', {
            url: "/user/contact",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/contact/view.html",
                controller: "ContactController"
              }
            }
          })
          .state('app.about', {
            url: "/about",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/aboutUs/about.html"
              }
            }
          })
          .state('app.user-appointments', {
            url: "/user/appointments",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/user/appointments/view.html',
                controller: 'UserAppointmentsController'
              }
            }
          })
          .state('app.user-booking-history', {
            url: "/user/booking-history",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/user/booking_history/view.html",
                controller: "UserBookingHistory"
              }
            }
          })
        //X: barber
        .state('app.barber-settings', {
          url: "/barber/settings",
          views: {
            'menuContent': {
              templateUrl: 'scripts/pages/barber/setting/view.html',
              controller: 'BarberSettingsController'
            }
          }
        })
          .state('app.barber-appointments', {
            url: "/barber/appointments",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/appointments/view.html',
                controller: 'BarberAppointmentsController'
              }
            }
          })
          .state('app.barber-recent-cuts', {
            url: "/barber/barber-recent-cuts",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/recent_cuts/view.html',
                controller: 'BarberRecentCutsController'
              }
            }
          })
          .state('app.barber-profile', {
            url: "/barber/profile",
            views: {
              'menuContent': {
                templateUrl: 'scripts/pages/barber/profile/view.html',
                controller: 'BarberProfileController'
              }
            }
          })
          .state('app.barber-details', {
            url: "/barber-details/:id",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/barber/details/view.html",
                controller: "BarberDetailsController"
              }
            }
          })
          .state('salons', {
            url: "/salon",
            abstract: true,
            templateUrl: "scripts/pages/salon/view.html",
            controller: "BarberSalonController"
          })
          .state('salons.results', {
            url: "/results",
            views: {
              'salonsResults': {
                templateUrl: "scripts/pages/salon/results/view.html",
              }
            }
          })
          .state('salons.options', {
            url: "/options",
            views: {
              'salonsOptions': {
                templateUrl: "scripts/pages/salon/options/view.html",
              }
            }
          })
        // .state('booking', {
        //   url: "/booking",
        //   abstract: true,
        //   templateUrl: "scripts/pages/booking/view.html"
        // })
        .state('app.booking-time', {
          url: "/booking-time/:id",
          views: {
            'menuContent': {
              templateUrl: "scripts/pages/booking/time/view.html",
              controller: "BookingTimeController"
            }
          }
        })
          .state('app.booking-cut-options', {
            url: "/booking-cut-options/:id?date&slot",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/cut_options/view.html",
                controller: "BookingController"
              }
            }
          })
          .state('app.booking-pay-info', {
            url: "/booking-pay-info/:id?date&startTime?barberId",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/pay_info/view.html",
                controller: "BookingController"
              }
            }
          })
          .state('app.booking-confirm', {
            url: "/booking-confirm",
            views: {
              'menuContent': {
                templateUrl: "scripts/pages/booking/confirm/view.html",
                controller: "BookingController"
              }
            }
          });
        // $urlRouterProvider.otherwise('/app/home');
      }
    ]).run(['$ionicPlatform', 'GeoService', '$rootScope', '$ionicLoading', '$ionicHistory', '$state', '$http', 'Router', 'Auth', '$timeout', '$ionicPopup', '$cordovaPush',
      function($ionicPlatform, GeoService, $rootScope, $ionicLoading, $ionicHistory, $state, $http, Router, Auth, $timeout, $ionicPopup, $cordovaPush) {
        Router.registerRouterChecker();
        $rootScope.checkingCurrentUser = true;

        var iosConfig = {
          "badge": true,
          "sound": true,
          "alert": true,
        };

        document.addEventListener("deviceready", function() {
          $cordovaPush.register(iosConfig).then(function(deviceToken) {
            localStorage.setItem('deviceToken', deviceToken);

          }, function(err) {
            alert("Registration error: " + err)
          });


          // $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
          //   if (notification.alert) {
          //     navigator.notification.alert(notification.alert);
          //   }

          //   if (notification.sound) {
          //     var snd = new Media(event.sound);
          //     snd.play();
          //   }

          //   if (notification.badge) {
          //     $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
          //       // Success!
          //     }, function(err) {
          //       // An error occurred. Show a message to the user
          //     });
          //   }
          // });

          // // WARNING! dangerous to unregister (results in loss of tokenID)
          // $cordovaPush.unregister(options).then(function(result) {
          //   // Success!
          // }, function(err) {
          //   // Error
          // });
        });

        $ionicPlatform.ready(function() {
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();
          }



          // check user login 
          Auth.checkCurrentToken().then(function(data) {
            $rootScope.checkingCurrentUser = false;
            if (Auth.oldRouter) {
              console.log(Auth.oldRouter);
              $state.go(Auth.oldRouter.name.name, Auth.oldRouter.parameters);
            } else {
              $state.go('app.home');
            }

          }, function() {
            $rootScope.checkingCurrentUser = false;
            $state.go('auth.signin');
          });

        });

        // $rootScope.saveInfo = function(uData) {
        //   //This field is obtained from the Firebase data. This can be updated with Google+ data
        //   $http.defaults.headers.common.Authorization = 'Bearer ' + Settings.userAuthData.google.accessToken;
        // };

        // $rootScope.clearUserInfo = function() {
        //   //There must be a better way to do this! Perhaps looping through $rootScope user variables and setting them all to null
        //   $http.defaults.headers.common.Authorization = null;
        // };

        $rootScope.homeRedirect = function() {
          $state.go('app.home').then(function() {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
          });
        };

        $rootScope.logout = function() {
          Auth.logout();

          // work around for logout authentication from google
          if (window.location.href.indexOf('http') == 0) {

            // for web version
            var width = $('body').width();
            var height = $('body').height();
            var win = window.open("https://accounts.google.com/logout", "", "width=" + width + ",height=" + height);
            setTimeout(function() {
              win.close();
            }, 500);
          } else {

            // for app version
            var browser = window.open('https://accounts.google.com/logout', '_blank', 'location = no');
            browser.addEventListener('loadstop', function(res) {
              if (res.url.indexOf('http://www.google.com.vn/accounts/Logout2') >= 0)
                browser.close();
            });
          }


        };

        $rootScope.goHome = function() {
          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();
          $state.go('app.home');
        };

        $rootScope.goBack = function() {
          // $ionicHistory.goBack();
          window.history.back();
        };

        $rootScope.comingSoon = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
          var alertPopup = $ionicPopup.alert({
            title: 'Coming soon',
            template: "We're still working on this. We'll keep you posted."
          });
          alertPopup.then(function(res) {

          });
        };

        $rootScope.reviewComingSoon = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
          var alertPopup = $ionicPopup.alert({
            title: 'Coming soon',
            template: "We're still working on this. We'll keep you posted."
          });
          alertPopup.then(function(res) {

          });
        };

        $rootScope.noAnimate = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
        };
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .factory('LocationService', ['$q', '$ionicLoading', 'Auth',
      function($q, $ionicLoading, Auth) {
        var autoUpdateLocation = true,
          hasGeoLocation = ("geolocation" in navigator),
          geo = navigator.geolocation,
          watchId = null,
          lastPosition = null,
          geo_options = {
            enableHighAccuracy: true,
            maximumAge: 30000,
            timeout: 27000
          };

        // console.log("geo = " + hasGeoLocation);
        // init();

        function init() {
          if (hasGeoLocation) {
            navigator.geolocation.getCurrentPosition(savePosition);
            watchId = navigator.geolocation.watchPosition(savePosition, errorPosition, geo_options);
          }
        }

        function stop() {
          if (watchId) {
            navigator.geolocation.clearWatch(watchId);
            watchId = null;
          }
        }

        function getCurrentPosition() {
          $ionicLoading.show({
            template: 'Get Current Position...',
            noBackdrop: true,
            duration: 2000
          });
          var deferred = $q.defer();
          if (lastPosition) {
            setTimeout(function() {
              deferred.resolve(lastPosition);
            });
          } else {
            navigator.geolocation.getCurrentPosition(function(position) {
              return deferred.resolve(position);
            }, function(err) {
              return deferred.reject(null);
            });
          }
          return deferred.promise;
        }

        function getLastPosition() {
          return lastPosition;
        }

        function pauseUpdates() {
          autoUpdateLocation = false;
        }

        function resumeUpdates() {
          autoUpdateLocation = true;
        }

        function savePosition(position) {
          var loc = position.coords.latitude + "%3A" + position.coords.longitude;
          lastPosition = position;
          if (watchId) {
            navigator.geolocation.clearWatch(watchId);
            watchId = null;
          } else if (autoUpdateLocation) {
            console.log(this);
            this.location = loc;
          }
        }

        function errorPosition() {
          console.log("Error received in geolocation");
        }

        return {
          init: init,
          stop: stop,
          getCurrentPosition: getCurrentPosition,
          getLastPosition: getLastPosition,
          pauseUpdates: pauseUpdates,
          resumeUpdates: resumeUpdates
        };
      }
    ]);
}());
(function() {

  angular.module('barber')

  .factory('Auth', [
    '$http',
    '$q',
    'Firebase',
    '$timeout',
    '$state',
    '$ionicHistory',
    'CONFIG',
    function($http, $q, Firebase, $timeout, $state, $ionicHistory, CONFIG) {
      var firebase = Firebase.getInstance();

      var Auth = {
        login: function(email, password) {
          var defer = $q.defer();

          firebase.authWithPassword({
            email: email,
            password: password
          }, function(error, authData) {
            if (error) {
              defer.reject(error);
            } else {
              Auth.authData = angular.copy(authData);
              Auth.me().then(function(userInfo) {
                defer.resolve(userInfo);
              });
            }
          });

          return defer.promise;
        },

        __standardGoogleData: function(googleData) {
          var userInfo = {
            type: googleData.type,
            provider: 'google',
            uid: googleData.uid,
            token: googleData.token,
            id: googleData.google.id,
            email: googleData.google.email
          }
          angular.extend(userInfo, googleData.google.cachedUserProfile);
          userInfo.name = userInfo.given_name + " " + userInfo.family_name;
          return userInfo;
        },

        registerGoogleAccount: function(googleData) {
          var userInfo = this.__standardGoogleData(googleData);
          if (localStorage.getItem('deviceToken')) {
            userInfo.deviceToken = localStorage.getItem('deviceToken');
            // register device token with notification server
            $http({
              method: 'POST',
              url: CONFIG.clyprPushServerURL + '/subscribe',
              data: {
                token: userInfo.deviceToken,
                user: userInfo.uid,
                type: 'ios'
              }
            }).then(function successCallback(response) {
              console.log(response);
            }, function errorCallback(error) {
              console.log(error);
            });
          }

          var defer = $q.defer();
          $http({
            method: 'GET',
            url: CONFIG.clyprServerURL + '/api/clypr-code/checkValid/' + googleData.active_code,
          }).then(function successCallback(res) {
            $http({
              method: 'PUT',
              url: CONFIG.clyprServerURL + '/api/clypr-code/useCode/?code=' + googleData.active_code + "&userInfo=" + JSON.stringify(userInfo),
            }).then(function successCallback(response) {
              var usersRef = firebase.child("users").child(userInfo.uid);
              usersRef.set(userInfo, function(error, authData) {
                if (error) {
                  defer.reject(error);
                } else {
                  Auth.authData = Auth.currentUserInfo = userInfo;
                  defer.resolve(userInfo);
                }
              });
            }, function errorCallback(error) {
              defer.reject(error);
            });
          }, function errorCallback(error) {
            defer.reject(error);
          })
          return defer.promise;
        },

        me: function() {
          var defer = $q.defer();

          var ref = firebase.child("users").child(Auth.authData.uid);

          ref.once("value", function(data) {
            var info = data.val();
            Auth.currentUserInfo = info;
            defer.resolve(Auth.currentUserInfo);

            var deviceToken = localStorage.getItem('deviceToken');
            if (info.deviceToken != deviceToken) {
              info.deviceToken = deviceToken;
              // register device token with notification server
              $http({
                method: 'POST',
                url: CONFIG.clyprPushServerURL + '/subscribe',
                data: {
                  token: deviceToken,
                  user: info.uid,
                  type: 'ios'
                }
              }).then(function successCallback(response) {
                console.log(response);
              }, function errorCallback(error) {
                console.log(error);
              });

              ref.set(info);
            }
          });

          return defer.promise;
        },

        checkCurrentToken: function() {
          var defer = $q.defer();

          var authData = firebase.getAuth();
          Auth.authData = authData;

          if (authData) {
            Auth.me().then(function(info) {
              defer.resolve(info);
            });
            return defer.promise;
          } else {
            $timeout(function() {
              defer.reject(authData);
            });
          }

          return defer.promise;
        },

        isAuthenticated: function() {
          return !!Auth.authData;
        },

        type: function() {
          if (Auth.currentUserInfo)
            return Auth.currentUserInfo.type;
          else
            return null;
        },

        logout: function() {
          firebase.unauth();
          Auth.authData = Auth.currentUserInfo = null;
          $state.go('auth.signin').then(function() {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
          });
        },

        checkRegistration: function(googleId) {
          var deferred = $q.defer();
          firebase.child('users').child(googleId).once('value', function(data) {
            if (data.val() === null) {
              deferred.reject("User is not registered");
            } else {
              Auth.currentUserInfo = data.val();
              deferred.resolve(data.val());
            }
          });
          return deferred.promise;
        }
      };

      return Auth;
    }
  ]);

}());
(function() {

  angular.module('barber')

  .factory('Firebase', [
    '$http',
    '$q',
    function($http, $q) {
      return {
        getInstance: function() {
          if (!this.firebase) {
            this.firebase = new Firebase("https://clypr-7.firebaseio.com/");
          }

          return this.firebase;
        }
      };
    }
  ]);

}());
(function() {
  angular.module('barber')

  .factory('Router', [
    '$http',
    '$q',
    'OUT_APP_STATES',
    'IN_APP_STATES',
    '$rootScope',
    '$state',
    'Auth',
    'HOME_PAGE',
    '$timeout',
    function($http, $q, OUT_APP_STATES, IN_APP_STATES, $rootScope, $state, Auth, HOME_PAGE, $timeout) {

      var hasAccessRight = function(stateName, type) {
        return IN_APP_STATES[stateName] == type || IN_APP_STATES[stateName].indexOf(type) >= 0;
      }

      /*
      Change state start handler
    */
      var handlerChangeStart = function(events, prams, status) {

        if ($rootScope.checkingCurrentUser) {
          Auth.oldRouter = {
            name: prams,
            parameters: status
          };
          return events.preventDefault();
        }

        if (!Auth.isAuthenticated()) {
          if (OUT_APP_STATES[prams.name])
            return true;
          else {
            $state.go('auth.signin');
          }
        }

        // stop routing when can't get me info
        if (!Auth.type()) {
          Auth.oldRouter = {
            name: prams,
            parameters: status
          };
          return events.preventDefault();
        }

        /*
         * Go to home page when user is logged in and current state is sigin, signup
         */
        if (OUT_APP_STATES[prams.name]) {
          if (Auth.isAuthenticated())
            $timeout(function() {
              $state.go(HOME_PAGE[Auth.type()]);
            })
          else
            return true;
        } else {
          /*
           * Redirect when trying to access page that don't have permisison
           * for example an customer can't go to barber pages
           */
          /* 
        if (Auth.isAuthenticated() && !hasAccessRight(prams.name, Auth.type())) {
          events.preventDefault();
          $state.go(HOME_PAGE[Auth.type()]);
        }
        */
        }

        return true;

      }

      return {
        registerRouterChecker: function() {
          $rootScope.$on('$stateChangeStart', handlerChangeStart);

          $rootScope.$on('$stateChangeSuccess', function(events, prams, status) {

          });
        }
      }
    }
  ]);

}());
(function() {

  angular.module('barber')

  .factory('File', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        getContent: function(file) {
          var defer = $q.defer();
          var reader = new FileReader();

          reader.onload = function(fileContent) {
            defer.resolve(fileContent.target.result);
          }

          reader.readAsDataURL(file);
          return defer.promise;
        }
      }
    }
  ]);
}());
(function() {
  angular.module('barber')

  .factory('utils', [
    '$q',
    'Firebase',
    'Auth',
    '$ionicPopup',
    function($q, Firebase, Auth, $ionicPopup) {
      return {
        object2List: function(object, keyProp) {
          var list = [];
          var keys = Object.keys(object);
          for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var temp = object[key];
            temp[keyProp] = key;
            list.push(temp);
          }
          return list;
        },
        confirm: function(title, template) {
          var confirmDialog = $ionicPopup.confirm({
            title: title,
            template: template,
            cssClass: 'dialog confirm-dialog'
          });

          return confirmDialog;
        },
        showSuccess: function(msg) {
          var type;
          this.messageToast = $.simplyToast(" <i class='fa fa-check'></i>" + msg, type = 'success', {
            delay: 3000,
            align: 'center',
            allowDismiss: true
          });
          return setTimeout((function(_this) {
            return function() {
              if (_this.messageToast) {
                $.simplyToast.remove(_this.messageToast);
                return _this.messageToast = null;
              }
            };
          })(this), 5000);
        },
        showError: function(msg) {
          if (this.lastMsg === msg) {
            return;
          }
          if (this.errorToast) {
            $.simplyToast.remove(this.errorToast);
            this.errorToast = null;
          }
          this.errorToast = $.simplyToast(msg + " ", 'danger', {
            align: 'center'
          });
          setTimeout((function(_this) {
            return function() {
              _this.lastMsg = "";
              $.simplyToast.remove(_this.errorToast);
              return _this.errorToast = null;
            };
          })(this), 5000);
          return this.lastMsg = msg;
        },
      }
    }
  ]);

}());
(function() {
  angular.module('barber')

  .factory('Account', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        /*
        Register user service on firebase
       */
        register: function(email, password, name, type) {
          var defer = $q.defer();
          firebase.createUser({
            email: email,
            password: password
          }, function(error, userData) {
            if (error) {
              switch (error.code) {
                case "EMAIL_TAKEN":
                  defer.reject({
                    msg: "The new user account cannot be created because the email is already in use."
                  });
                  break;
                case "INVALID_EMAIL":
                  defer.reject({
                    msg: "The specified email is not a valid email."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error creating user:",
                    error: error
                  });
              }
            } else {

              // create user-data in users
              firebase.child('users').child(userData.uid).set({
                type: type,
                name: name
              }, function(error) {
                console.log(error);
                if (error) {
                  defer.reject(error)
                } else {
                  defer.resolve(userData);
                }
              })
            }
          });

          return defer.promise;
        },

        /*
        Change avatar service
       */
        changeAvatar: function(avatar) {
          var defer = $q.defer();
          var ref = firebase.child("users").child(Auth.authData.uid).child('avatar');

          ref.set(avatar, function(error) {
            defer.resolve(error);
          });

          return defer.promise;
        },

        /*
        Change Email
       */
        changeEmail: function(oldEmail, newEmail, password) {
          var defer = $q.defer();

          firebase.changeEmail({
            oldEmail: oldEmail,
            newEmail: newEmail,
            password: password
          }, function(error) {
            if (error) {
              switch (error.code) {
                case "INVALID_PASSWORD":
                  defer.reject({
                    msg: "The specified user account password is incorrect."
                  });
                  break;
                case "INVALID_USER":
                  defer.reject({
                    msg: "The specified user account does not exist."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error creating user:",
                    data: error
                  });
              }
            } else {
              defer.resolve(true);
            }
          });

          return defer.promise;
        },

        /*
        Change password
       */
        changePassword: function(email, oldPassword, newpassword) {
          var defer = $q.defer();

          firebase.changePassword({
            email: email,
            oldPassword: oldPassword,
            newPassword: newpassword
          }, function(error) {
            if (error) {
              switch (error.code) {
                case "INVALID_PASSWORD":
                  defer.reject({
                    msg: "The specified user account password is incorrect."
                  });
                  break;
                case "INVALID_USER":
                  defer.reject({
                    msg: "The specified user account does not exist."
                  });
                  break;
                default:
                  defer.reject({
                    msg: "Error changing password:",
                    data: error
                  });
              }
            } else {
              defer.resolve("User password changed successfully!");
            }
          });

          return defer.promise;
        }
      }
    }
  ]);

}());
(function() {
  angular.module('barber')

  .factory('Google', [
    '$q',
    'Firebase',
    'Auth',
    '$ionicLoading',
    function($q, Firebase, Auth, $ionicLoading) {
      var firebase = Firebase.getInstance();
      var clientId = '447590262163-99vrnebrbjk7lr2k9ctlvnuo28ltl270.apps.googleusercontent.com';
      var apiKey = 'AIzaSyBPjJ5YH2y5ChVniev8PVkXqIYdMyU-TYE';
      var googleScope = "email,https://www.googleapis.com/auth/calendar,https://www.googleapis.com/auth/plus.login,https://www.googleapis.com/auth/plus.me";

      return {
        /*
        Register user service on firebase
       */
        login: function() {
          var deferred = $q.defer();

          firebase.authWithOAuthPopup("google", function(error, authData) {
            if (error) {
              if (error.code === "TRANSPORT_UNAVAILABLE") {
                firebaseRef.authWithOAuthRedirect("google", function(error, authData) {
                  if (error) {
                    $ionicLoading.show({
                      template: 'Login Failed!',
                      noBackdrop: true,
                      duration: 2000
                    });
                  } else if (authData) {
                    // user authenticated with Firebase
                    deferred.resolve(authData);
                  }
                }, {
                  scope: googleScope
                });
              }
            } else if (authData) {
              deferred.resolve(authData);
            }
          }, {
            scope: googleScope
          });

          return deferred.promise;
        }

      }
    }
  ]);

}());
(function() {
  "use strict";

  angular.module('barber')
    .factory('GeoService', ['$rootScope', '$q', '$ionicLoading', 'Firebase', 'Auth', 'DEFAULT_SEARCH_SETTINGS',
      function($rootScope, $q, $ionicLoading, Firebase, Auth, DEFAULT_SEARCH_SETTINGS) {
        var listBarbers = {},
          geoQuery = null,
          firebase = Firebase.getInstance(),
          geoFire = new GeoFire(firebase.child("geofire")),
          radiusInKm = convertMilesToKm(DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles),
          watchLocation = null;

        // TODO : break this into two function
        function requestUserLocation(isBarber) {
          var defer = $q.defer();

          if (typeof navigator !== "undefined" && typeof navigator.geolocation !== "undefined") {
            navigator.geolocation.getCurrentPosition(function(location) {

              var latitude = location.coords.latitude;
              var longitude = location.coords.longitude;

              // update user location to firebase
              var userID = Auth.currentUserInfo.uid;
              var uSettings = firebase.child("settings/").child(userID);

              uSettings.update({
                latitude: latitude,
                longitude: longitude
              }, function() {});

              if (!isBarber) {
                // instantiate the geoFire query
                geoQuery = geoFire.query({
                  center: [latitude, longitude], // don't know why using real data doesnt work
                  radius: radiusInKm
                });

                __registerGeofileListener(geoQuery);

                defer.resolve(listBarbers);
              }

              defer.resolve({
                latitude: latitude,
                longitude: longitude
              });
            }, function __errorHandler(error) {
              defer.reject("You must allow us to access location before doing any search !!");
            });

            if (watchLocation) navigator.geolocation.clearWatch(watchLocation);

            watchLocation = navigator.geolocation.watchPosition(function(location) {

              var latitude = location.coords.latitude;
              var longitude = location.coords.longitude;

              // update user location to firebase
              var userID = Auth.currentUserInfo.uid;
              var uSettings = firebase.child("settings/").child(userID);

              uSettings.update({
                latitude: latitude,
                longitude: longitude
              }, function() {});

              if (!isBarber) {
                // instantiate the geoFire query
                geoQuery.updateCriteria({
                  center: [latitude, longitude],
                });
              }

            }, function __errorHandler(error) {
              defer.reject("You must allow us to access location before doing any search !!");
            });
          } else {
            defer.reject("Your browser does not support the HTML5 Geolocation API, so this will not work.");
          }

          return defer.promise;
        }

        // TODO implement watch user location to update to firebase
        function __registerGeofileListener(geoQuery) {

          geoQuery.on("ready", function() {
            console.log('geoQuery in ready state!!');
          });

          geoQuery.on("key_entered", function(barberId, barberLocation, distanceInKm) {
            var distanceInMiles = convertKmToMiles(distanceInKm);

            firebase.child("users").child(barberId).once("value", function(dataSnapshot) {
              var barber = dataSnapshot.val();

              if (!barber) return;

              barber.distance = +distanceInMiles.toFixed(2);
              listBarbers[barberId] = barber;

              firebase.child("settings").child(barberId).once('value', function(setting) {
                listBarbers[barberId] = angular.extend(barber, setting.val());
                $rootScope.$broadcast('BARBERS_DATA_CHANGE', listBarbers);
              });
            });
          });

          geoQuery.on("key_moved", function(barberId, barberLocation, distanceInKm) {
            if (!listBarbers[barberId]) return;

            var barber = listBarbers[barberId];
            var distanceInMiles = convertKmToMiles(distanceInKm);
            barber.distance = distanceInMiles;
          });

          geoQuery.on("key_exited", function(barberId, barberLocation, distanceInKm) {
            delete listBarbers[barberId];
          });
        }

        function updateKeyDistance() {
          var listLen = Object.keys(listBarbers).length;
          var key, tlat, tlng, tloc, tdisK, tdisM;

          for (key in listBarbers) {
            tlat = listBarbers[key].latitude;
            tlng = listBarbers[key].longitude;
            tloc = [tlat, tlng];
            tdisK = GeoFire.distance(tloc, center);
            tdisM = convertKmToMiles(tdisK);
            listBarbers[key].distance = +tdisM.toFixed(2); // the + ensures the result is a number (instead of string)
          }
        }

        function addBarberLocation(location) {
          var defer = $q.defer();
          geoFire
            .set(Auth.currentUserInfo.uid, [location.latitude, location.longitude])
            .then(function() {
              console.log('Update barber ' + Auth.currentUserInfo.uid + ' location ');
              defer.resolve(true);
            }, function(error) {
              console.log(error);
              defer.resolve(false);
            });

          return defer.promise;
        }

        function updateGeofireQueryCenter(lat, lng) {
          // center = [lat, lng];
          // radiusInKm = convertMilesToKm(DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles);
          // geoQuery = geoFire.query({
          //   center: center,
          //   radius: radiusInKm
          // });
          // updateKeyDistance();
        }

        function updateGeofireQueryRadius(radiusInMiles) {
          radiusInKm = convertMilesToKm(radiusInMiles);
          geoQuery.updateCriteria({
            radius: radiusInKm
          });
        }

        function getGeofireQueryResults() {
          var deferred = $q.defer();

          deferred.resolve(listBarbers);

          return deferred.promise;
        }

        function getGeofireQueryCenter() {
          return geoQuery.center();
        }

        function convertKmToMiles(km) {
          return km * 0.621371;
        }

        function convertMilesToKm(miles) {
          return miles / 0.621371;
        }

        return {
          requestUserLocation: requestUserLocation,
          updateKeyDistance: updateKeyDistance,
          updateGeofireQueryCenter: updateGeofireQueryCenter,
          updateGeofireQueryRadius: updateGeofireQueryRadius,
          getGeofireQueryResults: getGeofireQueryResults,
          getGeofireQueryCenter: getGeofireQueryCenter,
          convertKmToMiles: convertKmToMiles,
          convertMilesToKm: convertMilesToKm,
          addBarberLocation: addBarberLocation
        };

      }
    ]);
}());
(function() {

  angular.module('barber')
    .directive('onValidSubmit', ['$parse', '$timeout',
      function($parse, $timeout) {
        return {
          require: '^form',
          restrict: 'A',
          link: function(scope, element, attrs, form) {
            form.$submitted = false;
            var fn = $parse(attrs.onValidSubmit);
            element.on('submit', function(event) {
              scope.$apply(function() {
                element.addClass('ng-submitted');
                form.$submitted = true;
                if (form.$valid) {
                  if (typeof fn === 'function') {
                    fn(scope, {
                      $event: event
                    });
                  }
                }
              });
            });
          }
        }
      }
    ])
    .directive('validated', ['$parse', '$timeout',
      function($parse, $timeout) {
        return {
          restrict: 'AEC',
          require: '^form',
          link: function(scope, element, attrs, form) {
            var inputs = element.find("*");
            for (var i = 0; i < inputs.length; i++) {
              (function(input) {
                var attributes = input.attributes;
                if (attributes.getNamedItem('ng-model') != void 0 && attributes.getNamedItem('name') != void 0) {
                  var field = form[attributes.name.value];
                  if (field != void 0) {
                    scope.$watch(function() {
                      return form.$submitted + "_" + field.$valid;
                    }, function() {
                      if (form.$submitted != true) return;
                      $timeout(function() {
                        var inp = angular.element(input);
                        if (inp.hasClass('ng-invalid')) {
                          element.removeClass('has-success');
                          element.addClass('has-error');
                        } else {
                          element.removeClass('has-error').addClass('has-success');
                        }
                      }, 10);
                    });
                  }
                }
              })(inputs[i]);
            }
          }
        }
      }
    ])
    .directive('toggleClass', function() {
      return {
          restrict: 'A',
          link: function(scope, element, attrs) {
              element.bind('click', function() {
                  element.toggleClass(attrs.toggleClass);
              });
          }
      };
    });

}());
(function() {
  "use strict";

  angular.module('barber')
    .controller('MenuController', function($rootScope, $scope, $ionicModal, $timeout, $ionicHistory, $ionicSideMenuDelegate, $ionicLoading, Auth, UserService) {
      $scope.userInfo = Auth.currentUserInfo;
      $scope.userType = Auth.type();
      UserService.getSettings().then(function(settings) {
        $rootScope.userSettings = settings;
      });           
      $scope.menuUserLogout = function() {
        //$scope.closeMenu();
        $rootScope.logout();
        //$ionicLoading.show({ template: 'Logging Out', noBackdrop: true, duration: 2000 });
      }

      $scope.closeMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
        $ionicHistory.nextViewOptions({
          disableAnimate: true
        });
      }
    });
}());
(function() {
  "use strict";

  angular.module('barber')
    .directive('fileModel', [
      '$parse',
      function($parse) {
        return {
          restrict: 'A',
          link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
              scope.$apply(function() {
                if (attrs.multiple) {
                  modelSetter(scope, element[0].files);
                } else {
                  modelSetter(scope, element[0].files[0]);
                }
              });
            });
          }
        };
      }
    ]);

}());
(function() {
  "use strict";

  angular.module('barber')
    .service('BarberService', ['$q', 'GeoService', 'Firebase',
      function($q, GeoService, Firebase) {

        var lastLocation = null,
          cachedListings = [],
          firebase = Firebase.getInstance();

        return {
          getBarberInfo: getBarberInfo,
          getBarberDetail: getBarberDetail,
          getSingleBusiness: getSingleBusiness,
          getStatistics: getStatistics
        };

        function getBarberInfo(uid) {
          var defer = $q.defer();
          var users = firebase.child('users').child(uid);
          var user_settings = firebase.child('user_settings').child(uid);

          users.once('value', function (userInfo) {
            user_settings.once('value', function (settings) {
              defer.resolve(
                angular.extend({
                  settings: settings.val()
                }, {
                  userInfo: userInfo.val()
                })
              );
            })
          })

          return defer.promise;
        }

        function getBarberDetail(uid) {
          console.log(uid);
          var defer = $q.defer();
          var users = firebase.child('users').child(uid);
          var settings = firebase.child('settings').child(uid);        
          users.once('value', function (userInfo) {
            settings.once('value', function (settings) {
              defer.resolve(
                angular.extend({}, settings.val(), userInfo.val())
              );
            })
          })
          return defer.promise;

        }

        function getSingleBusiness(businessId) {
          var url,
            deferred = $q.defer();

          return deferred.promise;
        }

        function getStatistics(params){
          var defer = $q.defer();
          setTimeout(function(){
            defer.resolve({
              monthlyGoal: 2000,
              totalSoFar: 1500,
              totalHaircuts: 40,
              avgTip: 9,
              totalTips: 360
            })
          }, 100);
          
          return defer.promise;
        }
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .service('SalonService', ['$q', 'GeoService', 'Firebase',
      function($q, GeoService, Firebase) {

        var lastLocation = null,
          cachedListings = [],
          firebase = Firebase.getInstance();

        return {
          requestListBarber: requestListBarber,
          getBusiness: getBusiness,
          getSingleBusiness: getSingleBusiness,
          getCached: getCached
        };

        function requestListBarber(currentPage) {
          var deferred = $q.defer();

          GeoService.getGeofireQueryResults().then(function(geoData) {
            cachedListings = geoData;
            deferred.resolve(geoData);
          });

          return deferred.promise;
        }

        function getBusiness(businessId) {
          var deferred = $q.defer();
          var ref = firebase.child("settings").child(businessId);

          ref.once("value", function(data) {
            if (data)
              deferred.resolve(data.val());
            else
              deferred.reject();
          });

          return deferred.promise;
        }

        function getSingleBusiness(businessId) {
          var url,
            deferred = $q.defer();

          return deferred.promise;
        }

        function getCached() {
          var deferred = $q.defer();

          if (cachedListings && cachedListings.length) {
            setTimeout(function() {
              return deferred.resolve(cachedListings);
            });
          } else {
            getListings(0).then(function(lists) {
              deferred.resolve(lists);
            }, function(err) {
              deferred.reject(null);
            });
          }
          return deferred.promise;
        }

      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .service('MapService', ['$state',
      function($state) {

        return {
          getLocation: getLocation,
          getAll: getAll,
          pinSymbol: pinSymbol,
          drawMarkers: drawMarkers
        };

        function pinSymbol(color) {
          return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#222',
            strokeWeight: 2,
            scale: 1
          };
        }

        function getLocation(location) {
          var found = null;
          location = location.toLowerCase();
          angular.forEach(locations, function(value, key) {
            if (location.indexOf(key) !== -1) {
              console.log(angular.toJson(value));
              value.key = key;
              found = value;
            }
          });
          return found;
        }

        function getAll(getAll) {
          return locations;
        }

        function drawMarkers(myMap, locations) {
          var bounds = new google.maps.LatLngBounds();

          angular.forEach(locations, function(value, key) {
            var latLng = new google.maps.LatLng(value.latitude, value.longitude),
              marker = new google.maps.Marker({
                position: latLng,
                map: myMap,
                title: value.name,
                icon: pinSymbol('#FF2'),
                myKey: key,
                biz: value
              });

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', function() {
              $state.go('app.details', {
                id: this.biz.listingId
              });
            });
          });

          return bounds;
        };
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .service('BookingService', ['$http', '$q', '$ionicLoading', '$ionicPopup', 'Auth', 'Firebase', 'utils',
      function($http, $q, $ionicLoading, $ionicPopup, Auth, Firebase, utils) {
        var firebase = Firebase.getInstance();

        return {
          getBookedList: getBookedList,
          getUserBookings: getUserBookings,
          getBarberBookings: getBarberBookings,
          getBarberBookingsByDate: getBarberBookingsByDate,
          getBookings: getBookings,
          getListBookingsByBaberID: getListBookingsByBaberID,
          setBookingInfo: setBookingInfo,
          setBarberBooking: setBarberBooking,
          setUserBooking: setUserBooking,
          setUserBookingStatus: setUserBookingStatus,
          payment: payment,
          setCallendar: setCallendar
        };

        function setCallendar(clientId, event) {
          console.log(event);          
          var success = function(message) {
            var alertPopup = $ionicPopup.alert({
              title: 'Appointment Booked!',
              template: ''
            });            
          };
          var error = function(message) {
            $ionicPopup.alert({
              title: 'Set Calendar Error:',
              template: message
            });
          };
          // if you want to create a calendar with a specific color, pass in a JS object like this:
          if(window.plugins){
            var startDate = new Date(event.year,parseInt(event.month)-1,event.date,event.slot,0,0,0,0); // beware: month 0 = january, 11 = december
            var endDate = new Date(event.year,parseInt(event.month)-1,event.date,parseInt(event.slot)+1,0,0,0,0);
            var title = "Haircut appointments";
            var eventLocation = event.baber_info.barbershop+" - "+event.baber_info.address;
            var notes = "You have an appointment with the barber "+event.baber_info.name+" at "+event.slot+"h "+event.year+"-"+event.month+"-"+event.date+". Baber address: "+event.baber_info.address;
            var calOptions = window.plugins.calendar.getCalendarOptions();
            calOptions.firstReminderMinutes = 30; 
            window.plugins.calendar.createEventWithOptions(title,eventLocation,notes,startDate,endDate,calOptions,success,error);
          }else{
            console.log("calendar plugin not defined");
          }
        }

        function getBookedList(day, businessId) {
          var $defer = $q.defer();
          var formatedDay = moment(day).format('dd/mm/yyyy');
          var ref = firebase.child("barber_bookings/" + businessId);
          ref.equalTo(formatedDay).orderByChild("timestamp").on("value", function(snapshot) {
            return $defer.resolve(snapshot.val());
          });
          return $defer.promise;
        }

        function getUserBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('user_bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });

          return defer.promise;
        }

        function getBarberBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('barber_bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getBarberBookingsByDate(id, date) {
          var defer = $q.defer();
          var ref = firebase.child('barber_bookings/' + id + '/' + date).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getBookings(id) {
          var defer = $q.defer();
          var ref = firebase.child('bookings/' + id).on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function getListBookingsByBaberID(id) {
          console.log(id);
          var defer = $q.defer();
          var ref = firebase.child('bookings').on('value', function(snapshot) {
            return defer.resolve({
              success: true,
              data: snapshot.val()
            });
          }, function(err) {
            return defer.resolve({
              success: false,
              code: err.code
            });
          });
          return defer.promise;
        }

        function setBookingInfo(key, value) {
          var defer = $q.defer();
          var ref = firebase.child("bookings").child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setBarberBooking(barberId, timestamp, key, value) {
          var defer = $q.defer();
          var ref = firebase.child("barber_bookings").child(barberId).child(timestamp).child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setUserBooking(userId, key, value) {
          var defer = $q.defer();
          var ref = firebase.child("user_bookings").child(userId).child(key);
          ref.set(value, function(error) {
            defer.resolve(error);
          });
          return defer.promise;
        }

        function setUserBookingStatus(subPath, value) {
          var defer = $q.defer();
          var user_bookings = firebase.child(subPath.user);
          var barber_bookings = firebase.child(subPath.barber);
          var bookings = firebase.child(subPath.booking);
          user_bookings.set(value, function(error) {
            if (error) {
              defer.resolve({
                success: false,
                code: error.code
              });
            } else {
              bookings.set(value, function(error) {
                if (error) {
                  defer.resolve({
                    success: false,
                    code: error.code
                  });
                } else {
                  defer.resolve({
                    success: true
                  });
                }
              });
              // barber_bookings.set(value, function(error) {
              //   if (error) {
              //     defer.resolve({
              //       success: false,
              //       code: error.code
              //     });
              //   } else {
              //     bookings.set(value, function(error) {
              //       if (error) {
              //         defer.resolve({
              //           success: false,
              //           code: error.code
              //         });
              //       } else {
              //         defer.resolve({
              //           success: true
              //         });
              //       }
              //     });
              //   }
              // });
            }
          });
          return defer.promise;
        }

        function payment(paymentInfo) {
          var defer = $q.defer();
          var ref = firebase.child("user_bookings");
          ref.set(paymentInfo, function(error) {
            if (error) {
              defer.resolve({
                success: false,
                code: error.code
              });
            } else {
              defer.resolve({
                success: true
              });
            }
          });
          return defer.promise;
        }
      }
    ]);
}());
(function() {
  "use strict";
  angular.module('barber')

  .factory('UserService', [
    '$q',
    'Firebase',
    'Auth',
    function($q, Firebase, Auth) {
      var firebase = Firebase.getInstance();

      return {
        getSettings: getSettings,
        updateSettings: updateSettings
      };

      function getSettings() {
        var defer = $q.defer();

        var ref = firebase.child("settings").child(Auth.currentUserInfo.uid);

        ref.once("value", function(data) {
          defer.resolve(data.val());
        });

        return defer.promise;
      }

      function updateSettings(settings) {
        var defer = $q.defer();

        var ref = firebase.child("settings").child(Auth.currentUserInfo.uid);

        ref.update(settings, function(error) {
          defer.resolve(error);
        });

        return defer.promise;
      }
    }
  ]);

}());
(function() {
  'use strict';

  angular.module('barber')
    .controller('AuthController', [
      '$rootScope',
      '$scope',
      '$interval',
      '$ionicLoading',
      'Auth',
      '$state',
      '$ionicHistory',
      '$firebase',
      'Google',
      function($rootScope, $scope, $interval, $ionicLoading, Auth, $state, $ionicHistory, $firebase, Google) {

        function logoutCountDown(){
          $scope.counter = 30;
          $scope.stopCount = false;
          var count = $interval(function(){
            if ($scope.stopCount) {
              $ionicLoading.hide();
              $scope.firstTime = false;
              $scope.counter = 0;
              return $interval.cancel(count);
            }
            $scope.counter--;
            if(!$scope.counter){
              $interval.cancel(count);
              $ionicLoading.hide();
              $scope.firstTime = false;
              $rootScope.logout();
            } 
          }, 1000);
        }

        $scope.gUserLogin = function(isBarber) {
          // request authentication from google
          Google.login().then(function(uAuthData) {
            // checking in our firebase system
            Auth.checkRegistration(uAuthData.uid).then(function(data) {
              $rootScope.homeRedirect();
            }, function(error) {
              $scope.userInfoTemp = uAuthData;
              $scope.firstTime = true;
              Auth.logout();
              // logoutCountDown();
            });

          });
        }

        $scope.userType = {};
        $scope.setType = function(){
          if($scope.userInfoTemp){
            var uAuthData = $scope.userInfoTemp;
            uAuthData.type = $scope.userType.isBarber ? 'barber' : 'customer';
            uAuthData.active_code = $scope.userType.active_code;
            Auth
              .registerGoogleAccount(uAuthData)
              .then(function() {
                $scope.userInfoTemp = null;
                $scope.firstTime = false;
                $scope.stopCount = true;
                // $rootScope.homeRedirect();
                if($scope.userType.isBarber){
                  $state.go('app.barber-settings').then(function() {
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                  });
                }else{
                  $state.go('app.user-settings').then(function() {
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                  });
                }                
              },function(error){
                console.log(error);
                $scope.statusActive = error.data.error;
              });
          }
          else{
            $scope.userInfoTemp = null;
            $scope.firstTime = false;
          }
        }
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .service('AuthService', ['$firebase', '$ionicLoading',
      function($firebase, $ionicLoading) {
        var firebase = new Firebase("https://clypdev.firebaseio.com"); // Save firebase name in constants! Dont hardcode this here!!!

        var Auth = {
          googleLogin: function() {

            firebase.authWithOAuthPopup("google", function(error, authData) {
              if (error) {
                if (error.code === "TRANSPORT_UNAVAILABLE") {
                  firebase.authWithOAuthRedirect("google", function(error, authData) {
                    if (error) {
                      $ionicLoading.show({
                        template: 'Login Failed!',
                        noBackdrop: true,
                        duration: 2000
                      });
                    } else if (authData) {
                      uAuthData = authData;
                      // user authenticated with Firebase
                      $ionicLoading.show({
                        template: 'Login worked via Redirect!!!...Logged in as ' + authData.google.displayName + ' ...logging out!!!',
                        noBackdrop: true,
                        duration: 2000
                      });
                    }
                  });
                }
              } else if (authData) {
                uAuthData = authData;
                $ionicLoading.show({
                  template: 'Login worked via Popup!!!...Logged in as ' + authData.google.displayName + ' ...logging out!!!',
                  noBackdrop: true,
                  duration: 2000
                });
              }
            });

            return false;
          },

          login: function(email, password) {
            var defer = $q.defer();

            firebase.authWithPassword({
              email: email,
              password: password
            }, function(error, authData) {
              if (error) {
                defer.reject(error);
              } else {
                Auth.authData = angular.copy(authData);
                Auth.me().then(function(userInfo) {
                  defer.resolve(userInfo);
                });

              }
            });

            return defer.promise;
          },

          me: function() {
            var defer = $q.defer();

            var ref = firebase.child("users").child(Auth.authData.uid);

            ref.once("value", function(data) {
              Auth.currentUserInfo = data.val();
              defer.resolve(Auth.currentUserInfo);
            });

            return defer.promise;
          },

          checkCurrentToken: function() {
            var authData = firebase.getAuth();

            Auth.authData = authData;

            if (authData) {
              var defer = $q.defer();
              Auth.me().then(function(info) {
                defer.resolve(info);
              });
              return defer.promise;
            }

            return authData;
          },

          isAuthenticated: function() {
            return !!Auth.authData;
          },

          type: function() {
            if (Auth.currentUserInfo)
              return Auth.currentUserInfo.type;
            else
              return null;
          },

          logout: function() {
            firebase.unauth();
            Auth.authData = null;
          }
        };

        return Auth;

      }
    ]);
}());
(function() {
  'use strict';

  angular.module('barber')
    .controller('RegController', ['$rootScope', '$scope', '$ionicLoading', '$state', '$ionicHistory', '$firebase', '$q',
      function($rootScope, $scope, $ionicLoading, $state, $ionicHistory, $firebase, $q) {

        // // Send these vars to settings vars. That way they will be accessable from userPref
        // $scope.userFirstName = Settings.userFirstName;
        // $scope.userLastName = Settings.userLastName;
        // $scope.userZipCode = Settings.userZipCode;
        // $scope.userFavStyle = Settings.userFavStyle;
        // $scope.userFreq = Settings.userFreq;

        // var range = [{
        //   name: "1 mile",
        //   value: "1"
        // }, {
        //   name: "2 miles",
        //   value: "2"
        // }, {
        //   name: "5 miles",
        //   value: "5"
        // }, {
        //   name: "10 miles",
        //   value: "10"
        // }, {
        //   name: "20 miles",
        //   value: "20"
        // }];

        // var freq = [{
        //   name: "3 Days",
        //   value: "3"
        // }, {
        //   name: "Weekly",
        //   value: "7"
        // }, {
        //   name: "Every 2 Weeks",
        //   value: "14"
        // }, {
        //   name: "Monthly",
        //   value: "30"
        // }, {
        //   name: "Every 3 Months",
        //   value: "90"
        // }];

        // var area = [
        //   "16801",
        //   "33162",
        //   "33138"
        // ];

        // var style = [
        //   "Fade",
        //   "Blowout",
        //   "Afro"
        // ];

        // $scope.currentStyle = style;
        // $scope.currentFreq = freq;

        // $scope.onChange = function(type, value) {
        //   $scope[type] = value;
        //   Settings[type] = value;
        //   //console.log("$scope[" + type + "] = " + value);
        // };

        // $scope.gUserLogout = function() {
        //   $rootScope.logout();
        // }

        // $scope.regBasicSubmit = function() {
        //   $scope.regBasicFormCheck();
        //   $scope.regBasicFormCheck().then(function(data) {
        //     console.log("regBasicSubmit");
        //     console.log("inputFirstNameVar: " + Settings.userFirstName);
        //     console.log("inputLastNameVar: " + Settings.userLastName);
        //     console.log("inputZipCodeVar: " + Settings.userZipCode);
        //     $state.go('reg.userPref');
        //   }, function(error) {
        //     console.log(error);
        //     alert(error);
        //     return false;
        //   });
        // }

        // $scope.regPrefSubmit = function() {
        //   console.log("regPrefSubmit");
        //   console.log("inputFirstNameVar: " + Settings.userFirstName);
        //   console.log("inputLastNameVar: " + Settings.userLastName);
        //   console.log("inputZipCodeVar: " + Settings.userZipCode);
        //   console.log("userFavStyle: " + Settings.userFavStyle);
        //   console.log("userFreq: " + Settings.userFreq.value);
        //   console.log("userEmail: " + Settings.userEmail);

        //   var usersRef = $rootScope.fbase.child("users").child(Settings.userGoogleID);
        //   usersRef.set(Settings, function(error) {
        //     if (error) {
        //       console.log("Data could not be saved." + error);
        //     } else {
        //       console.log("Data saved successfully.");
        //       $state.go('tut.one').then(function() {
        //         $ionicHistory.clearHistory();
        //         $ionicHistory.clearCache();
        //       });
        //     }
        //   });
        // }

        // $scope.regBasicFormCheck = function() {
        //   var deferred = $q.defer();
        //   if ($scope.userFirstName === null || $scope.userFirstName === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- First Name missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- First Name missing");
        //   } else if ($scope.userLastName === null || $scope.userLastName === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- Last Name missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- Last Name missing");
        //   } else if ($scope.userZipCode === null || $scope.userZipCode === "") {
        //     console.log("regBasicFormCheck error!!");
        //     console.log("Please enter all required information -- Zip Code missing");
        //     //alert("Please enter all required information");
        //     deferred.reject("Please enter all required information -- Zip Code missing");
        //   } else {
        //     deferred.resolve("All information has been entered");
        //   }
        //   return deferred.promise;
        // }
      }
    ]);
}());
(function() {
  'use strict';

  angular.module('barber')
    .controller('TutController', ['$scope', '$ionicLoading', '$state', '$ionicHistory',
      function($scope, $ionicLoading, $state, $ionicHistory) {
        console.log("TutController launching");

        $scope.noAnimate = function() {
          $ionicHistory.nextViewOptions({
            disableAnimate: true
          });
        };

        $scope.pageTwo = function() {
          $state.go("tut.two");
        };
      }
    ]);
}());
(function() {
  'use strict';

  angular.module('barber')
    .controller('HomeController', ['$scope', '$ionicLoading', '$state', '$ionicHistory', 'Auth', '$ionicPopup',
      function($scope, $ionicLoading, $state, $ionicHistory, Auth, $ionicPopup) {
        $scope.userType = Auth.type();        
        $scope.$watch(function() {
          return Auth.type();
        }, function() {
          $scope.userType = Auth.type();
        });
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .controller("BarberSalonController", ['$scope', '$timeout', '$rootScope', 'SalonService', '$ionicLoading', 'DEFAULT_SEARCH_SETTINGS', 'GeoService', '$state',
      function($scope, $timeout, $rootScope, SalonService, $ionicLoading, DEFAULT_SEARCH_SETTINGS, GeoService, $state) {
        var currentPage = 0;
        $scope.showingListings = true;
        $scope.searchLocation = null;
        $scope.listBarbers = {};

        $scope.sortBarberOrderList = [{
          name: "First Name",
          value: "given_name"
        }, {
          name: "Last Name",
          value: "family_name"
        }, {
          name: "Distance (Nearest First)",
          value: "distance"
        }, ];

        $scope.geocodeOptions = {
          componentRestrictions: {
            country: 'US'
          }
        };

        $scope.MAXIMUM_SEARCH_RANGE = DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles;
        $scope.option = {
          searchRadiusInMiles: DEFAULT_SEARCH_SETTINGS.searchRadiusInMiles,
          currentSortBarberOrder: $scope.sortBarberOrderList[0].value
        }

        $scope.setShowingListings = function(nextState) {
          $scope.showingListings = nextState;
        };

        function init() {
          $ionicLoading.show({
            template: 'Get Listings...',
            noBackdrop: true,
            duration: 5000
          });

          GeoService
            .requestUserLocation()
            .then(function() {
              SalonService.requestListBarber().then(function(listBarbers) {                
                $scope.listBarbers = listBarbers;
                currentPage++;
              });
            });
        }

        $scope.countProperties = function(obj) {
          var count = 0;
          for (var index in obj) count++;

          return count;
        }

        $rootScope.$on('BARBERS_DATA_CHANGE', function(event, barbers) {
          $timeout(function() {
            $scope.listBarbers = barbers;
          })
        });

        $scope.refresh = function() {
          $scope.listBarbers = [];
          currentPage = 0;

          $ionicLoading.show({
            template: 'Refreshing...',
            noBackdrop: true,
            duration: 2000
          });

          SalonService.requestListBarber().then(function(listBarbers) {
            $ionicLoading.show({
              template: 'Get Listings...',
              noBackdrop: true,
              duration: 2000
            });
            $scope.listBarbers = listBarbers;
            currentPage++;
          });
          $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.updateSearch = function() {
          GeoService.updateGeofireQueryRadius($scope.option.searchRadiusInMiles);
          $state.go('salons.results');
        }

        init();
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .controller("BookingController", ['$scope', '$rootScope', 'SalonService', '$stateParams', '$state', 'stripe', '$filter', '$ionicLoading', '$http', 'BookingService', '$window', 'CONFIG', 'Auth', 'BarberService', 'UserService',
      function($scope, $rootScope, SalonService, $stateParams, $state, stripe, $filter, $ionicLoading, $http, BookingService, $window, CONFIG, Auth, BarberService, UserService) {

        Stripe.setPublishableKey(CONFIG.stripePublicKey);
        $scope.selectedStyle = {};
        $scope.bData = {
          date: $stateParams.date,
          startTime: $stateParams.startTime,
          endTime: $stateParams.startTime + 1
        };
        var paymentForm = {};
        var cutOptions = {};
        var sTmp = '';
        var sTmp2 = '';
        var blend = [
          "High",
          "Middle",
          "Low"
        ];
        var tapeBack = [
          "Square",
          "Round",
          "Fade"
        ];
        if (localStorage.getItem($state.params.id)) {
          setCutOptionsInfo(JSON.parse(localStorage.getItem($state.params.id)));
        } else {
          BarberService.getBarberDetail($state.params.id).then(function(detail_info) {
            if (!$.isEmptyObject(detail_info)) {
              localStorage.setItem($state.params.id, JSON.stringify(detail_info));
              setCutOptionsInfo(detail_info);
            }
          });
        }

        function setCutOptionsInfo(detail_info) {
          $rootScope.baber_info = detail_info;
          $scope.barberID = $state.params.id;
          $scope.barb = {};
          $scope.cardNumber = null;
          $scope.cardType = null;
          $scope.cardMonth = null;
          $scope.cardYear = null;
          $scope.cardCVC = null;
          $scope.transData = {};

          //BookingService.setCutPrice(150);           

          $scope.sErr = '';
          //$scope.isValid = stripe.card.validateCardNumber('4242424242424242');
          $scope.isNumValid = null;
          $scope.isExpValid = null;
          $scope.isCVCValid = null;



          $scope.specialRequests = "";

          $scope.styleVarList = detail_info.haircuts;
          // $scope.styleVar = "Fade";

          $scope.blendVarList = blend;
          $scope.blendVar = "Middle";

          $scope.topLength = 1.5;
          $scope.sideLength = 0.5;

          $scope.tapeBackVarList = tapeBack;
          $scope.tapeBackVar = "Square";
        }

        $scope.saveCutOptions = function() {
          if (!$scope.selectedStyle.styleVar) return;
          cutOptions.style = $scope.styleVarList[$scope.selectedStyle.styleVar].style;
          cutOptions.blend = $scope.blendVar;
          cutOptions.topLength = $scope.topLength;
          cutOptions.sideLength = $scope.sideLength;
          cutOptions.tapeBack = $scope.tapeBackVar;
          cutOptions.specialRequests = $scope.specialRequests;
          var date = new Date($state.params.date);
          var timestamp = moment(date).format('YYYY-MM-DD');
          var booking_key = $scope.barberID + "_" + Auth.currentUserInfo.uid + "_" + date.valueOf();
          var price = $scope.styleVarList[$scope.selectedStyle.styleVar].price;
          var now_date = Date.now();
          BookingService.setBookingInfo(booking_key, {
            'hairStyle': cutOptions,
            'price': price,
            'status': 'pending',
            'slot': $state.params.slot,
            'created_date': now_date,
            'booked_time': (new Date(date)).valueOf() + parseInt($state.params.slot) * 60 * 60 * 1000
          });
          BookingService.setBarberBooking($scope.barberID, timestamp, booking_key, $state.params.slot);
          BookingService.setUserBooking(Auth.currentUserInfo.uid, booking_key, 'pending');

          $state.go('app.booking-pay-info', {
            id: booking_key,
            barberId: $scope.barberID,
            date: $stateParams.date,
            startTime: $stateParams.slot,
            endTime: parseInt($stateParams.slot) + 1
          });
        }

        $scope.show = function() {
          $ionicLoading.show({
            template: 'Procesing Payment...'
          });
        };

        $scope.hide = function() {
          $ionicLoading.hide();
        };
        $scope.form = {};
        UserService.getSettings().then(function(settings) {
          localStorage.setItem('barber_user_info', JSON.stringify(settings));
          $scope.user_settings = settings;
          //console.log($scope.user_settings);
        });
        //XXX: booking-pay-info
        BookingService.getBookings($state.params.id).then(function(booking) {
          if (booking.data) {
            $scope.bookingInfo = booking.data;
            $scope.bookingInfo.bookingTime = {
              weekday: moment($stateParams.date).format('dddd'),
              month: moment($stateParams.date).format('MMM'),
              day: moment($stateParams.date).format('DD'),
              year: moment($stateParams.date).format('YYYY')
            };
          }
        });
        $scope.submitPayment = function(form) {
          $scope.barberID = $state.params.barberId;
          $scope.showError = true;
          // this.paymentForm.submit();
          // if (!$scope.form.paymentForm.$valid) return;

          $scope.show();
          // if (!$scope.user_settings) {
          //   $scope.user_settings = JSON.parse(localStorage.getItem('barber_user_info'));
          // }
          // Save card info into JS object
          UserService.getSettings().then(function(settings) {
            $scope.user_settings = settings;

            paymentForm.object = 'card';
            paymentForm.number = $scope.user_settings.payment.cardnumber;
            paymentForm.exp_month = $scope.user_settings.payment.cardmonth;
            paymentForm.exp_year = $scope.user_settings.payment.cardyear;
            paymentForm.cvc = $scope.user_settings.payment.cardcvc;
            paymentForm.customerName = Auth.currentUserInfo.name;
            paymentForm.description = 'CLYP - Booking for haircut';

            var postData = {
              chargeInfo: paymentForm,
              cutInfo: $scope.bData,
              bookingId: $stateParams.id,
              userId: Auth.currentUserInfo.uid
            };

            $http.post(CONFIG.clyprServerURL + '/api/booking/makeAppt', postData).
            then(function(res) {
              sTmp = 'HTTP Post Request Successful \n';
              sTmp = sTmp + 'Transaction ID: ' + res.data.confNum;
              BookingService.setCallendar(Auth.currentUserInfo.uid, {
                year: moment($stateParams.date).format('YYYY'),
                month: moment($stateParams.date).format('MM'),
                date: moment($stateParams.date).format('DD'),
                slot: $scope.bookingInfo.slot,
                baber_info: $rootScope.baber_info
              });

              $http({
                method: 'POST',
                url: CONFIG.clyprPushServerURL + '/send',
                data: {
                  ios: {
                    badge: 0,
                    alert: "You've got new booking from " + Auth.currentUserInfo.name
                  },
                  users: [$scope.barberID]
                }
              }).then(function successCallback(response) {
                console.log(response);
              }, function errorCallback(error) {
                console.log(error);
              });

              $rootScope.baber_info = false;
              $scope.hide();
              $state.go('app.home');
            }, function(err) {
              sTmp = 'HTTP Post Request Failed \n';
              sTmp = sTmp + err.data.message;
              alert(sTmp);
              $scope.hide();
            });
          });

        }

        function stripeResponseHandler(status, response) {
          if (response.error) {
            alert('Stripe Token Creation - Failed');
          } else {
            var token = response.id;
            alert('Stripe Token Creation - success');
          }
        }

        $scope.backToAppointmentPage = function() {
          $state.go('app.booking-time', {
            id: $state.params.id
          });
        }

        $scope.onChange = function(type, value) {
          $scope[type] = value;
          if (type === 'cardNumber') {
            sTmp = String(value);
            $scope.isNumValid = stripe.card.validateCardNumber(sTmp);
            $scope.cardType = stripe.card.cardType(sTmp);
          }
          if (type === 'cardMonth' || type === 'cardYear') {
            sTmp = String($scope.cardMonth);
            sTmp2 = String($scope.cardYear);
            $scope.isExpValid = stripe.card.validateExpiry(sTmp, sTmp2);
          }
          if (type === 'cardCVC') {
            sTmp = String(value);
            $scope.isCVCValid = stripe.card.validateCVC(sTmp);
          }
        };

        SalonService.getSingleBusiness($scope.barberID).then(function(barber) {
          if (barber) {
            $scope.barb = barber;
          }
        });
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .controller("BookingTimeController", ['$scope', 'BookingService', '$state', '$ionicLoading', 'SalonService', '$q', '$window','Auth', 'BarberService',
      function($scope, BookingService, $state, $ionicLoading, SalonService, $q, $window, Auth, BarberService) {
        var LIST_DAYS = {
          'Mon': 0,
          'Tue': 1,
          'Wed': 2,
          'Thu': 3,
          'Fri': 4,
          'Sat': 5,
          'Sun': 6
        };

        /*
          model for data-picker
        */

        $scope.currentSelectedDate = new Date();

        $scope.$watch('currentSelectedDate', function(_new) {
          if (_new) {
            $scope.shownDate = $scope.currentSelectedDate.toDateString();
          }
        });

        /*
          Get avaible time for current selected day
        */
        $scope.listAvailableTime = [];
        $scope.listBooked = []

        var getAvailableTimeForDay = function(day) {
          var barberId = $state.params.id;
          $scope.barberId = barberId;      
          if(localStorage.getItem(barberId)){
            _calculateWorkingTime(JSON.parse(localStorage.getItem(barberId)));
            _getBookedOrder(day, barberId).then(function(listBooked) {
              $scope.listBooked = listBooked || [];
              _calculateAvailableTime();
            })
          }else{
            _getBarberSetting(barberId).then(function(barberSettings) {
              localStorage.setItem(barberId, JSON.stringify(barberSettings));
              _calculateWorkingTime(barberSettings);
              _getBookedOrder(day, barberId).then(function(listBooked) {
                $scope.listBooked = listBooked || [];
                _calculateAvailableTime();
              })
            });
          }          
        }

        // calculate available time base on working slot and booked orders
        function _calculateAvailableTime() {

        }

        function _calculateWorkingTime(barberSettings) {
          var barberId = $state.params.id;
          var currentDay = moment($scope.currentSelectedDate).format('ddd');          
          var schedule = barberSettings.schedules ? barberSettings.schedules[LIST_DAYS[currentDay]] : null;
          if (!schedule) {
            $ionicLoading.show({
              template: 'There is no schedule data for this barber !!!',
              duration: 2000
            });
          }
          _getBarberBooked(barberId).then(function(barberBookings){
            _getListBookingsByBaberID(barberId).then(function(listBookings){
              $scope.listAvailableTime = schedule ? _generateSlotTime(schedule,barberBookings,listBookings) : [];
            });
          });
        }

        // generate slot time for each day depend on settings schedule               

        function _generateSlotTime(schedule,barberBookings,listBookings) {
          var counter = parseFloat(schedule.start);
          var endTime = parseFloat(schedule.end);
          var breakon = parseFloat(schedule.breakon);
          var breakoff = parseFloat(schedule.breakoff);
          var slots = [];

          function _detectBooked(detail_info,slot){  
            var flag = false;
            for(var key in detail_info.data) {
              if(slot == detail_info.data[key] && listBookings.data[key].status!='cancel'){
                flag = true;
              }
            }
            return flag;
          }
          function _detectTimeout(slot){
            var curTime = new Date();            
            var curYear = moment(curTime).format('YYYY');
            var curMonth = moment(curTime).format('MM');
            var curDate = moment(curTime).format('DD');
            var curHouse = moment(curTime).format('H');            
            var selectYear = moment($scope.currentSelectedDate).format('YYYY');
            var selectMonth = moment($scope.currentSelectedDate).format('MM');
            var selectDate = moment($scope.currentSelectedDate).format('DD');                        
            var flag = false;
            if(selectYear < curYear || selectMonth < curMonth || selectDate < curDate){
              flag = true;
            }
            if(selectYear == curYear && selectMonth == curMonth && selectDate == curDate && slot < curHouse){
              flag = true;
            }
            return flag;
          }
          while (counter < endTime) {
            if (counter < breakon || counter >= breakoff) {
              slots.push({
                timeStart: counter,
                timeEnd: counter + 1,
                booked: _detectBooked(barberBookings,counter),
                timeout: _detectTimeout(counter),
              });
            }
            counter += 1;
          }

          return slots;
        }

        // get barber settings
        function _getBarberSetting(barberid) {
          var defer = $q.defer();
          SalonService
            .getBusiness(barberid)
            .then(function(data) {
              defer.resolve(data);
            }, function(error) {
              defer.reject(error);
            });
          return defer.promise;
        }
        function _getBarberBooked(barberId){          
          var defer = $q.defer();
          var currentDay = moment($scope.currentSelectedDate).format('YYYY-MM-DD');           
          BookingService.getBarberBookingsByDate(barberId,currentDay).then(function(data) {
            defer.resolve(data);
          }, function(error) {
            defer.reject(error);
          });
          return defer.promise;
        }
        function _getListBookingsByBaberID(barberId){          
          var defer = $q.defer();
          BookingService.getListBookingsByBaberID(barberId).then(function(data) {
            defer.resolve(data);
          }, function(error) {
            defer.reject(error);
          });
          return defer.promise;
        }
             
        // get boooked list
        function _getBookedOrder(day, barberId) {
          var defer = $q.defer();
          BookingService
            .getBookedList(day, barberId)
            .then(function(data) {
              defer.resolve(data);
            }, function(error) {
              defer.reject(error);
            });

          return defer.promise;
        }

        // event listener when user select day
        $scope.formatDate = function (date) {
            function pad(n) {
                return n < 10 ? '0' + n : n;
            }

            return date && date.getFullYear()
                + '-' + pad(date.getMonth() + 1)
                + '-' + pad(date.getDate());
        };

        $scope.parseDate = function (s) {
            var tokens = /^(\d{4})-(\d{2})-(\d{2})$/.exec(s);

            return tokens && new Date(tokens[1], tokens[2] - 1, tokens[3]);
        };
        $scope.onSelectDay = function(type, day) {          
          //console.log(day);
          $scope.currentSelectedDate = new Date(day);
          getAvailableTimeForDay($scope.currentSelectedDate);
        }

        // request avaiable time for current day
        getAvailableTimeForDay($scope.currentSelectedDate);

        // event when user select a slot
        $scope.onUserSelectDateRange = function(slot) {
          var isoDate = $scope.currentSelectedDate.toISOString();
          // $window.location.href = '#/booking/cut-options/' + $state.params.id + '?date=' + isoDate + '&slot=' + slot.timeStart;
          $state.go('app.booking-cut-options', {
            id: $state.params.id,
            date: isoDate,
            slot: slot.timeStart
          });
        }
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .directive("bookingCalendar", function() {

      return {
        restrict: "E",
        templateUrl: "page/booking/calendar-template.html",
        scope: {
          selected: "="
        },
        link: function(scope) {
          scope.selected = _removeTime(scope.selected || moment());
          scope.month = scope.selected.clone();
          var start = scope.selected.clone();
          start.date(1);
          _removeTime(start.day(0));
          _buildMonth(scope, start, scope.month);
          scope.select = function(day) {
            scope.selected = day.date;
          };
          scope.next = function() {
            var next = scope.month.clone();
            _removeTime(next.month(next.month() + 1).date(1));
            scope.month.month(scope.month.month() + 1);
            _buildMonth(scope, next, scope.month);
          };
          scope.previous = function() {
            var previous = scope.month.clone();
            _removeTime(previous.month(previous.month() - 1).date(1));
            scope.month.month(scope.month.month() - 1);
            _buildMonth(scope, previous, scope.month);
          };
        }
      };

      function _removeTime(date) {
        //return date.day(0).hour(0).minute(0).second(0).millisecond(0);
        return date.hour(0).minute(0).second(0).millisecond(0);
      }

      function _buildMonth(scope, start, month) {
        scope.weeks = [];
        var done = false,
          date = start.clone(),
          monthIndex = date.month(),
          count = 0;
        while (!done) {
          scope.weeks.push({
            days: _buildWeek(date.clone(), month)
          });
          date.add(1, "w");
          done = count++ > 2 && monthIndex !== date.month();
          monthIndex = date.month();
        }
      }

      function _buildWeek(date, month) {
        var days = [];
        for (var i = 0; i < 7; i++) {
          days.push({
            name: date.format("dd").substring(0, 1),
            number: date.date(),
            isCurrentMonth: date.month() === month.month(),
            isToday: date.isSame(new Date(), "day"),
            date: date
          });
          date = date.clone();
          date.add(1, "d");
        }
        return days;
      }

    });
}());
angular.module('barber')

.controller('UserAppointmentsController', [
  '$scope','$ionicLoading',
  function($scope, $ionicLoading){
    $scope.sliderToggle = 0;

    // pass in event source in this link
    $scope.eventSources = [];
    $ionicLoading.show({
      template: 'Loading...'
    });
    $scope.uiConfig = {
      calendar:{
        titleFormat: 'MMMM',
        header: {
          left: 'YYYY',
          center: 'prev, title next',
          right: ''
        },
        defaultDate: new Date(),
        viewRender: function(view) {
          $ionicLoading.hide();
          $scope.calendarYear = view.start.format().substr(0, 4);              
        }
      }
    };
  }
]);

angular.module('barber')
  .controller('UserBookingHistory', ['$scope', 'BookingService', 'BarberService', 'utils', 'Auth', '$timeout', '$ionicLoading', '$ionicModal', '$http', 'CONFIG', 'UserService', '$ionicPopup',
    function($scope, BookingService, BarberService, utils, Auth, $timeout, $ionicLoading, $ionicModal, $http, CONFIG, UserService, $ionicPopup) {
      var uid = Auth.currentUserInfo.uid;
      $scope.type = Auth.currentUserInfo.type;

      function getBookings() {
        $scope.getting = true;
        $ionicLoading.show({
          template: 'Get Listings...',
          noBackdrop: true
        });
        $scope.bookings = [];
        if ($scope.type == 'customer') {
          BookingService.getUserBookings(uid).then(function(res) {
            if (res.success && res.data) {
              $scope.userBookings = res.data;
              for (var ub in $scope.userBookings) {
                if ($scope.userBookings[ub] == 'pending')
                  delete $scope.userBookings[ub];
              }
              if ($scope.userBookings) {
                var i = 0;
                for (var ub in $scope.userBookings) {
                  i++;
                  var barberId = ub.split('_')[0];
                  (function(barberId, ub) {
                    if (localStorage.getItem(barberId)) {
                      setBarberDetailInfo(JSON.parse(localStorage.getItem(barberId)), ub);
                    } else {
                      BarberService.getBarberDetail(barberId).then(function(res) {
                        localStorage.setItem(barberId, JSON.stringify(res));
                        setBarberDetailInfo(res, ub);
                      });
                    }

                    function setBarberDetailInfo(res, ub) {
                      var bk = angular.extend({}, res);
                      BookingService.getBookings(ub).then(function(res) {
                        if (res.success && res.data) {
                          bk = angular.extend(bk, res.data);
                          $timeout(function() {
                            bk.bookingId = ub;
                            bk.bookedDate = moment(new Date(bk.booked_time)).format('dddd MMM D YYYY');
                            $scope.bookings.push(bk);
                            if (i >= $scope.bookings.length) {
                              $ionicLoading.hide();
                              $scope.$broadcast("scroll.refreshComplete");
                              $scope.getting = false;
                            }
                          });
                        }
                      });
                    }

                  })(barberId, ub);
                }
              } else {
                $ionicLoading.hide();
                $scope.$broadcast("scroll.refreshComplete");
                $scope.getting = false;
              }
            } else {
              $ionicLoading.hide();
              $scope.$broadcast("scroll.refreshComplete");
              $scope.getting = false;
            }
          });
        } else {
          BookingService.getBarberBookings(uid).then(function(res) {
            if (res.success && res.data) {
              $scope.userBookings = {};
              for (var date in res.data) {
                $scope.userBookings = angular.extend($scope.userBookings, res.data[date]);
              }
              if ($scope.userBookings) {
                var i = 0;
                for (var ub in $scope.userBookings) {
                  i++;
                  var customerId = ub.split('_')[1];
                  (function(customerId, ub) {
                    BarberService.getBarberDetail(customerId).then(function(res) {
                      console.log(res);
                      var bk = angular.extend({}, res);
                      BookingService.getBookings(ub).then(function(res) {
                        if (res.success && res.data) {
                          bk = angular.extend(bk, res.data);
                          $timeout(function() {
                            bk.bookingId = ub;
                            bk.createdDate = moment(new Date(bk.created_date)).format('dddd MMM D YYYY');
                            if (bk.status != 'pending')
                              $scope.bookings.push(bk);
                            if (i >= $scope.bookings.length) {
                              $ionicLoading.hide();
                              $scope.$broadcast("scroll.refreshComplete");
                              $scope.getting = false;
                            }
                          });
                        }
                      });
                    });
                  })(customerId, ub);
                }
              } else {
                $ionicLoading.hide();
                $scope.$broadcast("scroll.refreshComplete");
                $scope.getting = false;
              }
            } else {
              $ionicLoading.hide();
              $scope.$broadcast("scroll.refreshComplete");
              $scope.getting = false;
            }
          });
        }
      }

      getBookings();
      $scope.refresh = function() {
        getBookings();
      }

      $scope.updateStatus = function(booking, status) {
        var confirm = utils.confirm('Update Status', 'Are you sure you want to ' + status + ' this booking?').then(function(res) {
          if (res) {
            var barberId = booking.bookingId.split('_')[0];
            var path = {
              user: 'user_bookings/' + uid + '/' + booking.bookingId,
              barber: 'barber_bookings/' + barberId + '/' + booking.bookingId,
              booking: 'bookings/' + booking.bookingId + '/status'
            }
            BookingService.setUserBookingStatus(path, status).then(function(res) {
              if (res.success) {
                booking.status = status;
              }
            });
          }
        });
      }

      $scope.modal = {};
      $scope.openPaymentModal = function(booking) {
        console.log(booking);
        $scope.paymentInfo = {
          bookingId: booking.bookingId,
          status: booking.status,
          cardNumber: ''
        };

        $ionicLoading.show({
          template: 'Processing ...',
          noBackdrop: true
        });

        // get user settings
        var paymentForm = {};
        UserService
          .getSettings()
          .then(function(settings) {
            $ionicLoading.hide();


            if (!settings.payment.cardnumber ||
              !settings.payment.cardmonth ||
              !settings.payment.cardyear ||
              !settings.payment.cardcvc) {
              return swal("Oops...", "You haven't input the payment info ", "error");
            }

            settings.customerName = Auth.currentUserInfo.name;
            var temp = settings;
            delete temp.avatar;
            localStorage.setItem('user_settings', JSON.stringify(temp));

            // do the payment
            paymentForm.object = 'card';
            paymentForm.number = settings.payment.cardnumber;
            paymentForm.exp_month = settings.payment.cardmonth;
            paymentForm.exp_year = settings.payment.cardyear;
            paymentForm.cvc = settings.payment.cardcvc;
            paymentForm.customerName = Auth.currentUserInfo.name;
            paymentForm.description = 'CLYP - Pay money for haircut';

            $ionicLoading.show({
              template: 'Paying...',
              noBackdrop: true
            });

            $http.post(CONFIG.clyprServerURL + '/api/stripe/cardCharge', {
              chargeInfo: paymentForm,
              bookingId: $scope.paymentInfo.bookingId,
              userId: Auth.currentUserInfo.uid
            }).
            then(function(res) {
              if (res.data.err || res.data.status != "succeeded") {
                $ionicLoading.hide();
                return swal('Oops', res.data.msg || res.data.message || "Server return error - try again later !!", 'error');
              }
              $ionicLoading.hide();
              $ionicModal.fromTemplateUrl('scripts/pages/user/booking_history/payment/view.html', {
                scope: $scope,
                backdropClickToClose: false,
                hardwareBackButtonClose: false
              }).then(function(modal) {
                $scope.modal = modal;
                if (booking.status == 'booked')
                  $scope.modal.show();
              }, function() {
                getBookings();
              });
            }, function(err) {
              console.log(err);
            });

            // request tips
          });
      }

      $scope.paymentInfo = {};
      $scope.setTip = function(value) {
        $scope.paymentInfo.tips = value;
      }

      $scope.payment = function() {

        var paymentForm = {};
        // Save card info into JS object

        var userSettings = JSON.parse(localStorage.getItem('user_settings'));

        paymentForm.object = 'card';
        paymentForm.number = userSettings.payment.cardnumber;
        paymentForm.exp_month = userSettings.payment.cardmonth;
        paymentForm.exp_year = userSettings.payment.cardyear;
        paymentForm.cvc = userSettings.payment.cardcvc;
        paymentForm.customerName = userSettings.customerName;
        paymentForm.description = 'CLYP - Pay tips for barber';
        paymentForm.tips = $scope.paymentInfo.tips;
        if (paymentForm.tips==undefined) {
          return swal("Oops...", "You must add the tip !!", "error");
        }else if(paymentForm.tips==0){
          $scope.modal.hide();
          getBookings();          
          return true;
        }

        $ionicLoading.show({
          template: 'Paying...',
          noBackdrop: true
        });

        $http.post(CONFIG.clyprServerURL + '/api/stripe/paytips', {
          chargeInfo: paymentForm,
          bookingId: $scope.paymentInfo.bookingId,
          userId: Auth.currentUserInfo.uid
        })
          .then(function(res) {
            if (res.data.status == "succeeded") {
              $ionicLoading.hide();
              $scope.modal.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Payment Successful',
                // template: "Payment Successful"
              });
              alertPopup.then(function(res) {
                getBookings();
              });              
            } else {
              $ionicLoading.hide();
              return swal('Oops', res.data.message, 'error');
            }

          }, function(err) {
            console.log(err);
            $ionicLoading.hide();
          });

        $scope.modal.hide();

      }

      $scope.show_list = function(status, event) {
        $('#status_button .button').removeClass('button-positive').addClass('button-dark');
        $(event.target).removeClass('button-dark').addClass('button-positive');
        $('.ubh_item').css({
          display: 'none'
        });
        $('.' + status).css({
          display: 'block'
        });
      }
    }
  ]);
angular.module('barber')
  .directive('autoResizeImage', function($compile) {
    return {
      restrict: "EA",
      link: function(scope, elm, attrs) {

        var img = elm.find('img');

        function resizeImage() {
          var width = $(elm).width();
          if (width <= 138) {
            img.css({
              'width': width + 'px',
              'height': width + 'px'
            });
          } else {
            img.css({
              'width': '128px',
              'height': '128px'
            });
          }
        }

        scope.$watch(function() {
          return $(elm).width();
        }, resizeImage);

        $(window).bind('resize', function() {
          resizeImage();
        });
      }
    };
  });
(function() {
  'use strict';
  angular.module('barber')
    .controller('UserSettingsController', ['$scope', '$rootScope', '$timeout', '$ionicLoading', 'Auth', 'UserService', 'File', 'Account',
      function($scope, $rootScope, $timeout, $ionicLoading, Auth, UserService, File, Account) {
        $ionicLoading.show({
          template: 'Loading...'
        });

        $scope.settings = {
          email: Auth.currentUserInfo.email,
          payment: {
            type: 'card'
          }
        };

        UserService
          .getSettings()
          .then(function(settings) {
            $ionicLoading.hide();
            $scope.settings = angular.extend($scope.settings, settings);
            $timeout(function () {$('input[name="cardNumber"]').inputCloak({type: 'credit'});});
            listenSettingChange();
          });

        function listenSettingChange() {
          $scope.$watchCollection('settings', function(newVal, oldVal) {
            if (oldVal)
              UserService
              .updateSettings(newVal)
              .then(function(error) {});
          })
          $scope.$watchCollection('settings.payment', function(newVal, oldVal) {
            if (oldVal)
              UserService
              .updateSettings($scope.settings)
              .then(function(error) {});
          })
        }
        $scope.cardnumberChange = function(newVal) {
          $timeout(function () {
            $scope.settings.payment.cardnumber = newVal;
          });
        }
        $scope.avatarUploadChange = function(newVal) {
          if (newVal) {
            File
              .getContent(newVal)
              .then(function(content) {
                $scope.settings.avatar = content;
                $rootScope.userSettings.avatar = content;
              });
          }
        };
      }
    ]);
}());
(function() {
  "use strict";
  angular.module('barber')
    .controller("BarberDetailsController", ['$scope', '$stateParams', '$state', '$ionicHistory', 'BarberService',
      function($scope, $stateParams, $state, $ionicHistory, BarberService) {
        $scope.biz = {};
        $scope.barberId = $stateParams.id;
        $scope.$on('mapInitialized', function(event, map) {
          if(localStorage.getItem($stateParams.id)){
            setBaberDetailInfo(JSON.parse(localStorage.getItem($stateParams.id)), map);
          }else{
            BarberService.getBarberDetail($stateParams.id).then(function(detail_info) {
              if (detail_info) {
                localStorage.setItem($stateParams.id, JSON.stringify(detail_info));
                setBaberDetailInfo(detail_info, map);
              }
            });            
          }
        });
        function setBaberDetailInfo(detail_info, map){
          console.log(detail_info);
          $scope.biz = detail_info;
          var location = new google.maps.LatLng(detail_info.barberLocation.latitude, detail_info.barberLocation.longitude);
          var marker = new google.maps.Marker({
            position: location,
            map: map
          });
          map.panTo(location);
        }
      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .controller("MapController", ['$scope', 'MapService', 'LocationService', 'ListingsService',
      function($scope, MapService, LocationService, ListingsService) {
        //            var locations = MapService.getAll();
        var locations = [],
          position = {};

        console.log("waiting for mapInit");

        $scope.$on('mapInitialized', function(event, map) {

          LocationService.getCurrentPosition().then(function(position) {
            console.log("Got a fresh position: " + JSON.stringify(position.coords));
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(latLng);

            ListingsService.getCached().then(function(list) {
              var bounds = MapService.drawMarkers(map, list);
              map.fitBounds(bounds);
            });
          });
        });
      }
    ]);
}());
(function() {
  'use strict';
  angular.module('barber')
    .controller('BarberSettingsController', ['$scope', '$rootScope', '$timeout', '$ionicLoading', 'Auth', 'UserService', 'Account', 'GeoService', 'File',
      function($scope, $rootScope, $timeout, $ionicLoading, Auth, UserService, Account, GeoService, File) {
        $scope.settings = {
          email: Auth.currentUserInfo.email,
          payment: {},
          paymentType: 'Paypal',
        };

        $scope.option = {
          avatar: ""
        }
        $scope.map = {};

        $scope.$on('mapInitialized', function(event, map) {
          UserService
            .getSettings()
            .then(function(settings) {
              $scope.settings = angular.extend($scope.settings, settings);
              $timeout(function () {$('input[name="cardNumber"]').inputCloak({type: 'credit'});});
              if (typeof $scope.settings.payment == 'string')
                $scope.settings.payment = {};
              listenSettingChange();

              if ($scope.settings.barberLocation) {
                initMapBarberMarker(map, $scope.settings.barberLocation);
              } else {
                GeoService
                  .requestUserLocation(true)
                  .then(function(location) {

                    initMapBarberMarker(map, location);
                  });
              }
            });
        });

        function initMapBarberMarker(map, location) {
          // if (!$scope.settings.barberLocation) {
          //   GeoService.addBarberLocation(location);
          // };
          $scope.settings.barberLocation = {
            latitude: location.latitude,
            longitude: location.longitude
          };

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.latitude, location.longitude),
            map: map,
            draggable: true,
            title: 'Your shop location'
          });

          map.panTo(new google.maps.LatLng(location.latitude, location.longitude));


          google.maps.event.addListener(marker, "drag", function(event) {
            // event.preventDefault();
          });

          google.maps.event.addListener(marker, "dragend", function(event) {
            $scope.settings.barberLocation.latitude = event.latLng.lat();
            $scope.settings.barberLocation.longitude = event.latLng.lng();
            GeoService.addBarberLocation($scope.settings.barberLocation);
          });
        }

        function _updateSettingToFirebase(newVal) {
          UserService
            .updateSettings(newVal)
            .then(function(error) {});
        }

        function listenSettingChange() {
          $scope.$watchCollection('settings', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase(newVal);
          });

          $scope.$watchCollection('settings.payment', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          })

          $scope.$watch('settings.schedules', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);

          $scope.$watch('settings.haircuts', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);

          $scope.$watch('settings.barberLocation', function(newVal, oldVal) {
            if (oldVal)
              _updateSettingToFirebase($scope.settings);
          }, true);
        }

        $scope.cardnumberChange = function(newVal) {
          $timeout(function () {
            $scope.settings.payment.cardnumber = newVal;
          });
        }
        //avatar
        $scope.avatarUploadChange = function(newVal) {
          if (newVal) {
            File
              .getContent(newVal)
              .then(function(content) {
                $scope.settings.avatar = content;
                $rootScope.userSettings.avatar = content;
              });
          }
        };

      }
    ]);
}());
(function() {
  "use strict";

  angular.module('barber')
    .controller("BarberReviewsController", ['$scope', 'MapService', 'LocationService', 'BarberService', '$stateParams',
      function($scope, MapService, LocationService, BarberService, $stateParams) {

        $scope.id = $stateParams.id;
        $scope.barb = {};

        BarberService.getSingleBusiness($scope.id).then(function(barber) {
          if (barber) {
            $scope.barb = barber;
          }
        });
      }
    ]);
}());
angular.module('barber')

.controller('BarberStatisticController', [
  '$scope',
  'BarberService',
  function($scope, BarberService) {
    $scope.statistic = {};
    $scope.sliderToggle = 0;

    $scope.currentPeriod = "1";
	
	/*
		Get Statistic info throught services
		here you don't give me real api so i fake data,
		refer the barber_statistics.service.js for more info, i wrote an fake function for test and a real $http request(that commented).
	*/
	BarberService.getStatistics().then(function(result){
		result.needHairCuts = Math.ceil((result.monthlyGoal - result.totalSoFar - result.totalTips)/result.avgTip);
		$scope.statistic = result;
	})

    //bar data
    $scope.barChart = {
      "options": {
        "chart": {
          "backgroundColor": "transparent",
          "type": "areaspline",
          "width": $(document).width()
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        }
      },
      xAxis: {
        categories: ['Total So Far', 'Need to Reach Goal', 'Total From tips'],
        minPadding: 30
      },
      yAxis: {
        stackLabels: {
          style: {
            color: 'white'
          },
          enabled: true,
          x: -20,
        }
      },
      "series": [{
        "data": [1, 3, 5],
        "type": "column"
      }],
      title: {
        text: 'October',
        userHTML: true
      }
    }

    // line data
    $scope.lineChart = {
      "options": {
        "chart": {
          "backgroundColor": "transparent",
          "type": "line",
          "width": $(document).width()
        },
        "plotOptions": {
          "series": {
            "stacking": ""
          }
        }
      },
      "series": [{
        "data": [
          2,
          13,
          16,
          3,
          10,
          17,
          20,
          19,
          6,
          16
        ],
        "id": "series-4"
      }],

      title: {
        text: 'October',
        userHTML: true
      },

      //function (optional)
      func: function(chart) {
        console.log(chart);
        setTimeout(function() {
          var baseX = chart.plotLeft;
          var baseY = chart.chartHeight - chart.marginBottom;

          // calculate goal line
          var maxGoal = 22;
          var totalSofar = 1500;
          var goalLine = chart.yAxis[0].height / chart.yAxis[0].max * maxGoal;

          // draw goal text
          chart.renderer.text('GOAL', baseX - 50, baseY - goalLine)
            .css({
              color: 'white',
              fontSize: '16px'
            })
            .add();

          chart.renderer.path(['M', baseX, baseY - goalLine, 'L', baseX + chart.xAxis[0].width - 30, baseY - goalLine])
            .attr({
              'stroke-width': 4,
              stroke: '#00b4ff'
            })
            .add();

          chart.renderer.circle(baseX + chart.xAxis[0].width - 25, baseY - goalLine, 5)
            .attr({
              fill: '#00b4ff'
            })
            .add();

          // chart.renderer
          chart.renderer.label('$' + maxGoal, baseX + chart.xAxis[0].width - 80, baseY - goalLine - 40, 'callout')
            .css({
              color: '#FFFFFF'
            })
            .attr({
              fill: '#00b4ff',
              r: 5,
              zIndex: 6
            })
            .add();

          // total so far text
          var point = chart.series[0].points[chart.series[0].points.length - 1];
          chart.renderer.label('$' + totalSofar, point.plotX + chart.plotLeft - 60, point.plotY + chart.plotTop - 10, 'callout')
            .css({
              color: '#FFFFFF'
            })
            .attr({
              fill: '#00b4ff',
              r: 5,
              zIndex: 6
            })
            .add();

        }, 100);

      }
    }


    // donut pie
    $scope.donutChart = {
      options: {
        chart: {
          "backgroundColor": "transparent",
          "width": $(document).width(),
          type: 'pie'
        },

        plotOptions: {
          pie: {
            borderColor: '#000000',
            innerSize: '60%'
          }
        },
      },
      series: [{
        data: [
          ['Done', 60],
          ['Not Done', 40],
        ],
        size: '60%'
      }],
      title: {
        text: 'October',
        userHTML: true
      }
    };

  }
]);
angular.module('barber')
.controller('BarberAppointmentsController', ['$scope', 'BookingService', 'BarberService', 'utils', 'Auth', '$timeout', '$ionicLoading', '$filter',
  function ($scope, BookingService, BarberService, utils, Auth, $timeout, $ionicLoading, $filter) {
    var uid = Auth.currentUserInfo.uid;
    $scope.type = Auth.currentUserInfo.type;    
      $ionicLoading.show({
        template: 'Get Listings...',
        noBackdrop: true
      });
      $scope.bookings = [];
      BookingService.getBarberBookings(uid).then(function(res){        
        if(res.success && res.data){
          $scope.userBookings = {};
          var date = new Date();
          var nowDate = moment(date).format('YYYY-MM-DD');
          var nowHouse = moment(date).format('H');
          for(var date in res.data){            
            var $obj = res.data[date];
            var $slot = $obj[Object.keys($obj)];            
            if(date > nowDate || (date == nowDate && $slot > nowHouse)){              
              $scope.userBookings = angular.extend($scope.userBookings, res.data[date]);                
            }            
          }
          if(!$.isEmptyObject($scope.userBookings)){
            var i = 0;
            for(var ub in $scope.userBookings){
              i++;              
              var customerId = ub.split('_')[1];              
              (function(customerId, ub){
                BarberService.getBarberDetail(customerId).then(function(res){
                  var bk = angular.extend({}, res);console.log(bk);
                  var bookingTime = parseInt(ub.split('_')[2]);
                  BookingService.getBookings(ub).then(function(res){
                    if(res.success && res.data){
                      bk = angular.extend(bk, res.data);
                      bk.bookingTime = {
                        weekday: moment(bookingTime).format('dddd'),
                        month: moment(bookingTime).format('MMM'),
                        day: moment(bookingTime).format('DD'),                        
                        year: moment(bookingTime).format('YYYY')
                      };
                      $timeout(function(){
                        bk.bookingId = ub;                        
                        if(bk.status != 'pending' && bk.status != 'done')
                          $scope.bookings.push(bk);
                        if(i >= $scope.bookings.length){
                          $ionicLoading.hide();
                        }
                      });
                    }
                  });
                });
              })(customerId, ub);
            }
          }else{            
            $scope.userBookings = null;
            $ionicLoading.hide();  
          }
        }
        else{
          $ionicLoading.hide();
        }
      });
    }
]);
angular.module('barber')
.controller('BarberRecentCutsController', ['$scope', 'BookingService', 'BarberService', 'utils', 'Auth', '$timeout', '$ionicLoading',
  function ($scope, BookingService, BarberService, utils, Auth, $timeout, $ionicLoading) {
    var uid = Auth.currentUserInfo.uid;
    $scope.type = Auth.currentUserInfo.type;    
      $ionicLoading.show({
        template: 'Get Listings...',
        noBackdrop: true
      });
      $scope.bookings = [];
      BookingService.getBarberBookings(uid).then(function(res){        
        if(res.success && res.data){
          $scope.userBookings = {};
          var date = new Date();
          var nowDate = moment(date).format('YYYY-MM-DD');
          var nowHouse = moment(date).format('H');
          for(var date in res.data){            
            var $obj = res.data[date];
            var $slot = $obj[Object.keys($obj)];            
            if(date <= nowDate || (date == nowDate && $slot <= nowHouse)){              
              $scope.userBookings = angular.extend($scope.userBookings, res.data[date]);                
            }            
          }        
          if(!$.isEmptyObject($scope.userBookings)){
            var i = 0;
            for(var ub in $scope.userBookings){              
              i++;
              var customerId = ub.split('_')[1];
              (function(customerId, ub){
                BarberService.getBarberDetail(customerId).then(function(res){
                  var bk = angular.extend({}, res);
                  var bookingTime = parseInt(ub.split('_')[2]);
                  BookingService.getBookings(ub).then(function(res){
                    if(res.success && res.data){
                      bk = angular.extend(bk, res.data);
                      bk.bookingTime = {
                        weekday: moment(bookingTime).format('dddd'),
                        month: moment(bookingTime).format('MMM'),
                        day: moment(bookingTime).format('DD'),                        
                        year: moment(bookingTime).format('YYYY')
                      };
                      bk.slot = (bk.slot>12)?((bk.slot-12)+" PM"):(12+" AM");
                      $timeout(function(){
                        bk.bookingId = ub;                        
                        if(bk.status != 'pending' && bk.status != 'booked'){
                          $scope.bookings.push(bk);
                        }
                        if(i >= $scope.bookings.length){
                          $ionicLoading.hide();
                        }
                      });
                    }
                  });
                });
              })(customerId, ub);
            }
          }else{               
            $scope.userBookings = null;      
            $ionicLoading.hide();  
          }
        }
        else{
          $ionicLoading.hide();
        }
      });
      $scope.show_list = function(status,event,limit) {
        $('#status_button .button').removeClass('button-positive').addClass('button-dark');
        $(event.target).removeClass('button-dark').addClass('button-positive');
        $('.ubh_item').css({display:'none'});
        $('.'+status+':lt('+limit+')').css({display:'block'});
      }
    }
]);
(function() {
  'use strict';
  angular.module('barber')
    .controller('ContactController', ['$scope', '$ionicLoading', '$state', '$ionicHistory', 'Auth', '$ionicPopup',
      function($scope, $ionicLoading, $state, $ionicHistory, Auth, $ionicPopup) {
        
      }
    ]);
}());